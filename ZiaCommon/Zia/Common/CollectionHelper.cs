using System;
using System.Linq;
using System.Collections.Generic;

namespace Zia.Common
{
    public class List
    {
        public static List<T> Of<T>(params T[] items)
        {
            return new List<T>(items);
        }
    }

    public class Pair
    {
        public static KeyValuePair<T, U> Of<T, U>(T key, U value)
        {
            return new KeyValuePair<T, U>(key, value);
        }
    }

    public class Map
    {
        public static Dictionary<T, U> Of<T, U>(params KeyValuePair<T, U>[] items)
        {
            return items.ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }
}

