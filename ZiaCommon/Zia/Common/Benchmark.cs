using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

namespace Zia.Common
{
	public class Benchmark
	{
		public Benchmark ()
		{
		}

		public static TimeSpan Sum(Action callback, int count)
		{
			List<TimeSpan> times = new List<TimeSpan>();

			for (int i = 0; i < count; i++) {
				times.Add (Clock (callback));
			}

			return TimeSpan.FromMilliseconds (times.Sum (time => time.TotalMilliseconds));
		}

        public static void AverageWrite(Action callback, int count, string name)
        {
            Console.WriteLine ("{0} Average Time : {1}ms", name, Sum(callback, count).TotalMilliseconds / count);
        }

		public static void SumWrite(Action callback, int count, string name)
		{
			Console.WriteLine ("{0} Sum Time : {1}ms", name, Sum(callback, count).TotalMilliseconds);
		}

		public static TimeSpan Clock(Action callback)
		{
			var start = DateTime.Now;
			callback ();
			var end = DateTime.Now;

			return end - start;
		}

		public static void ClockWrite(Action callback, string name)
		{
			Console.WriteLine ("{0} Time : {1}ms", name, Clock(callback).TotalMilliseconds);
		}

        public static void ClockFunction(Action callback)
        {
            var trace = new StackTrace();
            ClockWrite(callback, trace.GetFrame(1).GetMethod().Name);
        }
	}
}

