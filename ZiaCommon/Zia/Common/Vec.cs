using System;
using System.Runtime.InteropServices;

namespace Zia.Common
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Vec3
	{
		public float X, Y, Z;

		public Vec3 (float x, float y, float z = 0)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public Vec3 (Vec2 vec2, float z = 0)
		{
			X = vec2.X;
			Y = vec2.Y;
			Z = z;
		}

        
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct Vec2
	{
		public float X, Y;
        public static readonly Vec2 Max = new Vec2(float.MaxValue, float.MaxValue);
        public static readonly Vec2 Min = new Vec2(float.MinValue, float.MinValue);

		public Vec2 (float x, float y)
		{
			X = x;
			Y = y;
		}

        public Vec2 Round()
        {
            return new Vec2((int)X, (int)Y);
        }

        public Vec2 Clamp(Vec2 min, Vec2 max)
        {
            return new Vec2(MathUtil.Clamp(X, min.X, max.X), MathUtil.Clamp(Y, min.Y, max.Y));
        }



        public Vec2 Apply(Func<float, float> f)
        {
            return new Vec2(f(X), f(Y));
        }

        public static bool operator==(Vec2 left, Vec2 right) 
        {
            return left.X == right.X && left.Y == right.Y;
        }

        public static bool operator!=(Vec2 left, Vec2 right) 
        {
            return left.X != right.X || left.Y != right.Y;
        }
		

        public static Vec2 operator/(Vec2 left, float right) 
        {
            return new Vec2(left.X / right, left.Y / right);
        }

		public static Vec2 operator*(Vec2 left, Vec2 right) 
		{
			return new Vec2(left.X * right.X, left.Y * right.Y);
		}

        public static Vec2 operator*(Vec2 left, float right) 
        {
            return new Vec2(left.X * right, left.Y * right);
        }

        public static Vec2 operator+(Vec2 left, Vec2 right) => 
        new Vec2(left.X + right.X, left.Y + right.Y);

		public static Vec2 operator-(Vec2 left, Vec2 right) 
		{
			return new Vec2(left.X - right.X, left.Y - right.Y);
		}

		public static Vec2 operator/(Vec2 left, Vec2 right) 
		{
			return new Vec2(left.X / right.X, left.Y / right.Y);
		}

		

        public override string ToString()
        {
            return string.Format("[Vec2: X={0}, Y={1}]", X, Y);
        }
        
	}
}

