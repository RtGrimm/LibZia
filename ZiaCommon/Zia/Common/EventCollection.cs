using System;
using System.Collections.Generic;
using System.Linq;

namespace Zia.Common
{
	public class EventCollection<T> : ICollection<T>
	{
		ICollection<T> _BackingCollection;

		public event Action<T> ItemAdded = (item) => {};
		public event Action<T> ItemRemoved = (item) => {};

		public event Action OnClear = () => {};
		public event Action Changed = () => {};


		public EventCollection (ICollection<T> backingCollection)
		{
			_BackingCollection = backingCollection;
		}



		public void Add (T item)
		{
			_BackingCollection.Add (item);
			ItemAdded (item);
			Changed ();
		}

		public void Clear ()
		{
			OnClear ();
			_BackingCollection.Clear ();


			Changed ();
		}

		public bool Contains (T item)
		{
			return _BackingCollection.Contains (item);
		}

		public void CopyTo (T[] array, int arrayIndex)
		{
			_BackingCollection.CopyTo (array, arrayIndex);
		}

		public bool Remove (T item)
		{
			var ret = _BackingCollection.Remove (item);

			ItemRemoved (item);
			Changed ();

			return ret;
		}

		public IEnumerator<T> GetEnumerator ()
		{
			return _BackingCollection.GetEnumerator ();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
		{
			return _BackingCollection.GetEnumerator ();
		}

		public int Count {
			get {
				return _BackingCollection.Count;
			}
		}

		public bool IsReadOnly {
			get {
				return _BackingCollection.IsReadOnly;
			}
		}
	}
}

