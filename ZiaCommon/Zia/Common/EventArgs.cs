using System;

namespace Zia.Common
{
	public class EventArgs<T>
	{
		public T Args {
			get;
			private set;
		}

		public EventArgs (T args)
		{
			Args = args;
		}
	}
}

