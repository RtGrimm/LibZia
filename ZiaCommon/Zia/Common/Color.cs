using System;
using System.Runtime.InteropServices;

namespace Zia.Common
{
   
    public struct HSVColor
    {
        public float Hue;
        public float Value;
        public float Saturation;


        public HSVColor(float hue, float value, float saturation)
        {
            this.Hue = hue;
            this.Value = value;
            this.Saturation = saturation;
        }

        public Color ToRGBA()
        {
            int hi = Convert.ToInt32(Math.Floor(Hue / 60)) % 6;
            double f = Hue / 60 - Math.Floor(Hue / 60);

            Value = Value * 255;
            int v = Convert.ToInt32(Value);
            int p = Convert.ToInt32(Value * (1 - Saturation));
            int q = Convert.ToInt32(Value * (1 - f * Saturation));
            int t = Convert.ToInt32(Value * (1 - (1 - f) * Saturation));

            if (hi == 0)
                return Color.FromInt(v, t, p, 255);
            else if (hi == 1)
                return Color.FromInt(q, v, p, 255);
            else if (hi == 2)
                return Color.FromInt(p, v, t, 255);
            else if (hi == 3)
                return Color.FromInt(p, q, v, 255);
            else if (hi == 4)
                return Color.FromInt(t, p, v, 255);
            else
                return Color.FromInt(v, p, q, 255);
        }
        
    }

	[StructLayout(LayoutKind.Sequential)]
	public struct Color
	{
        public static readonly Color Turquoise = Color.FromInt(26, 188, 156);
        public static readonly Color Emerland = Color.FromInt(46, 204, 113);
        public static readonly Color PeterRiver = Color.FromInt(52, 152, 219);
        public static readonly Color Amethyst = Color.FromInt(155, 89, 182);
        public static readonly Color WetAsphalt = Color.FromInt(52, 73, 94);
        public static readonly Color GreenSea = Color.FromInt(22, 160, 133);
        public static readonly Color Nephritis = Color.FromInt(39, 174, 96);
        public static readonly Color BelizeHole = Color.FromInt(41, 128, 185);
        public static readonly Color Wisteria = Color.FromInt(142, 68, 173);
        public static readonly Color MidnightBlue = Color.FromInt(44, 62, 80);
        public static readonly Color SunFlower = Color.FromInt(241, 196, 15);
        public static readonly Color Carrot = Color.FromInt(230, 126, 34);
        public static readonly Color Alizarin = Color.FromInt(231, 76, 60);
        public static readonly Color Clouds = Color.FromInt(236, 240, 241);
        public static readonly Color Concrete = Color.FromInt(149, 165, 166);
        public static readonly Color Orange = Color.FromInt(243, 156, 18);
        public static readonly Color Pumpkin = Color.FromInt(211, 84, 0);
        public static readonly Color Pomegranate = Color.FromInt(192, 57, 43);
        public static readonly Color Silver = Color.FromInt(189, 195, 199);
        public static readonly Color Asbestos = Color.FromInt(127, 140, 141);

		public float R, G, B, A;

		public Color (float r, float g, float b, float a)
		{
			R = r;
			G = g;
			B = b;
			A = a;
		}

        public static Color Random()
        {
            return Random(new Random());
        }

        public static Color Random(Random random)
        {
            return new Color((float)random.NextDouble(), 
                             (float)random.NextDouble(), 
                             (float)random.NextDouble(), 
                             (float)random.NextDouble());
        }

        public Color Lighter(float factor = 0.5f)
        {
            return Darker(-factor);
        }

        public Color Darker(float factor = 0.5f)
        {
            factor = 1 - factor;

            var hsvColor = ToHSV();
            hsvColor.Value *= factor;

            return hsvColor.ToRGBA();
        }

        public float Hue() {

            float min = Math.Min(Math.Min(R, G), B);
            float max = Math.Max(Math.Max(R, G), B);

            float hue = 0f;
            if (max == R) {
                hue = (G - B) / (max - min);

            } else if (max == G) {
                hue = 2f + (B - R) / (max - min);

            } else {
                hue = 4f + (R - G) / (max - min);
            }

            hue = hue * 60;
            if (hue < 0) hue = hue + 360;

            return hue;
        }



        public HSVColor ToHSV()
        {
            var r = R * 255;
            var g = G * 255;
            var b = B * 255;

            float max = Math.Max(r, Math.Max(g, b));
            float min = Math.Min(r, Math.Min(g, b));

            var hue = Hue();
            var saturation = (max == 0) ? 0 : 1f - (1f * min / max);
            var value = max / 255f;

            return new HSVColor(hue, saturation, value);
        }

        public static Color operator*(Color left, float right) 
        {
            return new Color(left.R * right, left.G * right, left.B * right, left.A * right);
        }

        public static Color operator+(Color left, Color right) 
        {
            return new Color(left.R + right.R, left.G + right.G, left.B + right.B, left.A + right.A);
        }

        public static Color operator-(Color left, Color right) 
        {
            return new Color(left.R - right.R, left.G - right.G, left.B - right.B, left.A - right.A);
        }


		public static Color FromInt(int r, int g, int b, int a = 255)
		{
			return new Color (r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
		}

		public override string ToString ()
		{
			return string.Format ("{0}, {1}, {2}, {3}", R, G, B, A);
		}
	}
}

