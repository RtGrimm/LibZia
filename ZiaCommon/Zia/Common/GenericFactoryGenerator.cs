using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;

namespace Zia.Common
{
    public class GenericFactoryGenerator
    {
        private static Dictionary<Tuple<Type, Type[]>, Delegate> _CreationCallbackCache =
            new Dictionary<Tuple<Type, Type[]>, Delegate>();

        private static Delegate GetFactory(Type type, Type[] argumentTypes)
        {
            var key = Tuple.Create(type, argumentTypes);
            if(!_CreationCallbackCache.ContainsKey(key))
            {
                var paramExpressions = argumentTypes.Select(
                    (paramType, i) => Expression.Parameter(paramType, "param" + i.ToString())).ToList();
                var newExpression = Expression.New(type.GetConstructor(argumentTypes.ToArray()), paramExpressions);

                var callback = Expression.Lambda(newExpression, paramExpressions).Compile();
                _CreationCallbackCache[key] = callback;
            }

            return _CreationCallbackCache[key];
        }

        public static object Create(Type type, object[] arguments)
        {
            return GetFactory(type, arguments.Select(arg => arg.GetType()).ToArray()).DynamicInvoke(arguments);
        }
    }
}

