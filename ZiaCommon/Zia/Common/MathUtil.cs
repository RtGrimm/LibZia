using System;

namespace Zia.Common
{
	public class MathUtil
	{
		public static float Min(float x, float y)
		{
			return x < y ? x : y;
		}

		public static float Max(float x, float y)
		{
			return x > y ? x : y;
		}

		public static float ToRadians(float deg)
		{
			return deg * 3.14159265359f / 180.0f;
		}

		public static float ToDegrees(float rad)
		{
			return rad * 180.0f / 3.14159265359f;
		}

        public static int Clamp(int value, int min, int max)
        {
            if(min > max)
            max = Clamp(max, min, int.MaxValue);
            if(max < min)
            min = Clamp(min, int.MinValue, max);

            if (value < min)
                return min;

            if (value > max)
                return max;

            return value;
        }

        public static float Clamp(float value, float min, float max)
        {
            if (value < min)
                return min;

            if (value > max)
                return max;

            return value;
        }
	}
}


