using System;
using System.Collections.Generic;

namespace Zia.Common
{
	public static class ExMethods
	{
		public static void AddRange<T>(this ICollection<T> target, IEnumerable<T> items)
		{
			foreach (var item in items) {
				target.Add (item);
			}
		}

		public static void Each<T>(this IEnumerable<T> target, Action<T> callback)
		{
			foreach (var item in target) {
				callback (item);
			}
		}

		public static void Each<T>(this IEnumerable<T> target, Action<T, int> callback)
		{
			var i = 0;
			foreach (var item in target) {
				callback (item, i);
				i++;
			}
		}

        public static U Apply<T, U>(this T target, Func<T, U> f)
        {
            return f(target);
        }

        public static void Apply<T>(this T target, Action<T> f)
        {
            f(target);
        }

		public static TOut SafeCast<TCast, TOut>(this object target, Func<TCast, TOut> callback) 
			where TCast : class where TOut : class
		{
			var castTarget = target as TCast;

			if (castTarget != null)
				return callback (castTarget);

			return default(TOut);
		}

		public static TOut SafeCast<TCast, TOut>(this object target, Func<TCast, TOut> callback, Func<TOut> defaultCallback) 
			where TCast : class where TOut : class
		{
			var castTarget = target as TCast;

			if (castTarget != null)
				return callback (castTarget);

			return defaultCallback();
		}

        public static U As<U, T>(this T target) 
            where T : class 
            where U : class => target as U;

		public static void SafeCast<TCast>(this object target, Action<TCast> callback) 
			where TCast : class
		{
			var castTarget = target as TCast;

			if (castTarget != null)
				callback (castTarget);

		}

        public static T SafeNull<T>(this T target, T defaultValue)
        {
            return target == null ? defaultValue : target;
        }

		public static U SafeNull<T, U>(this T target, Func<T, U> notNullCallback, Func<U> defaultCallback)
		{
			return target == null ? defaultCallback () : notNullCallback(target);
		}

		public static U SafeNull<T, U>(this T target, Func<T, U> notNullCallback, U defaultValue)
		{
			return target == null ? defaultValue : notNullCallback(target);
		}

		public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> target, TKey key, Func<TValue> createValueCallback)
		{
			if (!target.ContainsKey (key)) {
				var value = createValueCallback ();
				target [key] = value;

				return value;
			}

			return target [key];
		}

        public static U ConvertEnum<T, U>(this T target) where T : struct, IComparable where U : struct, IComparable
        {
            return (U)Enum.Parse(typeof(U), target.ToString());
        }
	}
}

