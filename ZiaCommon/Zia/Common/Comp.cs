using System;
using System.Collections.Generic;

namespace Zia.Common
{
	public class Comp<T> : IEqualityComparer<T>
	{
		Func<T, T, bool> _Callback;

		public Comp (Func<T, T, bool> callback)
		{
			_Callback = callback;
		}

		public bool Equals (T x, T y)
		{
			return _Callback (x, y);
		}

		public int GetHashCode (T obj)
		{
			return obj.GetHashCode ();
		}
	}
}

