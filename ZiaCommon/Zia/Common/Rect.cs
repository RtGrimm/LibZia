using System;
using System.Runtime.InteropServices;

namespace Zia.Common
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Rect
	{
		public Vec2 Size;
		public Vec2 Location;

        public Vec2 Max {
            get {
                return Location + Size;
            }
        }

		public Rect (Vec2 location, Vec2 size)
		{
			Size = size;
			Location = location;
		}

		public Rect (float x, float y, float width, float height)
		{
			Location = new Vec2(x, y);
			Size = new Vec2(width, height);
		}

		public bool Contains(Vec2 point)
		{
			var result = point.X >= Location.X
				&& point.Y >= Location.Y
				&& (point.X <= Location.X + Size.X)
				&& (point.Y <= Location.Y + Size.Y);

			return result;
		}

        public bool Intersects(Rect rect)
        {
            if (this.Max.X < rect.Location.X) return false; // a is left of b
            if (this.Location.X > rect.Max.X) return false; // a is right of b
            if (this.Max.Y < rect.Location.Y) return false; // a is above b
            if (Location.Y > rect.Max.Y) return false; // a is below b
            return true; // boxes overlap
        }

		public static Rect LRTB(float left, float right, float top, float bottom)
		{
			return new Rect (new Vec2(left, top), new Vec2(right - left, bottom - top));
		}
        public override string ToString()
        {
            return string.Format("[Rect: Size={0}, Location={1}]", Size, Location);
        }
        
	}
}

