using System;

namespace Zia.Common
{
	public static class Singleton<T> where T : new()
	{
		static T _Instance;

		public static T Instance {
			get {
                if (_Instance == null)
                    _Instance = new T();

				return _Instance;
			}
		}
	}
}

