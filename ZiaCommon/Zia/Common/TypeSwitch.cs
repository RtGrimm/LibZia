using System;
using System.Collections.Generic;
using System.Linq;

namespace Zia.Common
{
	public class TypeSwitch
	{
		interface ICase
		{
			Type Type {
				get;
			}
			void Run(object target);
		}

		class CaseRunner<T> : ICase
		{
			Action<T> _Callback;

			public Type Type {
				get {
					return typeof(T);
				}
			}
			public CaseRunner(Action<T> callback)
			{
				_Callback = callback;
			}

			public void Run (object target)
			{
				_Callback ((T)target);
			}
		}

		private List<ICase> _Cases = new List<ICase>();

		public void Case<T>(Action<T> callback)
		{
			_Cases.Add (new CaseRunner<T>(callback));
		}

        public void Case<T>(Action callback)
        {
            _Cases.Add (new CaseRunner<T>(input => callback()));
        }

		public void Run<T>(T target)
		{
            _Cases.First (@case => @case.Type.IsAssignableFrom(target.GetType())).Run(target);
		}
	}
}

