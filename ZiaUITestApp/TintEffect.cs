using System;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using Zia.Common;
using Zia.UI;
using Zia.UI.Shapes;
using Zia.UI.Controls;
using Zia.UI.Brushes;
using Zia.UI.Animation;
using Zia.UI.Media;
using Zia.Gfx.Text;
using Zia.UI.Input;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq.Expressions;

namespace ZiaUITestApp
{

    class TintEffect : Zia.Gfx.Platform.OpenGL.GLShaderEffect, IUIEffect
    {
        private static String _VertexShader = @"
#version 440 core

in vec3 VertLocation;
in vec2 VertUV;

out vec2 UV;

void main()
{
    UV = VertUV;
    gl_Position = vec4(VertLocation, 1);
}
";

        private static String _FragmentShader = @"
            #version 440 core
            layout(origin_upper_left) in vec4 gl_FragCoord;
        in vec2 UV;
        out vec4 OutColor;

        uniform Data
        {
            vec4 Color;
        };

        uniform sampler2D Inputs;
        uniform bool FlipVertical;

        void main()
        {
            OutColor = texture2D(Inputs, vec2(UV.x, FlipVertical ? 1 - UV.y : UV.y)) * Color;  
        }
        ";

        struct Data
        {
            public Color Color;
        }

        Color _Color;

        public Color Color
        {
            get
            {
                return _Color;
            }
            set
            {
                _Color = value;

                SetData(new Data
                {
                    Color = _Color
                });
              
            }
        }

        public Zia.Gfx.Effect.IShaderEffect ShaderEffect
        {
            get
            {
                return this;
            }
        }

        public TintEffect() :  base(_FragmentShader, _VertexShader)
        {
            
        }

        public void SetInput(Zia.Gfx.ISurface surface)
        {
            Inputs.Clear();
            Inputs.Add(surface);
        }
    }
    
}
