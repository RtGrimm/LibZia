using System;
using Zia.Common;
using Zia.UI;
using Zia.UI.Shapes;
using Zia.UI.Controls;
using Zia.UI.Brushes;
using Zia.UI.Animation;
using Zia.UI.Media;
using Zia.Gfx.Text;
using Zia.UI.Input;
using System.Collections.Generic;

namespace ZiaUITestApp
{
    public class UnionFlag
    {
        public static AdvancedElement Create()
        {
            return new Rectangle
            {
                Children = new List<Element> {

                    new Zia.UI.Shapes.Path  {
                        Figures = new List<Figure> {
                            new SegmentFigure {
                                StartPoint = new Vec2(0, 0),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(1, 1) }
                                }
                            },
                            new SegmentFigure {
                                StartPoint = new Vec2(1, 0),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(0, 1) }
                                }
                            }
                        },
                        Stroke = new SolidBrush(new Color(1, 1, 1, 1)),
                        EndType = Zia.Gfx.Geometry.EndType.OpenButt,
                        Transforms = new List<ElementTransform> {

                        },
                        StrokeWidth = 30
                    },
                    new Zia.UI.Shapes.Path  {
                        Figures = new List<Figure> {
                            new SegmentFigure {
                                StartPoint = new Vec2(0, 0),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(1, 1) }
                                }
                            },
                            new SegmentFigure {
                                StartPoint = new Vec2(1, 0),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(0, 1) }
                                }
                            }
                        },
                        Stroke = new SolidBrush(new Color(1, 0, 0, 1)),
                        EndType = Zia.Gfx.Geometry.EndType.OpenButt,
                        Transforms = new List<ElementTransform> {

                        },
                        StrokeWidth = 10
                    },
                    new Zia.UI.Shapes.Path  {
                        Figures = new List<Figure> {
                            new SegmentFigure {
                                StartPoint = new Vec2(0.5f, 0),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(0.5f, 1) }
                                }
                            },
                            new SegmentFigure {
                                StartPoint = new Vec2(0, 0.5f),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(1, 0.5f) }
                                }
                            }
                        },
                        Stroke = new SolidBrush(new Color(1, 1, 1, 1)),
                        EndType = Zia.Gfx.Geometry.EndType.OpenButt,
                        Transforms = new List<ElementTransform> {

                        },
                        StrokeWidth = 30
                    },
                    new Zia.UI.Shapes.Path  {
                        Figures = new List<Figure> {
                            new SegmentFigure {
                                StartPoint = new Vec2(0.5f, 0),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(0.5f, 1) }
                                }
                            },
                            new SegmentFigure {
                                StartPoint = new Vec2(0, 0.5f),
                                Segments = new List<Segment> {
                                    new Line { To = new Vec2(1, 0.5f) }
                                }
                            }
                        },
                        Stroke = new SolidBrush(new Color(1, 0, 0, 1)),
                        EndType = Zia.Gfx.Geometry.EndType.OpenButt,
                        Transforms = new List<ElementTransform> {

                        },
                        StrokeWidth = 20
                    }
                },
                Fill = new SolidBrush(new Color(0, 0, 1, 1)),
                Clip = true
            
            };
        }
    }
}

