using System;
using System.Runtime.InteropServices;
using Zia.Gfx.Platform.OpenGL;
using Zia.UI;
using Zia.Common;
using Zia.UI.Data;

namespace ZiaUITestApp
{
    public class RippleEffect : PropertyBase, IUIEffect
    {


        private static Tag RippleEffectInvalidateTag = 
            new Tag(() => RippleEffectInvalidateTag);

        public static readonly Property<Vec2, RippleEffect> CenterProperty = 
                    Property<Vec2, RippleEffect>
                        .New()
                .Tags(RippleEffectInvalidateTag, Element.RenderInvalidationTag)
                        .DefaultValue(new Vec2(0.5f, 0.5f))
                        .Name(() => CenterProperty);

        public Vec2 Center
        {
            get
            {
                return CenterProperty.GetValue(this);
            }
            set
            {
                CenterProperty.SetValue(this, value);
            }
        }
        
        public static readonly Property<float, RippleEffect> AmplitudeProperty = 
                    Property<float, RippleEffect>
                        .New()
                .Tags(RippleEffectInvalidateTag, Element.RenderInvalidationTag)
                        .Name(() => AmplitudeProperty);

        public float Amplitude
        {
            get
            {
                return AmplitudeProperty.GetValue(this);
            }
            set
            {
                AmplitudeProperty.SetValue(this, value);
            }
        }

        public static readonly Property<float, RippleEffect> FrequencyProperty = 
                    Property<float, RippleEffect>
                        .New()
                .Tags(RippleEffectInvalidateTag, Element.RenderInvalidationTag)
                        .Name(() => FrequencyProperty);

        public float Frequency
        {
            get
            {
                return FrequencyProperty.GetValue(this);
            }
            set
            {
                FrequencyProperty.SetValue(this, value);
            }
        }

        public static readonly Property<float, RippleEffect> PhaseProperty = 
                    Property<float, RippleEffect>
                        .New()
                .Tags(RippleEffectInvalidateTag, Element.RenderInvalidationTag)
                        .Name(() => PhaseProperty);

        public float Phase
        {
            get
            {
                return PhaseProperty.GetValue(this);
            }
            set
            {
                PhaseProperty.SetValue(this, value);
            }
        }



        

        private static String _VertexShader = @"
#version 440 core

in vec3 VertLocation;
in vec2 VertUV;

out vec2 UV;

void main()
{
    UV = VertUV;
    gl_Position = vec4(VertLocation, 1);
}
";
        private static String _FragmentShader = @"
            #version 440 core
            layout(origin_upper_left) in vec4 gl_FragCoord;
        in vec2 UV;
        out vec4 OutColor;

        uniform Data
        {
            vec2 center;
            float amplitude;
            float frequency;
            float phase;
        };

        uniform sampler2D Inputs;
        uniform bool FlipVertical;

        void sincos(float x, out float s, out float c)
        {
            s = sin(x);
            c = cos(x);
        }


        void main()
        {
            vec2 uv = vec2(UV.x, FlipVertical ? 1 - UV.y : UV.y);

           vec2 dir = uv - center;

            vec2 toPixel = uv - center; // vector from center to pixel
            float distance = length(toPixel);
             vec2 direction = toPixel/distance;
            float angle = atan(direction.y, direction.x);
            vec2 wave;
            sincos(frequency * distance + phase, wave.x, wave.y);

            float falloff = clamp(1-distance, 0, 1);
           falloff *= falloff;

            distance += amplitude * wave.x * falloff;
            sincos(angle, direction.y, direction.x);
             vec2 uv2 = center + distance * direction;

            float lighting = clamp(wave.y * falloff, 0, 1) * 0.2 + 0.8;

            vec4 color = texture2D( Inputs, uv2 );
            color.rgb *= lighting;

            OutColor = color;
        }
        ";

        class Effect : GLShaderEffect
        {
            [StructLayout(LayoutKind.Sequential)]
            public struct Data
            {
                public Vec2 Center;
                public float Amplitude;
                public float Frequency;
                public float Phase;
            }

            public Effect() : base(_FragmentShader, _VertexShader)
            {
            }
        }

        private Effect _Effect = new Effect();

        public Zia.Gfx.Effect.IShaderEffect ShaderEffect
        {
            get
            {
                return _Effect;
            }
        }



        public RippleEffect ()
        {
            this.AddListener(Listener.New()
                             .WithTags(RippleEffectInvalidateTag)
                             .WithCallback(() => UpdateData()));

            UpdateData();
        }

        public void SetInput(Zia.Gfx.ISurface surface)
        {
            if (_Effect.Inputs.Count == 0)
            {
                _Effect.Inputs.Add(surface);
            }
            else
            {
                _Effect.Inputs[0] = surface;
            }
        }

        private void UpdateData()
        {
            _Effect.SetData(new Effect.Data {
                Center = new Vec2(Center.X, 1.0f - Center.Y) ,
                Amplitude = Amplitude,
                Frequency = Frequency,
                Phase = Phase
            });
        }


    }
}

