using System;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using Zia.Common;
using Zia.UI;
using Zia.UI.Shapes;
using Zia.UI.Controls;
using Zia.UI.Brushes;
using Zia.UI.Animation;
using Zia.UI.Media;
using Zia.Gfx.Text;
using Zia.UI.Input;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq.Expressions;

namespace ZiaUITestApp
{

    class OpenTKPointerInputSource : IPointerInputSource
    {
        public event Action<PointerEventArgs> PointerUp = (args) => {};

        public event Action<PointerEventArgs> PointerDown = (args) => {};

        public event Action<PointerEventArgs> PointerMove = (args) => {};

        public OpenTKPointerInputSource(GameWindow window)
        {
            window.MouseMove += (sender, e) => 
                PointerMove(new PointerEventArgs(new Vec2(e.X, e.Y)));

            window.MouseDown += (sender, e) => 
                PointerDown(new PointerEventArgs(new Vec2(e.X, e.Y)));

            window.MouseUp += (sender, e) => 
                PointerUp(new PointerEventArgs(new Vec2(e.X, e.Y)));
        }
    }
    
}
