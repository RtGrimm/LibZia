using System;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using Zia.Common;
using Zia.UI;
using Zia.UI.Shapes;
using Zia.UI.Controls;
using Zia.UI.Brushes;
using Zia.UI.Animation;
using Zia.UI.Media;
using Zia.Gfx.Text;
using Zia.UI.Input;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq.Expressions;
using Zia.UI.Data;

namespace ZiaUITestApp
{
    class MouseLocation : IBindingSource<Vec2>
    {
        public event Action Changed = () => {};

        private Vec2 _Location;

        public MouseLocation(GameWindow window)
        {
            window.MouseMove += (sender, e) => {
                _Location = new Vec2(e.X, e.Y);
                Changed();
            };
        }

        public Vec2 GetValue()
        {
            return _Location;
        }
    }
    
}
