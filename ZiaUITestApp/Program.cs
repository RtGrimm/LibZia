using System;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using Zia.Common;
using Zia.UI;
using Zia.UI.Shapes;
using Zia.UI.Controls;
using Zia.UI.Brushes;
using Zia.UI.Animation;
using Zia.UI.Media;
using Zia.Gfx.Text;
using Zia.UI.Input;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq.Expressions;
using Zia.UI.Data;
using Zia.UI.Resource;

namespace ZiaUITestApp
{
    class Root : Rectangle
    {
        public Root()
        {

        }
    }



    class TestViewModel
    {
        public ObservableProperty<int> Count = new ObservableProperty<int>();


        public void Inc()
        {
            Count.Value++;
        }
    }

    class MainWindow : GameWindow
    {
        private Scene _Scene;

        private ObservableProperty<int> _Counter = new ObservableProperty<int>(0);



        public MainWindow() : base(1200, 800, new OpenTK.Graphics.GraphicsMode(32, 0, 0, 8))
        {
            Init();
        }

        void Init()
        {
            VSync = VSyncMode.Off;

            _Scene = new Scene(new Zia.Gfx.Platform.OpenGL.GLWindowTarget(this));
            _Scene.SetPointerInputSource(new OpenTKPointerInputSource(this));
            _Scene.SetKeyInputSource(new OpenTKKeyInputSource(this));

            Resize += (sender, e) => _Scene.SizeChanged(Width, Height);

            RenderFrame += Update;


            InitUI();

        }


        void InitUI()
        {


            var lightFont = Resource.Load<FontLoader, Font>("Fonts/OpenSans-Light.ttf").Result; 
            var normalFont = Resource.Load<FontLoader, Font>("Fonts/OpenSans-Bold.ttf").Result;  


            var listbox = new Listbox<string> {
                Height = Length.Auto,
                SizeToContent = true
            };

            listbox.ProxyOverrides.Add(new ProxyOverride {
                Name = "Item",
                Template = new CallbackTemplate(() => {
                    var letter = new TextBlock {
                        Font = lightFont,
                        FontSize = 20,
                        Brush = new SolidBrush(Color.PeterRiver),
                        Width = Length.Auto,
                        Height = Length.Auto,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    };

                    var text = new TextBlock {
                        Font = normalFont,
                        FontSize = 20,
                        Brush = new SolidBrush(Color.Clouds),
                        Width = Length.Auto,
                        Height = Length.Auto,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    };

                    BindingBuilder
                        .Bind(Listbox<string>.ItemDataContext, letter, 
                            str => str.First().ToString())
                        .To(letter, TextBlock.TextProperty)
                        .Set<string>();

                    BindingBuilder
                        .Bind(Listbox<string>.ItemDataContext, text, 
                            str => str)
                        .To(text, TextBlock.TextProperty)
                        .Set<string>();

                    var rectangle = new Rectangle {
                        Children = new List<Element> {
                            new Ellipse() {
                                Children = List.Of<Element>(letter),
                                Fill = new SolidBrush(Color.Clouds),
                                HorizontalAlignment = HorizontalAlignment.Left,
                                VerticalAlignment = VerticalAlignment.Center,
                                Padding = new Margins(20),
                                Width = Length.Auto,
                                Height = Length.Auto
                            },
                            text
                        },
                        Height = Length.Auto,
                        Fill = new SolidBrush(Color.PeterRiver),
                        Margins = new Margins(1),
                        Padding = new Margins(10),
                        CornerRadius = new CornerRadius(0),
                        CacheChildren = false,
                        CacheEnabled = false,
                        Opacity = 0.7f
                    };

                    rectangle.Adopted += parent => {
                        ClockAnimationBuilder<Margins>.New()
                            .Start(new Margins(rectangle.Parent.Parent.RenderSize.X, -rectangle.Parent.Parent.RenderSize.X, 0, 0))
                            .End(new Margins()).Target(rectangle, Element.MarginsProperty)
                            .Interpolator(new QuadEaseInOut())
                            .Length(TimeSpan.FromMilliseconds(1000))
                            .Start();
                    };

                    rectangle.Adopted += parent => parent.RemoveOverride = remove => {
                        ClockAnimationBuilder<Margins>.New()
                            .End(new Margins(parent.RenderSize.X, -parent.RenderSize.X, 0, 0))
                            .Target(parent, Element.MarginsProperty)
                            .Interpolator(new QuadEaseInOut())
                            .Length(TimeSpan.FromMilliseconds(1000))
                            .Start()
                            .Ended += () => remove();

                        Console.WriteLine("Remove");
                    };

                    rectangle.Triggers = new List<ITrigger> {
                        new PropertyTrigger<bool, AdvancedElement>(AdvancedElement.IsPointerOverProperty, true) {
                            EnterActions = new List<ITriggerAction> {
                                new ClockTriggerAction {
                                    Animations = new List<ITimeable> {
                                        AnimationBuilder<float>.New()
                                            .Target(rectangle, Element.OpacityProperty).End(1)
                                            .Interpolator(new QuadEaseInOut())
                                        },
                                    Length = TimeSpan.FromMilliseconds(200)
                                }
                            },
                            ExitActions = new List<ITriggerAction> {
                                new ClockTriggerAction {
                                    Animations = new List<ITimeable> {
                                        AnimationBuilder<float>.New()
                                            .Target(rectangle, Element.OpacityProperty).End(0.7f)
                                            .Interpolator(new QuadEaseInOut())
                                        },
                                    Length = TimeSpan.FromMilliseconds(200)
                                }
                            }
                        }
                    };

                    return rectangle;
                })
            });

            var addButton = new Button
            {
                Content = new TextBlock
                {
                    Font = lightFont,
                    FontSize = 20,
                    Brush = new SolidBrush(Color.Clouds),
                    Text = "Add",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Width = Length.Auto,
                    Height = Length.Auto
                },
                MaxWidth = 400,
                Height = Length.Auto,
                Width = Length.MatchSlot,
                Padding = new Margins(2),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Opacity = 1f
            };

            var removeButton = new Button
            {
                Content = new TextBlock
                {
                    Font = lightFont,
                    FontSize = 20,
                    Brush = new SolidBrush(Color.Clouds),
                    Text = "Remove",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Width = Length.Auto,
                    Height = Length.Auto
                },
                MaxWidth = 400,
                Height = Length.Auto,
                Width = Length.MatchSlot,
                Padding = new Margins(2),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Opacity = 1f
            };

            Grid.GridColumnProperty.SetValue(addButton, 0);
            Grid.GridColumnProperty.SetValue(removeButton, 1);

            var buttonGrid = new Grid
            {
                Rows = List.Of(new Grid.Row(1)),
                Columns = List.Of(new Grid.Column(0.5f), new Grid.Column(0.5f)),
                Children = new List<Element> {
                    addButton,
                    removeButton
                }
            };



            Grid.GridRowProperty.SetValue(buttonGrid, 1);


            var scrollViewer = new VelocityScrollViewer {
                Children = List.Of<Element>(listbox),
                Friction = 1.01f,
                Opacity = 1f
            };
            var root = new Rectangle
            {
                Children = new List<Element>() {
                    new Rectangle {
                        Children = new List<Element> {
                            new Grid {
                                Rows = List.Of(
                                    new Grid.Row(1), 
                                    new Grid.Row(100)),
                                Columns = List.Of(new Grid.Column(1)),
                                Children = new List<Element> {
                                    new Rectangle {
                                        Children = new List<Element> {
                                            scrollViewer
                                        },
                                        Fill = new SolidBrush(Color.Clouds),
                                        Margins = new Margins(20),
                                        CacheEnabled = true,
                                        CacheChildren = true
                                    },
                                    buttonGrid
                                }
                            }
                        },
                        Width = new Exact(960),
                        Fill = new SolidBrush(Color.Clouds),
                        HorizontalAlignment = HorizontalAlignment.Center
                    }

                },
                CacheChildren = false,
                CacheEnabled = false,
                Fill = new SolidBrush(Color.Silver),
                Opacity = 0,
                Width = Length.FillSlot,
                Height = Length.FillSlot
            };


            var words = File.ReadAllText("/usr/share/dict/words").Split('\n').ToList();
            var random = new Random();

            addButton.PointerDown += args => {
                listbox.Items.Add(words.ElementAt(random.Next(words.Count - 1)));
                scrollViewer.Velocity += new Vec2(0, 1);
            };

            removeButton.PointerDown += args => {
                if(listbox.Items.Count == 0)
                    return;

                listbox.Items.Remove(listbox.Items.Last());
                scrollViewer.Velocity += new Vec2(0, -1);
            };

            _Scene.SetRoot(root);
            TransitionUI(root);
        }

        void TransitionUI(Element root)
        {
            ClockAnimationBuilder<float>.New()
                .Interpolator(new QuadEaseInOut())
                .End(1)
                .Target(root, Element.OpacityProperty)
                .Length(TimeSpan.FromMilliseconds(1000)).Start();
        }

        void Update(object sender, FrameEventArgs e)
        {
            Console.WriteLine("RenderTime : {0}ms; FPS : {1};", 
                              RenderTime * 1000, 1000 / (RenderTime * 1000));

            _Scene.Update();
            SwapBuffers();
        }
    }

    class MainClass
    {


        public static void Main()
        {


            MainWindow mainWindow = new MainWindow();
            mainWindow.Run();
        }
    }
}

