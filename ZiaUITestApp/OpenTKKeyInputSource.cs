using System;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using Zia.Common;
using Zia.UI;
using Zia.UI.Shapes;
using Zia.UI.Controls;
using Zia.UI.Brushes;
using Zia.UI.Animation;
using Zia.UI.Media;
using Zia.Gfx.Text;
using Zia.UI.Input;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq.Expressions;

namespace ZiaUITestApp
{

    class OpenTKKeyInputSource : IKeyInputSource
    {

        public event Action<Zia.UI.Input.KeyPressEventArgs> KeyPress;

        public event Action<KeyEventArgs> KeyDown;

        public event Action<KeyEventArgs> KeyUp;

        private KeyEventArgs ConvertArgs(OpenTK.Input.KeyboardKeyEventArgs args)
        {
            return new KeyEventArgs
            {
                Alt = args.Alt,
                Control = args.Control,
                Shift = args.Shift,
                KeyCode = args.Key.ConvertEnum<OpenTK.Input.Key, Key>(),

            };
        }

        public OpenTKKeyInputSource(GameWindow window)
        {
            window.KeyDown += (sender, e) => 
                KeyDown(ConvertArgs(e));

            window.KeyUp += (sender, e) => 
                KeyUp(ConvertArgs(e));

            window.KeyPress += (sender, e) => 
                KeyPress(new Zia.UI.Input.KeyPressEventArgs(e.KeyChar));
        }
    }
    
}
