using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using OpenTK;

using OpenTK.Graphics.OpenGL;

using Zia.Common;
using Zia.Gfx;
using Zia.Gfx.Geometry;
using Zia.Gfx.Text;
using System.Drawing.Imaging;

namespace ZiaGfxTestApp
{
	class TintEffect : Zia.Gfx.Platform.OpenGL.GLShaderEffect
	{
		private static String _VertexShader = @"
#version 440 core

in vec3 VertLocation;
in vec2 VertUV;

out vec2 UV;

void main()
{
	UV = VertUV;
    gl_Position = vec4(VertLocation, 1);
}
";
		private static String _FragmentShader = @"
#version 440 core
layout(origin_upper_left) in vec4 gl_FragCoord;
in vec2 UV;
out vec4 OutColor;

uniform Data
{
	vec4 Color;
};

uniform sampler2D Inputs;
uniform bool FlipVertical;

void main()
{
	OutColor = texture2D(Inputs, vec2(UV.x, FlipVertical ? 1 - UV.y : UV.y)) * Color;
    OutColor = Color;
}
		";

		struct Data
		{
			public Color Color;
		}

		Color _Color;
		public Color Color {
			get {
				return _Color;
			}
			set {
				SetData (new Data {
					Color = _Color
				});

				_Color = value;
			}
		}

		public TintEffect () :  base(_FragmentShader, _VertexShader)
		{
			
		}


	}


	class MainClass
	{
		



		static byte[] ReadImageData (System.Drawing.Bitmap image)
		{
			var bits = image.LockBits (new System.Drawing.Rectangle (0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			byte[] bytes = new byte[bits.Stride * bits.Height];
			System.Runtime.InteropServices.Marshal.Copy (bits.Scan0, bytes, 0, bytes.Length);
			image.UnlockBits (bits);
			return bytes;
		}

		static ISurface SurfaceFromBitmap (Context context, System.Drawing.Bitmap image, bool isA8 = false)
		{
			var bytes = ReadImageData (image);



			var surface = context.CreateSurface (image.Width, image.Height, bytes, isA8 ? ColorFormat.Alpha : ColorFormat.RGBA, SurfaceExtendMode.Repeat);
			return surface;
		}

		public static ISurface LoadImage (Context context, string path)
		{
			var image = new System.Drawing.Bitmap (path);

			return SurfaceFromBitmap (context, image);


		}

        private static void IRTFramebufferTransferArtifactTest(Context context, GameWindow gameWindow)
        {
            var windowTarget = new Zia.Gfx.Platform.OpenGL.GLWindowTarget (gameWindow);

            var text = new TextLayout()
            {
                Text = "Hello World",
                Font = new Font(File.ReadAllBytes("/home/ryan/Downloads/Raleway/Raleway-Light.ttf")) {
                },
                Size = 100,
                Wrap = false
            };

            text.UpdateLayout();

            var path = new PathGeometry ();
            path.Ellipse (new Vec2(0, 0), 500, 500, 100);

            var background = new PathGeometry();
            background.Rectangle(new Rect(0, 0, 1000, 1000));

            var projection = context.CreateTransform().Ortho (
                0, gameWindow.ClientSize.Width, gameWindow.ClientSize.Height, 0, -1000, 1000);



            var irt = context.CreateRenderableTarget (1000, 1000, ColorFormat.RGBA);

            context.PushTarget (irt);

            var transform = context.CreateTransform ().Translate (new Vec3 (500, 500, 0));
            context.FillGeometry (path, new LinearGradientBrush {
                Start = new Vec2(0, 0),
                End = new Vec2(1, 1),
                Stops = new List<GradientStop> {
                    new GradientStop(new Color(0, 0, 0, 1), 0),
                    new GradientStop(new Color(1, 1, 1, 1), 1)
                }
            }, transform * projection, 1);

      
            context.DrawTextLayout(text, new LinearGradientBrush {
                Start = new Vec2(0, 0),
                End = new Vec2(1, 1),
                Stops = new List<GradientStop> {
                    new GradientStop(new Color(0, 0, 0, 1), 0),
                    new GradientStop(new Color(1, 1, 1, 1), 1)
                }
            }, projection, 1);


            context.PopTarget ();

            context.Flush();

            context.PushTarget (windowTarget);
            context.Clear (new Color(1, 1, 1, 1));



            context.FillGeometry(background, new LinearGradientBrush {
                Start = new Vec2(0, 0),
                End = new Vec2(1, 1),
                Stops = new List<GradientStop> {
                    new GradientStop(new Color(0, 0, 0, 1), 0),
                    new GradientStop(new Color(1, 1, 1, 1), 1)
                }
            }, projection, 1);
            context.DrawSurface (irt, projection, 1000, 1000);



            context.Flush();
            irt.Dispose();
        }


		public static void Main (string[] args)
		{
			var gameWindow = new GameWindow (1000, 1000, new OpenTK.Graphics.GraphicsMode(32, 0, 0, 4));

			var context = new Context ();
			var windowTarget = new Zia.Gfx.Platform.OpenGL.GLWindowTarget (gameWindow);

			
            var geometry = new PathGeometry();
            var bboxGeometry = new PathGeometry();

            gameWindow.MouseDown += (sender, e) => 
            {
                if(e.Button == OpenTK.Input.MouseButton.Left)
                {
                    geometry.AddPoint(new Vec2(e.X, e.Y));
                }

                if(e.Button == OpenTK.Input.MouseButton.Right)
                {
                    geometry.NewFigure();
                }
            };

            var projection = context.CreateTransform().Ortho (
                0, gameWindow.ClientSize.Width, gameWindow.ClientSize.Height, 0, -1000, 1000);

            var image = LoadImage(context, "/home/ryan/Desktop/884903-scotland-wallpaper.jpg");



			gameWindow.RenderFrame += (sender, e) => {
               

                /*Console.WriteLine("RenderTime : {0}ms; FPS : {1};", gameWindow.RenderTime * 1000, 1000 / (gameWindow.RenderTime * 1000));
				context.PushTarget(windowTarget);
                context.PushClip(new Rect(200, 200, 200, 200));

                bboxGeometry.Clear();
                bboxGeometry.Rectangle(geometry.BoundingBox());
                context.DrawSurface(image, projection, gameWindow.ClientSize.Width, gameWindow.ClientSize.Height);
                context.FillGeometry(geometry, new SolidBrush(new Color(0, 0, 1, 1)), projection, 1f);
                context.FillGeometry(bboxGeometry, new SolidBrush(new Color(1, 0, 1, 1)), projection, 0.3f);



                context.Flush();*/

                IRTFramebufferTransferArtifactTest(context, gameWindow);

				gameWindow.SwapBuffers();

			};



			gameWindow.Run ();
			gameWindow.Dispose ();
		}
	}
}
