using System;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;
using Zia.Gfx.Geometry;
using Zia.Gfx.Platform;
using Zia.Gfx.RenderList;

namespace Zia.Gfx
{
	public class Context
	{
		private List<IRenderCommand> _CurrentCommands;
		private Stack<ITarget> _TargetStack = new Stack<ITarget>();
        private Stack<Rect> _ClipStack = new Stack<Rect>();


		private IPlatform _Platform;

		private Text.TextRenderer _TextRenderer;


		public Context ()
		{
			_TextRenderer = new Text.TextRenderer ();
			_Platform = new Platform.OpenGL.GLPlatform ();
			_CurrentCommands = new List<IRenderCommand> ();
		}



        public void DrawSurface(ISurface surface, Transform transform, float scaleX, float scaleY, float opacity = 1)
		{
			_CurrentCommands.Add (new SurfaceCommand {
				Surface = surface,
				Transform = CreateTransform() 
                    .Scale(new Vec3(scaleX, scaleY, 1)).Mul(transform),
                Opacity = opacity
			});


		}

        public void DrawEffect(Effect.IShaderEffect effect, Transform transform, float opacity = 1)
        {
            var size = effect.Size();
            DrawEffect(effect, transform, size.Item1, size.Item2, opacity);
        }

        public void DrawEffect(Effect.IShaderEffect effect, Transform transform, float scaleX, float scaleY, float opacity = 1)
        {
            _CurrentCommands.Add(new DrawEffectCommand {
                Effect = effect,
                Transform = CreateTransform() 
                    .Scale(new Vec3(scaleX, scaleY, 1)).Mul(transform),
                Opacity = opacity
            });
            
        }

		public void FillOpacityMask(ISurface surface, Brush brush, Transform transform, float opacity)
		{
			FillOpacityMask (surface, brush, transform, surface.Size().Item1, surface.Size().Item2, opacity);
		}

        public void FillOpacityMask(ISurface surface, Brush brush, Transform transform, float scaleX, float scaleY, float opacity)
		{
			_CurrentCommands.Add (new FillOpacityMask {
				Mask = surface,
				Transform = CreateTransform() 
					.Scale(new Vec3(scaleX, scaleY, 1)).Mul(transform),
					Brush =  brush,
                    Opacity = opacity
			});
		}


        public void DrawSurface(ISurface surface, Transform transform, float opacity = 1)
		{
			DrawSurface (surface, transform, surface.Size().Item1, surface.Size().Item2);
		}

        public void FillGeometry(GeometryBase geomtery, Brush brush, Transform transform, float opacity)
		{
			if (geomtery.Invalid) {
				var polyline = geomtery.GetPolyline ();

				List<Vec3> vertices;
				List<uint> indices;

				polyline.Tesselate (out indices, out vertices);
				geomtery.FillMesh = geomtery.FillMesh == null ? _Platform.CreateMesh () : geomtery.FillMesh;
				geomtery.FillMesh.Update (vertices, indices);
				geomtery.FillElementCount = indices.Count;
				geomtery.Invalid = false;
			}

			_CurrentCommands.Add (new GeometryCommand {
				Brush = brush,
				Mesh = geomtery.FillMesh,
				Transform = transform,
                ElementCount = geomtery.FillElementCount,
                Opacity = opacity
			});
		}

		public void DrawTextLayout(Text.TextLayout layout, Brush brush, Transform transform, float opacity)
		{
            var text = layout.CharsList
                .Select(charData => charData.C)
                    .ToArray();

            var locations = layout
                .CharsList
                    .Select(charData => charData.Location)
                    .ToList();

            _TextRenderer.DrawText(this, brush, transform, text, locations, layout.Font, layout.RealSize, opacity);
		}

        public void DrawText(string text, ICollection<Vec2> locations, Brush brush, Text.Font font, float size, Transform transform, float opacity)
		{
			_TextRenderer.DrawText (this, brush, transform, text, locations, font, size, opacity);
		}

        public void StrokeGeometry(GeometryBase geomtery, Brush brush, Transform transform, float opacity)
		{
			if (geomtery.StrokeInvalid) {
				var polyline = geomtery.GetPolyline ();

				List<Vec3> vertices = null;
				List<uint> indices = null;

				
					polyline.TesselateOutline (out indices, out vertices);
				


				geomtery.StrokeMesh = geomtery.StrokeMesh == null ? _Platform.CreateMesh () : geomtery.StrokeMesh;
				geomtery.StrokeMesh.Update (vertices, indices);
				geomtery.StrokeElementCount = indices.Count;

				geomtery.StrokeInvalid = false;
			}

			_CurrentCommands.Add (new GeometryCommand {
				Brush = brush,
				Mesh = geomtery.StrokeMesh,
				Transform = transform,
				ElementCount = geomtery.StrokeElementCount,
                Opacity = opacity
			});
		}

		public IRenderableTarget CreateRenderableTarget(int width, int height, ColorFormat colorFormat,
		                                                SurfaceExtendMode extendMode = SurfaceExtendMode.Clamp,
		                                                SurfaceFilterType filterType = SurfaceFilterType.Linear)
		{


			return _Platform.CreateRenderableTarget (width, height, colorFormat, extendMode, filterType);
		}

		public ISurface CreateSurface(int width, int height, byte[] data, ColorFormat colorFormat, 
		                                       SurfaceExtendMode extendMode = SurfaceExtendMode.Clamp,
		                                       SurfaceFilterType filterType = SurfaceFilterType.Linear)
		{
			return _Platform.CreateSurface (width, height, data, colorFormat, extendMode, filterType);
		}

		public Transform CreateTransform()
		{
			return _Platform.CreateTransform ();
		}

        public void EnableClip()
        {
            _CurrentCommands.Add (new RenderList.EnableClipCommand());
        }

        public void DisableClip()
        {
            _CurrentCommands.Add (new RenderList.DisableClipCommand());
        }

		public void PushClip(Rect clip)
        {


            _ClipStack.Push(clip);
            UpdateClip();
        }

        public void PopClip()
        {
            _ClipStack.Pop();;
            UpdateClip();
        }

        private void UpdateClip()
        {

            if (!_ClipStack.Any())
                return;

            var clip = _ClipStack.Peek();

            _CurrentCommands.Add (new RenderList.ClipCommand() {
                Width = (int)clip.Size.X,
                Height = (int)clip.Size.Y,

                X = (int)clip.Location.X,
                Y = (int)clip.Location.Y
            });
        }

		public void PushTarget(ITarget target)
		{
			_TargetStack.Push (target);
			UpdateTarget ();
		}

		public void PopTarget()
		{
			_TargetStack.Pop ();
			UpdateTarget ();
		}

		private void UpdateTarget()
		{
			if (_TargetStack.Count > 0) {
				_CurrentCommands.Add (new RenderList.SetTarget { Target = _TargetStack.Peek() });
			}
		}

		public void Clear(Color color)
		{
			_CurrentCommands.Add (new RenderList.ClearCommand() { ClearColor = color });
		}
		public void Flush()
		{
			_Platform.Render (_CurrentCommands);
			

            var error = OpenTK.Graphics.OpenGL.GL.GetError();

            if (error != OpenTK.Graphics.OpenGL.ErrorCode.NoError)
                throw new PlatformException(error.ToString());

            _TargetStack.Clear ();
            _CurrentCommands.Clear ();
		}



	}
}
