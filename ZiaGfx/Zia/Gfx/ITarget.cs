using System;

namespace Zia.Gfx
{
	public interface ITarget
	{
		int Width();
		int Height();

		ColorFormat Format();
	}
}

