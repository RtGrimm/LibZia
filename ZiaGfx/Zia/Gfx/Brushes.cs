using System;
using System.Collections.Generic;
using Zia.Common;
namespace Zia.Gfx
{
	public class SolidBrush : Brush
	{
		public Color Color {
			get;
			set;
		}

		public SolidBrush (Color color)
		{
			Color = color;
		}

		public SolidBrush ()
		{
			
		}
	}

	public class SurfaceBrush : Brush
	{
		public ISurface Surface {
			get;
			set;
		}



		public SurfaceBrush ()
		{
			
		}

		public SurfaceBrush (ISurface surface)
		{
			Surface = surface;
		}
	}

	public class GradientStop
	{
		public Color Color {
			get;
			set;
		}

		public float Offset {
			get;
			set;
		}

		public GradientStop (Color color, float offset)
		{
			Color = color;
			Offset = offset;
		}
	}

	public class GradientBrush : Brush
	{
		public ICollection<GradientStop> Stops {
			get;
			set;
		}

		public GradientBrush ()
		{
			Stops = new List<GradientStop> ();
		}
	}

	public class LinearGradientBrush : GradientBrush
	{
		public Vec2 Start {
			get;
			set;
		}

		public Vec2 End {
			get;
			set;
		}
	}

	public class RadialGradientBrush : GradientBrush
	{
		public Vec2 Center {
			get;
			set;
		}

		public Vec2 Radius {
			get;
			set;
		}
	}
}

