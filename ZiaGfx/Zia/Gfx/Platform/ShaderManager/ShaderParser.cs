using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Zia.Common;

namespace Zia.Gfx.Platform.ShaderManager
{

	class ShaderParser
	{
		public ICollection<FragmentDef> Fragments {
			get;
			private set;
		}

		public ICollection<ShaderDef> Shaders {
			get;
			private set;
		}

		public ShaderParser () {}

		public ShaderDef Shader(string name)
		{
			return Shaders.Single (shader => shader.Name == name);
		}

		public void Parse(string target)
		{
			Parse (XDocument.Parse (target).Root);
		}

		public void Parse (XElement root)
		{
			Fragments = root.Descendants ("Fragment").Select (
				node => ParseFragment<FragmentDef> (node, new FragmentDef ())).ToList ();

			Shaders = root.Descendants ("Shader")
				.Select (node => ParseShader (node)).ToList ();
		}

		private T ParseFragment<T> (XElement fragmentElement, T def) where T : FragmentDef
		{
			Func<XElement, IODef> nodeToIoDef = node => new IODef {
				Name = node.Attribute("name").Value,
				Type = node.Attribute("type").Value
			};

			def.InputBlocks = fragmentElement.Elements ("InputBlock")
				.Select (node => new KeyValuePair<String, ICollection<IODef>> (
					node.Attribute ("name").Value, 
					(ICollection<IODef>)(node.Descendants ("Input").Select (nodeToIoDef).ToList ())))
					.ToDictionary (pair => pair.Key, pair => pair.Value);

			def.Outputs = fragmentElement.Elements ("Output")
				.Select (nodeToIoDef).ToList ();

			def.Inputs = fragmentElement.Elements ("Input")
				.Select (nodeToIoDef).ToList ();

			def.Source = fragmentElement.Descendants ("Source").Single ().Value;
			def.Name = fragmentElement.Attribute ("name").Value;

			return def;
		}

		private ShaderDef ParseShader (XElement shaderElement)
		{
			var def = new ShaderDef ();
			var shaderType = shaderElement.Attribute ("type").Value;

			if (shaderType.ToLower () == "vertex") {
				def.Type = ShaderType.Vertex;
			} else if (shaderType.ToLower () == "fragment") {
				def.Type = ShaderType.Fragment;
			}

			ParseFragment (shaderElement, def);
			return def;
		}
	}

}
