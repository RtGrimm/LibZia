using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Zia.Common;

namespace Zia.Gfx.Platform.ShaderManager
{

	enum ShaderType
	{
		Vertex,
		Fragment
	}

	class IODef
	{
		public string Name {
			get;
			set;
		}

		public string Type {
			get;
			set;
		}

        public IODef(string type, string name)
        {
            this.Name = name;
            this.Type = type;
        }
        
	}

	class BlockDef
	{
		public string Name {
			get;
			set;
		}
	}

	class FragmentDef
	{

		public Dictionary<String, ICollection<IODef>> InputBlocks {
			get;
			set;
		}

		public ICollection<IODef> Inputs {
			get;
			set;
		}

		public ICollection<IODef> Outputs {
			get;
			set;
		}

		public ICollection<IODef> Uniforms {
			get;
			set;
		}

		public Dictionary<String, String> Defines {
			get;
			set;
		}

		public string Source {
			get;
			set;
		}

		public string Name {
			get;
			set;
		}

		public FragmentDef ()
		{
			InputBlocks = new Dictionary<String, ICollection<IODef>> ();
			Inputs = new List<IODef> ();
			Outputs = new List<IODef> ();
			Defines = new Dictionary<string, string>();
            Uniforms = new List<IODef>();
		}
	}

	class ShaderDef : FragmentDef
	{
		public ShaderType Type {
			get;
			set;
		}
	}

	class ShaderData
	{
		public ICollection<FragmentDef> Fragments {
			get;
			private set;
		}


		public ShaderData () {
			Fragments = new List<FragmentDef> ();
		}

        public ShaderData(ICollection<ShaderDef> shaders, ICollection<FragmentDef> fragments)
        {
            Fragments = fragments;
        }
	}

	interface IShaderLang
	{
		string Prefix  { get; }
		string Suffix  { get; }

		string BuildDefines (Dictionary<string, string> defines);

		string BuildInputs (ICollection<IODef> inputs);
		string BuildOutputs (ICollection<IODef> outputs);
		string BuildUniforms (ICollection<IODef> uniforms);

		string BuildInputBlock (String name, ICollection<IODef> inputs);
	}





	class ShaderBuilder
	{
		IShaderLang _Lang;

		private const string _IOBlockName = "IO";

		public ShaderBuilder (IShaderLang lang)
		{
			_Lang = lang;
		}

		private String ResolveFragment (FragmentDef fragment, List<FragmentDef> outFragments, ShaderData shaderData)
		{
			outFragments.Add (fragment);

			var matches = Regex.Matches (fragment.Source, "{{(?<FragmentName>.*)}}");
			var fragments = new Dictionary<String, String> ();

			foreach (var matchObj in matches) {
				var match = (Match)matchObj;
				var name = match.Groups ["FragmentName"].Value;

				if (name == _IOBlockName)
					continue;

				fragments [name] = ResolveFragment (shaderData.Fragments.Single (frag => frag.Name == name), outFragments, shaderData);
			}

			var source = fragment.Source;

			foreach (var fragmentPair in fragments) {
				source = source.Replace ("{{" + fragmentPair.Key + "}}", fragmentPair.Value);
			}

			return source;
		}

		void BuildInputBlocks (StringBuilder builder, List<FragmentDef> fragments)
		{
			foreach (var inputBlock in fragments.Select(frag => frag.InputBlocks).SelectMany(list => list)) {
				builder.AppendLine (_Lang.BuildInputBlock (inputBlock.Key, inputBlock.Value));
			}
		}

		string BuildIODefs(List<FragmentDef> fragments, 
		                 Func<FragmentDef, ICollection<IODef>> itemSelector, 
		                 Func<ICollection<IODef>, String> buildCallback)
		{
			return buildCallback (fragments.Select(itemSelector).SelectMany(list => list).ToList());
		}

		public String BuildShader (ShaderDef def, Dictionary<string, string> defines, ShaderData shaderData)
		{
			StringBuilder builder = new StringBuilder ();

			List<FragmentDef> fragments = new List<FragmentDef> ();
			var source = ResolveFragment (def, fragments, shaderData);

			var allDefines = defines.Concat (def.Defines)
				.Concat(fragments.Select(frag => frag.Defines)
				        .SelectMany(list => list))
					.ToDictionary(pair => pair.Key, pair => pair.Value);

			builder.AppendLine (_Lang.BuildDefines(allDefines));

			builder.AppendLine (_Lang.Prefix);


			var ioBuilder = new StringBuilder ();
			ioBuilder.AppendLine(BuildIODefs(fragments, (ioDef) => ioDef.Inputs, _Lang.BuildInputs));
			ioBuilder.AppendLine(BuildIODefs(fragments, (ioDef) => ioDef.Outputs, _Lang.BuildOutputs));
			ioBuilder.AppendLine(BuildIODefs(fragments, (ioDef) => 
                                             ioDef.Uniforms, _Lang.BuildUniforms));

			BuildInputBlocks (ioBuilder, fragments);

			if (source.Contains ("{{" + _IOBlockName + "}}")) {
				source = source.Replace ("{{" + _IOBlockName + "}}", ioBuilder.ToString ());
			} else {
				builder.AppendLine (ioBuilder.ToString());
			}


			builder.AppendLine (source);
			builder.AppendLine (_Lang.Suffix);

			return builder.ToString ();

		}
	}


}

