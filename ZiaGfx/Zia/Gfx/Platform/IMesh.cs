using System;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.Gfx.Platform
{
	internal interface IMesh : IDisposable
	{
		void Update(List<Vec3> vertices, List<uint> indices);
	}
}

