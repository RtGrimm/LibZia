using System;

namespace Zia.Gfx.Platform
{
	
	[Serializable]
	public class PlatformException : Exception
	{
		public PlatformException ()
		{

		}

		public PlatformException (string message) : base (message)
		{

		}

		public PlatformException (string message, Exception inner) : base (message, inner)
		{

		}

		protected PlatformException (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base (info, context)
		{

		}
	}
}

