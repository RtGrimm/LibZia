using System;
using System.Collections.Generic;
using Zia.Common;
using Zia.Gfx.RenderList;

namespace Zia.Gfx.Platform
{
	internal interface IPlatform
	{
		ISurface CreateSurface (int width, int height, byte[] data, ColorFormat format, SurfaceExtendMode extendMode, SurfaceFilterType filterType);
	    IRenderableTarget CreateRenderableTarget (int width, int height, ColorFormat format, SurfaceExtendMode extendMode, SurfaceFilterType filterType);

		Transform CreateTransform ();

		void Render(List<IRenderCommand> commands);
		IMesh CreateMesh ();
	}
}

