using System;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Zia.Common;
using Zia.Gfx.Effect;
using Zia.Gfx.Platform.OpenGL.Helper;

namespace Zia.Gfx.Platform.OpenGL
{
	class EffectRenderer : IDisposable
	{
		class CacheItem
		{
			public int TextureId {
				get;
				set;
			}

			public int FramebufferId {
				get;
				set;
			}
		}

		[StructLayout(LayoutKind.Sequential)]
		struct Vertex
		{
			public float X, Y, Z;
			public float U, V;

			public Vertex (Vec3 location, Vec2 uv)
			{
				X = location.X;
				Y = location.Y;
				Z = location.Z;

				U = uv.X;
				V = uv.Y;
			}
		}

		private Cache<Tuple<int, int>, CacheItem> _Cache;
		private Mesh<Vertex, uint> _Mesh;

		public EffectRenderer ()
		{
			_Cache = new Cache<Tuple<int, int>, CacheItem> (CreateCache);

			var inputLayout = new InputLayout ();
			inputLayout.Elements.Add (new InputLayout.Element (VertexAttribPointerType.Float, 3));
			inputLayout.Elements.Add (new InputLayout.Element (VertexAttribPointerType.Float, 2));

			_Mesh = new Mesh<Vertex, uint> (inputLayout);

			_Mesh.UpdateVertices (new Vertex[] {
				new Vertex(new Vec3(-1, 1, 0), new Vec2(0, 1)),
				new Vertex(new Vec3(1, 1, 0), new Vec2(1, 1)),
				new Vertex(new Vec3(-1, -1, 0), new Vec2(0, 0)),
				new Vertex(new Vec3(1, -1, 0), new Vec2(1, 0))
			});
		}

		static int CreateFramebuffer (int texture)
		{
			var framebuffer = GL.GenFramebuffer ();
			var framebufferRestore = new Helper.RestoreFramebuffer ();

			GL.BindFramebuffer (FramebufferTarget.Framebuffer, framebuffer);

			GL.FramebufferTexture2D (FramebufferTarget.Framebuffer, 
			                         FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, texture, 0);

			if (GL.CheckFramebufferStatus (FramebufferTarget.Framebuffer) != FramebufferErrorCode.FramebufferComplete) {
				throw new PlatformException ("Framebuffer error.");
			}

			framebufferRestore.Restore ();

			return framebuffer;
		}

		static int CreateTexture (Tuple<int, int> size)
		{
			var texture = GL.GenTexture ();

			GL.BindTexture (TextureTarget.Texture2D, texture);

			GL.TexParameter (TextureTarget.Texture2D, 
			                 TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

			GL.TexParameter (TextureTarget.Texture2D,
			                 TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);

			GL.TexImage2D (TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, size.Item1, size.Item2, 
			               0, PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);

			GL.BindTexture (TextureTarget.Texture2D, 0);

			return texture;
		}

		CacheItem CreateCache (Tuple<int, int> size)
		{
			var texture = CreateTexture (size);
			var framebuffer = CreateFramebuffer (texture);

			return new CacheItem {
				TextureId = texture,
				FramebufferId = framebuffer
			};
		}

		public void Dispose ()
		{
			_Cache.AllItems ().ForEach (item => {
				GL.DeleteTexture (item.TextureId);
				GL.DeleteFramebuffer (item.FramebufferId);
			});
		}

		CacheItem GetOutputCache (IShaderEffect effect, bool isLast, System.Collections.Generic.List<GLTextureSurface> inputs)
		{
			CacheItem outputCache;
			Func<CacheItem, bool> itemPred = item =>  {
				return !inputs.Any (input => input.TextureHandle () == item.TextureId);
			};
			if (isLast)
				outputCache = _Cache.PullItems (1, effect.Size (), itemPred).Single ();
			else
				outputCache = _Cache.GetItems (1, effect.Size (), itemPred).Single ();

			return outputCache;
		}

		private void CompileEffect(GLShaderEffect effect)
		{
			if (effect.Program != -1)
				return;

			effect.Program = ShaderBuilder.Build (new[] {
				new ShaderBuilder.Shader(effect.FragmentShader, ShaderType.FragmentShader),
				new ShaderBuilder.Shader(effect.VertexShader, ShaderType.VertexShader)
			});
		}

		private GLTextureSurface RenderEffectInternal (IShaderEffect effect, bool isLast)
		{
			if (isLast)
				_Mesh.Bind ();

			var inputs = effect.Inputs.Select (ResovleInput).ToList();
			var outputCache = GetOutputCache (effect, isLast, inputs);
			var error = GL.GetError ();

			var size = effect.Size ();


			var restoreFramebuffer = new RestoreFramebuffer ();

			var glEffect = (GLShaderEffect)effect;

			CompileEffect (glEffect);

			GL.BindFramebuffer (FramebufferTarget.Framebuffer, outputCache.FramebufferId);
			GL.UseProgram (glEffect.Program);

			glEffect.UniformBuffer.BindToIndex (glEffect.Program, 0);
            UniformBuffer.BindBlock(glEffect.Program, "Data", 0);

			GL.Viewport (0, 0, size.Item1, size.Item2);
			GL.ClearColor (0, 0, 0, 0);
			GL.Clear (ClearBufferMask.ColorBufferBit);


			var i = 0;

			foreach (var input in inputs) {
				GL.ActiveTexture ((TextureUnit)(((int)TextureUnit.Texture0) + i));
				GL.BindTexture (TextureTarget.Texture2D, input.TextureHandle ());
				i++;
			}


			GL.Uniform1 (GL.GetUniformLocation (glEffect.Program, "Inputs"), 
			             inputs.Count, Enumerable.Range (0, inputs.Count).ToArray());

			GL.Uniform1 (GL.GetUniformLocation (glEffect.Program, "FlipVertical"), 
			             inputs.Count, inputs.Select(input => Convert.ToInt32(input.FilpVertical())).ToArray());

			GL.DrawArrays (PrimitiveType.TriangleStrip, 0, 4);



			GL.UseProgram (0);

			restoreFramebuffer.Restore ();

			if(isLast)
				_Mesh.Unbind ();

			return new GLTextureSurface (outputCache.TextureId, 
                                         outputCache.FramebufferId, 
                                         false, size.Item1, size.Item2);
		}

		public ISurface RenderEffect (IShaderEffect effect)
		{
			return RenderEffectInternal (effect, true);
		}

		private GLTextureSurface ResovleInput (IEffectInput input)
		{
			var surface = input as ISurface;

			if (surface != null)
				return (GLTextureSurface)surface;

			var effect = input as IShaderEffect;

			if (effect != null)
				return RenderEffectInternal (effect, false);

			return null;
		}
	}
}

