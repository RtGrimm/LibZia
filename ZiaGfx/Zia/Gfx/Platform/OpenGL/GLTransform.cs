using System;
using OpenTK;
using Zia.Common;

namespace Zia.Gfx.Platform.OpenGL
{
	internal class GLTransform : Transform
	{
		Matrix4 _Matrix;

		internal Matrix4 Matrix {
			get {
				return this._Matrix;
			}
		}

		public override Transform Perspective (float fovy, float aspect, float zNear, float zFar)
		{
			return new GLTransform (_Matrix * Matrix4.CreatePerspectiveFieldOfView (fovy, aspect, zNear, zFar));
		}

		public override Transform LookAt (Vec3 eye, Vec3 target, Vec3 up)
		{
			return new GLTransform (_Matrix * Matrix4.LookAt (
				eye.X, eye.Y, eye.Z, 
				target.X, target.Y, target.Z, 
				up.X, up.Y, up.Z));
		}

		public override Transform RotateX (float degAngle)
		{
			return new GLTransform (_Matrix * Matrix4.CreateRotationX (ToRadians (degAngle)));
		}

		public override Transform RotateY (float degAngle)
		{
			return new GLTransform (_Matrix * Matrix4.CreateRotationY (ToRadians (degAngle)));
		}

		public override Transform RotateZ (float degAngle)
		{
			return new GLTransform (_Matrix * Matrix4.CreateRotationZ (ToRadians (degAngle)));
		}

		private static float ToRadians (float angle)
		{
			return (float)(Math.PI / 180) * angle;
		}

		public override Transform Ortho (float left, float right, float bottom, float top, float znear, float zfar)
		{
			return new GLTransform (_Matrix * Matrix4.CreateOrthographicOffCenter (left, right, bottom, top, znear, zfar));
		}

		public override Transform Translate (Zia.Common.Vec3 offset)
		{
			return new GLTransform (_Matrix * Matrix4.CreateTranslation (new Vector3 (offset.X, offset.Y, offset.Z)));
		}

		public override Transform Scale (Zia.Common.Vec3 scale)
		{
			return new GLTransform (_Matrix * Matrix4.CreateScale (new Vector3 (scale.X, scale.Y, scale.Z)));
		}

		public override Vec3 Mul (Vec3 right)
		{
			var result = Vector4.Transform (new Vector4 (right.X, right.Y, right.Z, 1), _Matrix);
			return new Vec3 (result.X, result.Y, result.Z);
		}

		public override Vec2 Mul (Vec2 right)
		{
			var vec3 = Mul (new Vec3 (right.X, right.Y, 1));
			return new Vec2 (vec3.X, vec3.Y);
		}

		public override Transform Mul (Transform right)
		{
			var glRight = (GLTransform)right;

   			return new GLTransform (Matrix * glRight.Matrix);
		}

		public override Transform Invert ()
		{
			return new GLTransform (Matrix4.Invert (_Matrix));
		}

		public GLTransform (Matrix4 matrix)
		{
			_Matrix = matrix;
		}
	}
}

