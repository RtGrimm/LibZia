using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Zia.Gfx.Platform.ShaderManager;

namespace Zia.Gfx.Platform.OpenGL
{

	
	class GL440 : IShaderLang
	{
		public string BuildUniforms (ICollection<IODef> uniforms)
		{
			return BuildIO (uniforms, "uniform");
		}

		public string BuildInputs (ICollection<IODef> inputs)
		{
			return BuildIO (inputs, "in");
		}

		public string BuildOutputs (ICollection<IODef> outputs)
		{
			return BuildIO (outputs, "out");
		}

		private string BuildIO (ICollection<IODef> ioDefs, string prefix)
		{
			return ioDefs.Aggregate ("", (str, def) => 
			                         str + String.Format ("{0} {1} {2};\n", prefix, def.Type, def.Name));
		}

		public string BuildDefines (Dictionary<string, string> defines)
		{
			return defines.Aggregate ("", (str, pair) => 
			                          str + String.Format ("#define {0} {1}\n", pair.Key, pair.Value));
		}

		public string BuildInputBlock (string name, ICollection<IODef> inputs)
		{
			return String.Format("uniform {0} {{ {1} }};", name, inputs.Aggregate
			                     ("", (acc, def) => acc + BuildIO(new[] {def}, "")));
		}

		public string Prefix {
			get {
				return "#version 440 core";
			}
		}

		public string Suffix {
			get {
				return "";
			}
		}
	}

}
