using System;
using System.Linq;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using Zia.Gfx.Platform.OpenGL.Helper;
using Zia.Common;
using System.Collections.Generic;

namespace Zia.Gfx.Platform.OpenGL
{
	internal class OpacityMaskRenderer : IDisposable
	{
		struct Vertex
		{
			//public Vec3 Location;
			//public  Vec2 UV;

			public float X, Y, Z;
			public float U, V;

			public Vertex (Vec3 location, Vec2 uv)
			{
				//Location = location;
				//UV = uv;

				X = (float)location.X;
				Y = (float)location.Y;
				Z = (float)location.Z;

				U = (float)uv.X;
				V = (float)uv.Y;
			}
		}

		private Mesh<Vertex, uint> _Mesh;
		private ShaderBrushManager _BrushManager;



		public OpacityMaskRenderer ()
		{
			_BrushManager = new ShaderBrushManager (true);

			var inputLayout = new InputLayout ();
			inputLayout.Elements.Add (new InputLayout.Element (VertexAttribPointerType.Float, 3));
			inputLayout.Elements.Add (new InputLayout.Element (VertexAttribPointerType.Float, 2));

			_Mesh = new Mesh<Vertex, uint> (inputLayout);

			Vertex[] data = new Vertex[] {
				new Vertex(new Vec3(0, 0, 0), new Vec2(0, 1)),
				new Vertex(new Vec3(1, 0, 0), new Vec2(1, 1)),
				new Vertex(new Vec3(0, 1, 0), new Vec2(0, 0)),
				new Vertex(new Vec3(1, 1, 0), new Vec2(1, 0))

			};

			_Mesh.UpdateVertices (data);
		}

		public void Render (ICollection<RenderList.FillOpacityMask> commands, ColorFormat colorFormat)
		{
			_BrushManager.Bind ();
			_Mesh.Bind ();

			_BrushManager.UpdateIsA8 (true);

			RenderList.FillOpacityMask lastCommand = null;

			_BrushManager.UpdateColorFormat (colorFormat);

			foreach (var command in commands) {

				var transform = ((GLTransform)command.Transform).Matrix;

                if (lastCommand == null || command.Opacity != lastCommand.Opacity)
                    _BrushManager.UpdateOpacity (command.Opacity);
			
				if (lastCommand == null || command.Mask != lastCommand.Mask)
					_BrushManager.UpdateA8Texture (command.Mask);

				if (lastCommand == null || command.Transform != lastCommand.Transform)
					_BrushManager.UpdateTransform (transform);

				if (lastCommand == null || command.Brush != lastCommand.Brush)
					_BrushManager.UpdateFromBrush (command.Brush);


				GL.DrawArrays (PrimitiveType.TriangleStrip, 0, 4);

				//GL.BindTexture (TextureTarget.Texture2D, 0);

				lastCommand = command;
			}

			_Mesh.Unbind ();
			_BrushManager.Unbind ();
		}


		public void Dispose ()
		{
			_BrushManager.Dispose ();

		}
	}
}

