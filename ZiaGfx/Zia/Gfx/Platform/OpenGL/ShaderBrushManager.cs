using System;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using Zia.Gfx.Platform.ShaderManager;

namespace Zia.Gfx.Platform.OpenGL
{
	internal class ShaderBrushManager : IDisposable
	{
		private int _Program;

		public ShaderBrushManager (bool isA8)
		{
			var manager = new ShaderBuilder(new GL440());

            var shaderData = new ShaderData();

            shaderData.Fragments.Add(Shaders.BrushShader.Shader);
            shaderData.Fragments.Add(Shaders.CommonShader.ColorFormatHelper);


			var fragmentShader = "";
			var vertexShader = "";

			if (isA8) {
                fragmentShader = manager.BuildShader 
                    (Shaders.BrushShaderMain.FragmentA8, new Dictionary<string, string> (), shaderData);

                vertexShader = manager.BuildShader 
                    (Shaders.BrushShaderMain.VertexA8, new Dictionary<string, string> (), shaderData);
			} else {
                fragmentShader = manager.BuildShader 
                    (Shaders.BrushShaderMain.GeometryFragment, new Dictionary<string, string> (), shaderData);

                vertexShader = manager.BuildShader 
                    (Shaders.BrushShaderMain.GeometryVertex, new Dictionary<string, string> (), shaderData);
			}




			_Program = Helper.ShaderBuilder.Build (new [] {
				new Helper.ShaderBuilder.Shader(fragmentShader, OpenTK.Graphics.OpenGL.ShaderType.FragmentShader),
				new Helper.ShaderBuilder.Shader(vertexShader, OpenTK.Graphics.OpenGL.ShaderType.VertexShader)
			});


		}

		public void Dispose ()
		{
			if (_Program != -1) {
				GL.DeleteProgram (_Program);
				_Program = -1;
			}
		}

		public void UpdateIsA8 (bool isA8)
		{
			GL.Uniform1 (GL.GetUniformLocation (_Program, "IsA8"), isA8 ? 1 : 0); 
		}

		public void UpdateA8Texture (ISurface surface)
		{
			var glTextureSurface = ((GLTextureSurface)surface);

			GL.ActiveTexture (TextureUnit.Texture1);
			GL.BindTexture (TextureTarget.Texture2D, glTextureSurface.TextureHandle ());

			GL.Uniform1 (GL.GetUniformLocation (_Program, "A8AlphaTexture"), 1);
			GL.Uniform1 (GL.GetUniformLocation (_Program, "FlipA8TextureVertical"), 
			             glTextureSurface.FilpVertical () ? 1 : 0);

			GL.Uniform1 (GL.GetUniformLocation (_Program, "A8InputColorFormat"), (int)surface.Format ());
		}

		public void UpdateColorFormat (ColorFormat format)
		{
			GL.Uniform1 (GL.GetUniformLocation (_Program, "OutColorFormat"), (int)format);
		}

        public void UpdateOpacity(float opacity)
        {
            GL.Uniform1(GL.GetUniformLocation(_Program, "Opacity"), opacity);
        }

		public void UpdateFromBrush (Brush brush)
		{
			var matrix = brush.Transform != null ? ((GLTransform)brush.Transform).Matrix : Matrix4.Identity;


			matrix.Invert ();

		
			GL.UniformMatrix4 (GL.GetUniformLocation (_Program, "BrushTransform"), false, ref matrix);


			if (brush is SolidBrush) {
				UpdateSolidBrush ((SolidBrush)brush);
			} else if (brush is LinearGradientBrush) {

				var gradientBrush = (LinearGradientBrush)brush;

				UpdateGradient (gradientBrush);

				UpdateLinearGradient (gradientBrush);
			} else if (brush is RadialGradientBrush) {
				var gradientBrush = (RadialGradientBrush)brush;
				UpdateGradient (gradientBrush);
				UpdateRadialGradient (gradientBrush);
			} else if (brush is SurfaceBrush) {
				UpdateSurfaceBrush ((SurfaceBrush)brush);
			}
		}

		public void UpdateSolidBrush (SolidBrush brush)
		{
			var brushType = GL.GetUniformLocation (_Program, "BrushType");
			GL.Uniform1 (brushType, 0); 
			GL.Uniform4 (GL.GetUniformLocation (_Program, "SolidBrushColor"), brush.Color.R, brush.Color.G, brush.Color.B, brush.Color.A);
		}

		public void UpdateSurfaceBrush (SurfaceBrush brush)
		{
			GL.ActiveTexture (TextureUnit.Texture0);
			GL.BindTexture (TextureTarget.Texture2D, ((GLTextureSurface)brush.Surface).TextureHandle ());

			GL.Uniform1 (GL.GetUniformLocation (_Program, "BrushType"), 1); 
			GL.Uniform1 (GL.GetUniformLocation (_Program, "ImageBrushTexture"), 0); 
			var i = GL.GetUniformLocation (_Program, "ImageBrushSize");
			GL.Uniform2 (i, new Vector2 (brush.Surface.Size().Item1, brush.Surface.Size().Item2)); 
		}

		public void UpdateBoundingBox(Rect bbox)
		{
			GL.Uniform4 (GL.GetUniformLocation (_Program, "BoundingBox"), 
			             new Vector4 (bbox.Location.X, bbox.Location.Y, bbox.Size.X, bbox.Size.Y));
		}

		private void UpdateGradient (GradientBrush brush)
		{

			var colors = brush.Stops
				.Select (stop => new[] { stop.Color.R, stop.Color.G, stop.Color.B, stop.Color.A })
				.SelectMany (array => array).ToArray ();

			var offsets = brush.Stops.Select (stop => stop.Offset).ToArray ();

			GL.Uniform1 (GL.GetUniformLocation (_Program, "GradientStopCount"), brush.Stops.Count); 

			GL.Uniform1 (GL.GetUniformLocation (_Program, "GradientStopOffsets"), offsets.Length, offsets); 


			GL.Uniform4 (GL.GetUniformLocation (_Program, "GradientStopColors"), colors.Length / 4, colors);

		}

		private void UpdateRadialGradient (RadialGradientBrush brush)
		{
			GL.Uniform1 (GL.GetUniformLocation (_Program, "BrushType"), 2); 

			GL.Uniform2 (GL.GetUniformLocation (_Program, "RadialGradientBrushCenter"), 
			             new Vector2 (brush.Center.X, brush.Center.Y)); 

			GL.Uniform2 (GL.GetUniformLocation (_Program, "RadialGradientRadius"), 
			             new Vector2 (brush.Radius.Y, brush.Radius.Y)); 
		}

		private void UpdateLinearGradient (LinearGradientBrush brush)
		{
			GL.Uniform1 (GL.GetUniformLocation (_Program, "BrushType"), 3); 

			GL.Uniform2 (GL.GetUniformLocation (_Program, "LinearGradientStart"), 
			             new Vector2 (brush.Start.X, brush.Start.Y)); 

			GL.Uniform2 (GL.GetUniformLocation (_Program, "LinearGradientEnd"), 
			             new Vector2 (brush.End.X, brush.End.Y)); 
		}

		public void UpdateTransform (Matrix4 transform)
		{
			GL.UniformMatrix4 (GL.GetUniformLocation (_Program, "Transform"), false, ref transform);
		}

		public void Bind ()
		{
			GL.UseProgram (_Program);
		}

		public void Unbind ()
		{
			GL.UseProgram (0);
		}
	}
}

