using System;
using OpenTK;

namespace Zia.Gfx.Platform.OpenGL
{
	public class GLWindowTarget : IGLTarget
	{
		GameWindow _Window;

		public GLWindowTarget (GameWindow window)
		{
			_Window = window;
		}

		public int Width ()
		{
			return _Window.ClientSize.Width;
		}

		public int Height ()
		{
			return _Window.ClientSize.Height;
		}

		public ColorFormat Format ()
		{
			return ColorFormat.RGBA;
		}

		public int FramebufferHandle ()
		{
			return 0;
		}
	}
}

