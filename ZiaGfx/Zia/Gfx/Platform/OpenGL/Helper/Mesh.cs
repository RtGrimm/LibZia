using System;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Zia.Gfx.Platform.OpenGL.Helper
{
	internal class Mesh<Vertex, Index> : IDisposable where Vertex : struct where Index : struct
	{
		private int _VertexArray;
		private int _VertexBuffer;
		private int _IndexBuffer;

		private int _VertexBufferSize;
		private int _IndexBufferSize;

		private bool _UseIndexBuffer;

		public Mesh (InputLayout inputLayout, bool useIndexBuffer = false)
		{
			_UseIndexBuffer = useIndexBuffer;
			_VertexArray = GL.GenVertexArray ();
			_VertexBuffer = GL.GenBuffer ();

			if(useIndexBuffer)
				_IndexBuffer = GL.GenBuffer ();

			GL.BindVertexArray (_VertexArray);


			GL.BindBuffer (BufferTarget.ArrayBuffer, _VertexBuffer);

			if(useIndexBuffer)
				GL.BindBuffer (BufferTarget.ElementArrayBuffer, _IndexBuffer);


			inputLayout.Apply ();

			GL.BindVertexArray(0);
		}

		public void Dispose ()
		{




			if (_VertexBuffer != -1) {
				GL.DeleteBuffer (_VertexBuffer);
				_VertexBuffer = -1;
			}

			if (_VertexArray != -1) {
				GL.DeleteVertexArray (_VertexArray);
				_VertexArray = -1;
			}

			if (_UseIndexBuffer && _IndexBuffer != -1) {
				GL.DeleteBuffer (_IndexBuffer);
				_IndexBuffer = -1;
			}
		}

		public void Bind()
		{
			GL.BindVertexArray (_VertexArray);
		}

		public void Unbind()
		{
			GL.BindVertexArray (0);
		}

		public void Update(Vertex[] vertices, Index[] indices)
		{
			UpdateVertices (vertices);
			UpdateIndices (indices);
		}

		public void UpdateVertices(Vertex[] vertices)
		{
			var vertexSize = System.Runtime.InteropServices
				.Marshal.SizeOf (typeof(Vertex)) * vertices.Length;

			GL.BindBuffer (BufferTarget.ArrayBuffer, _VertexBuffer);

			if (_VertexBufferSize < vertexSize) {
				GL.BufferData (BufferTarget.ArrayBuffer, (IntPtr)(vertexSize), vertices, BufferUsageHint.StaticDraw);
				_VertexBufferSize = vertexSize;
			} else {
				GL.BufferSubData (BufferTarget.ArrayBuffer, (IntPtr)0, (IntPtr)vertexSize, vertices);
			}

			GL.BindBuffer (BufferTarget.ArrayBuffer, 0);
		}

		public void UpdateIndices(Index[] indices)
		{
			if (!_UseIndexBuffer)
				return;

			var indexSize = System.Runtime.InteropServices
				.Marshal.SizeOf (typeof(Index)) * indices.Length;

			GL.BindBuffer (BufferTarget.ElementArrayBuffer, _IndexBuffer);

			if (_IndexBufferSize < indexSize) {
				GL.BufferData (BufferTarget.ElementArrayBuffer, (IntPtr)(indexSize), indices, BufferUsageHint.StaticDraw);
				_IndexBufferSize = indexSize;
			} else {
				GL.BufferSubData (BufferTarget.ElementArrayBuffer, (IntPtr)0, (IntPtr)indexSize, indices);
			}

			GL.BindBuffer (BufferTarget.ElementArrayBuffer, 0);
		}
	}
}

