using System;
using System.Runtime.InteropServices;

using OpenTK.Graphics.OpenGL;

namespace Zia.Gfx.Platform.OpenGL.Helper
{
	class UniformBuffer : IDisposable
	{
		private int _Buffer;

		private int _BufferSize;

		public UniformBuffer ()
		{
			_Buffer = GL.GenBuffer ();

			GL.BindBuffer (BufferTarget.UniformBuffer, _Buffer);
			GL.BufferData (BufferTarget.UniformBuffer, (IntPtr)0, IntPtr.Zero, BufferUsageHint.DynamicDraw);
			GL.BindBuffer (BufferTarget.UniformBuffer, 0);
		}

		public void Dispose ()
		{
			if (_Buffer != -1) {
				GL.DeleteBuffer (_Buffer);
				_Buffer = -1;
			}
		}

		public static void BindBlock(int program, string name, int index)
		{
			GL.UniformBlockBinding (program, GL.GetUniformBlockIndex (program, name), index);
		}

		public void BindToIndex(int program, int index)
		{
			GL.BindBufferBase (BufferTarget.UniformBuffer, index, _Buffer);
		}

		public void UpdateUniformBuffer<T>(T data) where T : struct
		{
			GL.BindBuffer (BufferTarget.UniformBuffer, _Buffer);
			var newBufferSize = Marshal.SizeOf (typeof(T));

			if (newBufferSize > _BufferSize) {
				GL.BufferData (BufferTarget.UniformBuffer, (IntPtr)newBufferSize, ref data, BufferUsageHint.DynamicDraw);
				_BufferSize = newBufferSize;
			} else {
				GL.BufferSubData (BufferTarget.UniformBuffer, (IntPtr)0, (IntPtr)newBufferSize, ref data);
			}

			GL.BindBuffer (BufferTarget.UniformBuffer, 0);
		}
	}

}

