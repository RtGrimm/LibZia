using System;
using OpenTK.Graphics.OpenGL;

namespace  Zia.Gfx.Platform.OpenGL.Helper
{
	internal class RestoreFramebuffer
	{
		private int _LastFramebuffer;
		private FramebufferTarget _Target;

		public RestoreFramebuffer (FramebufferTarget target = FramebufferTarget.Framebuffer)
		{
			_Target = target;
			CaptureCurrentFramebuffer ();
		}

		public void CaptureCurrentFramebuffer()
		{
			_LastFramebuffer = GL.GetInteger (GetPName.FramebufferBinding);
		}

		public void Restore()
		{
			GL.BindFramebuffer (_Target, _LastFramebuffer);
		}
	}
}

