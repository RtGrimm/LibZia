using System;


using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;

namespace Zia.Gfx.Platform.OpenGL.Helper
{
	internal class InputLayout
	{
		public class Element
		{
			public VertexAttribPointerType Type {
				get;
				set;
			}

			public int Size {
				get;
				set;
			}

			public Element (VertexAttribPointerType type, int size)
			{
				Type = type;
				Size = size;
			}
		}

		public InputLayout ()
		{
			Elements = new List<Element> ();
		}


		public void Apply()
		{
			int stride = (int)Elements.Sum((element) => element.Size * _Sizes [element.Type]);

			for (int i = 0; i < Elements.Count; i++) {
				GL.EnableVertexAttribArray (i);
			}

			int offset = 0;
			int index = 0;


			foreach (var element in Elements) {
				var size = (int)_Sizes [element.Type] * element.Size;

				GL.VertexAttribPointer(index, element.Size, element.Type, false, stride, offset);

				offset += size;
				index++;
			}

		}

		static private Dictionary<VertexAttribPointerType, uint> _Sizes = new Dictionary<VertexAttribPointerType, uint>()
		{
			{ VertexAttribPointerType.Float, sizeof(float) },
			{ VertexAttribPointerType.Byte, sizeof(byte) },
			{ VertexAttribPointerType.Int, sizeof(int) },
			{ VertexAttribPointerType.Double, sizeof(double) },
			{ VertexAttribPointerType.UnsignedInt, sizeof(uint) },
			{ VertexAttribPointerType.UnsignedShort, sizeof(ushort) }
		};

		public ICollection<Element> Elements {
			get;
			set;
		}
	}
}

