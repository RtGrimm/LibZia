using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Collections.Generic;


namespace Zia.Gfx.Platform.OpenGL.Helper
{
	class ProgramError
	{
		public ProgramError (int program)
		{
			_Program = program;
		}

		public bool HasError {
			get {
				int error = 0;
				GL.GetProgram (_Program, ProgramParameter.LinkStatus, out error);

				return error != 1;
			}
		}

		public string ErrorLog {
			get {
				return GL.GetProgramInfoLog (_Program);
			}
		}

		private int _Program;
	}

	class ShaderError
	{
		public ShaderError (int shader)
		{
			_Shader = shader;
		}

		public bool HasError {
			get {
				int error = 0;
				GL.GetShader (_Shader, ShaderParameter.CompileStatus, out error);

				return error != 1;
			}
		}

		public string ErrorLog {
			get {
				return GL.GetShaderInfoLog (_Shader);
			}
		}

		private int _Shader;
	}

	class ShaderBuilder
	{
		public class Shader
		{
			public ShaderType Type {
				get;
				set;
			}

			public String Source {
				get;
				set;
			}

			public Shader (string source, ShaderType type)
			{
				Type = type;
				Source = source;
			}
		}

		public static int Build (IEnumerable<Shader> shaders)
		{
			var program = GL.CreateProgram ();

			var shaderIds = new List<int> ();

			foreach (var shader in shaders) {
				var id = GL.CreateShader (shader.Type);
				GL.ShaderSource (id, shader.Source);
				GL.CompileShader (id);
				GL.AttachShader (program, id);

				var error = new ShaderError(id);
				if (error.HasError) {
					throw new ArgumentException (String.Format("Shader error:{0}", error.ErrorLog));
				}

				shaderIds.Add (id);
			}

			GL.LinkProgram (program);

			var programError = new ProgramError (program);

			if(programError.HasError)
			{
				throw new ArgumentException (String.Format("Program error:{0}", programError.ErrorLog));
			}

			shaderIds.ForEach ((int id) => GL.DeleteShader (id));

			return program;
		}
	}
}

