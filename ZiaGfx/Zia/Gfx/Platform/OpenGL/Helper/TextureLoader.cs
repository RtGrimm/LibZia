using System;

using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace Zia.Gfx.Platform.OpenGL.Helper
{
	class TextureLoader
	{
		public static int Load(byte[] data)
		{
			var bitmap = new System.Drawing.Bitmap (new System.IO.MemoryStream (data));
			var bitmapData = bitmap.LockBits (new Rectangle (0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			var texture = GL.GenTexture ();

			GL.BindTexture (TextureTarget.Texture2D, texture);

			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);

			GL.TexImage2D (TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmap.Width, bitmap.Height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);

			GL.BindTexture (TextureTarget.Texture2D, 0);



			return texture;
		}
	}
}

