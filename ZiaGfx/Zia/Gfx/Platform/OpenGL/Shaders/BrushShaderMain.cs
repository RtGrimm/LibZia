using System;
using Zia.Gfx.Platform.ShaderManager;
using System.Collections.Generic;

namespace Zia.Gfx.Platform.OpenGL.Shaders
{
    static class BrushShaderMain
    {
        public static ShaderDef VertexA8
        {
            get
            {
                return new ShaderDef
                {
                    Name = "A8BrushVertexShader",
                    Type = ShaderType.Vertex,
                    Inputs = new List<IODef>
                    {
                        new IODef("vec3", "Pos"),
                        new IODef("vec2", "TexCoord")
                    },
                    Outputs = new List<IODef>
                    {
                        new IODef("vec2", "UV")
                    },
                    Uniforms = new List<IODef>
                    {
                        new IODef("mat4", "Transform")
                    },
                    Source = @"
void main()
{
   vec4 pos = vec4(Pos, 1);
   gl_Position = Transform * pos;
   UV = TexCoord;
}
                    "
                };
            }
        }

        public static ShaderDef FragmentA8
        {
            get
            {
                return new ShaderDef
                {
                    Name = "A8BrushFragmentShader",
                    Type = ShaderType.Fragment,
                    Inputs = new List<IODef>
                    {
                        new IODef("vec2", "UV")
                    },
                    Outputs = new List<IODef>
                    {
                        new IODef("vec4", "OutColor")
                    },
                    Uniforms = new List<IODef>
                    {
                        new IODef("bool", "FlipA8TextureVertical"),
                        new IODef("sampler2D", "A8AlphaTexture"),

                        new IODef("int", "A8InputColorFormat"),
                        new IODef("int", "OutColorFormat"),
                        new IODef("float", "Opacity")
                    },
                    Source = @"

layout(origin_upper_left) in vec4 gl_FragCoord;
{{IO}}

{{Brushes}}
{{ColorFormatHelper}}

 void main()
 {
    vec2 coord = gl_FragCoord.xy;
    vec4 color = vec4(0);
    RenderBrushColor(vec2(UV.x, 1 - UV.y), color);

    vec4 outColor = vec4(0);

    float alpha = 0;
    vec4 maskColor = texture(A8AlphaTexture,
               vec2(UV.x, FlipA8TextureVertical ? (1 - UV.y) : (UV.y)));

    GetMaskAlpha(A8InputColorFormat, maskColor, alpha);

    outColor = vec4(color.r, color.g, color.b, alpha * color.a);
    outColor.a *= Opacity;

    GetOutputColor(OutColorFormat, outColor, OutColor);
 }
                    "
                };
            }
        }


        public static ShaderDef GeometryVertex
        {
            get
            {
                return new ShaderDef
                {
                    Name = "GeometryBrushVertexShader",
                    Type = ShaderType.Vertex,
                    Inputs = new List<IODef>
                    {
                        new IODef("vec3", "Pos")
                    },
                    Outputs = new List<IODef>
                    {
                        new IODef("vec2", "UV")
                    },
                    Uniforms = new List<IODef>
                    {
                        new IODef("mat4", "Transform"),
                        new IODef("vec4", "BoundingBox")
                    },
                    Source = @"
void main()
{
    vec4 pos = vec4(Pos, 1);
    gl_Position = Transform * pos;

    vec2 location = BoundingBox.xy;
    vec2 size = BoundingBox.zw;

    vec2 relLocation = pos.xy - location;

    UV = relLocation / size;
}
                    "
                };
            }
        }

        public static ShaderDef GeometryFragment
        {
            get
            {
                return new ShaderDef
                {
                    Name = "GeometryBrushFragmentShader",
                    Type = ShaderType.Fragment,
                    Inputs = new List<IODef>
                    {
                        new IODef("vec2", "UV")
                    },
                    Outputs = new List<IODef>
                    {
                        new IODef("vec4", "OutColor")
                    },
                    Uniforms = new List<IODef>
                    {
                        new IODef("int", "OutColorFormat"),
                        new IODef("float", "Opacity")
                    },
                    Source = @"
layout(origin_upper_left) in vec4 gl_FragCoord;
{{IO}}

 {{Brushes}}
{{ColorFormatHelper}}

 void main()
 {
    vec4 color = vec4(0);
    RenderBrushColor(UV, color);
    color.a *= Opacity;

    

    GetOutputColor(OutColorFormat, color, OutColor);

 }
                    "
                };
            }
        }

    }
}
