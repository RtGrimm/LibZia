using System;
using Zia.Gfx.Platform.ShaderManager;
using System.Collections.Generic;

namespace Zia.Gfx.Platform.OpenGL.Shaders
{
    static class SurfaceRendererShader
    {
        public static ShaderDef Vertex
        {
            get
            {
                return new ShaderDef
                {
                    Name = "SurfaceRendererVertex",
                    Inputs = new List<IODef> {
                        new IODef("vec3", "VertLocation"),
                        new IODef("vec2", "VertUV")
                    },
                    Outputs = new List<IODef> {
                        new IODef("vec2", "UV")
                    },
                    Uniforms = new List<IODef> {
                        new IODef("mat4", "Transform")
                    },
                    Type = ShaderType.Vertex,
                    Source = @"
void main()
{
    vec4 pos = vec4(VertLocation, 1);

    UV = VertUV;
    gl_Position = Transform * pos;
}
"
                };
            }
        }

        public static ShaderDef Fragment
        {
            get
            {
                return new ShaderDef
                {
                    Inputs = new List<IODef>() {
                        new IODef("vec2", "UV")
                    },
                    Type = ShaderType.Fragment,
                    Uniforms = new List<IODef>() {
                        new IODef("sampler2D", "Tex"),
                        new IODef("bool", "FlipVertical"),
                        new IODef("bool", "IsA8"),
                        new IODef("int", "OutColorFormat"),
                        new IODef("float", "Opacity"),
                    },
                    Outputs = new List<IODef>() {
                        new IODef("vec4", "OutColor")
                    },
                    Source = @"
{{ColorFormatHelper}}

void main()
{
    vec2 uv = vec2(UV.x, FlipVertical ? 1 - UV.y : UV.y);

    vec4 outColor = vec4(0);

    if(IsA8)
    {
        outColor = vec4(1, 1, 1, texture2D(Tex, uv).r);
    }
    else
    {
        outColor = texture2D(Tex, uv);
    }


    vec4 finalColor = vec4(0);
    GetOutputColor(OutColorFormat, outColor, finalColor);
    
    OutColor = finalColor * Opacity;
}"
                };
            }
        }
    }
}
