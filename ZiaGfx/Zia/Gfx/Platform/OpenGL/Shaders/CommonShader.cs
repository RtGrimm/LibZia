using System;
using Zia.Gfx.Platform.ShaderManager;

namespace Zia.Gfx.Platform.OpenGL.Shaders
{
	static class CommonShader
	{
        public static FragmentDef ColorFormatHelper {
            get {
                return new FragmentDef
                {
                    Name = "ColorFormatHelper",
                    Source = @"
void GetMaskAlpha(int colorFormat, vec4 color, out float alpha)
{
    //RGBA
    if(colorFormat == 0)
    {
        alpha = color.a;
    }
    //Alpha
    else if(colorFormat == 1)
    {
        alpha = color.r;
    }
}

void GetOutputColor(int colorFormat, vec4 color, out vec4 outColor)
{
    //RGBA
    if(colorFormat == 0)
    {
        outColor = color;
    }
    //Alpha
    else if(colorFormat == 1)
    {
        outColor = vec4(color.a, 0, 0, color.a);
    }
}
"
                };
            }
        }

		
	}
}

