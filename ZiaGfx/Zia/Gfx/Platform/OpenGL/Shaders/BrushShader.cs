using System;
using Zia.Gfx.Platform.ShaderManager;
using System.Collections.Generic;

namespace Zia.Gfx.Platform.OpenGL.Shaders
{
    static class BrushShader
    {
        public static FragmentDef Shader
        {
            get
            {
                return new FragmentDef
                {
                    Name = "Brushes",
                    Uniforms = new List<IODef> {
                        new IODef("sampler2D", "ImageBrushTexture"),
                        new IODef("vec2", "ImageBrushSize"),
                        new IODef("vec4", "SolidBrushColor"),
                        new IODef("vec2", "RadialGradientBrushCenter"),
                        new IODef("vec2", "RadialGradientRadius"),
                        new IODef("vec2", "LinearGradientStart"),
                        new IODef("vec2", "LinearGradientEnd"),
                        new IODef("int", "GradientStopCount"),
                        new IODef("vec4", "GradientStopColors[10]"),
                        new IODef("float", "GradientStopOffsets[10]"),
                        new IODef("int", "BrushType"),
                        new IODef("mat4", "BrushTransform")
                        }
                    ,
                    Source = @"
void TransformCoord(vec2 target, out vec2 transformedCoord, float lastValue = 1)
{
    transformedCoord = ( BrushTransform * vec4(target, 0, lastValue) ).xy;
    //transformedCoord = target;
}

 void SolidColorBrush(out vec4 color)
 {
    color = SolidBrushColor;
 }

 void ImageBrush(vec2 coord, out vec4 color)
 {
    color = texture(ImageBrushTexture, coord);
 }

 void OutputGradient(float offset, out vec4 color)
 {
    if(offset <= GradientStopOffsets[0])
    {
        color = GradientStopColors[0];
        return;
    }

    if(offset >= GradientStopOffsets[GradientStopCount - 1])
    {
        color = GradientStopColors[GradientStopCount - 1];
        return;
    }

    for(int i = 0; i < GradientStopCount - 1; i++)
    {
        float startOffset = GradientStopOffsets[i];
        float stopOffset = GradientStopOffsets[i + 1];

        if(offset > GradientStopOffsets[i])
        {
            float localOffset = ((offset - startOffset) / (stopOffset - startOffset));
            color = mix(GradientStopColors[i], GradientStopColors[i + 1], localOffset);
        }
    }
 }

 void RadialGradientBrush(vec2 coord, out vec4 color)
 {

    vec2 center = RadialGradientBrushCenter;
    vec2 radius = RadialGradientRadius;

    float offset = length(abs(center - coord) / radius);
    OutputGradient(offset, color);
 }

 void LinearGradientBrush(vec2 coord, out vec4 color)
 {
    vec2 startPoint = LinearGradientStart;
    vec2 endPoint = LinearGradientEnd;

    vec2 v = endPoint - startPoint;
    float d = length(v);
    v = normalize(v);

    vec2 v0 = coord - startPoint;
    float offset = dot(v0, v);
    offset = clamp(offset / d, 0, 1);

    OutputGradient(offset, color);
 }

 void RenderBrushColor(vec2 coord, out vec4 color)
 {
    vec2 transCoord = UV;
    TransformCoord(coord, transCoord, 1);

    if(BrushType == 0)
    {
        SolidColorBrush(color);
    }
    else if(BrushType == 1)
    {
        ImageBrush(transCoord, color);
    }
    else if(BrushType == 2)
    {
        RadialGradientBrush(transCoord, color);
    }
    else if(BrushType == 3)
    {
        LinearGradientBrush(transCoord, color);
    }
 }
"
                };
            }
        }
    }
}
