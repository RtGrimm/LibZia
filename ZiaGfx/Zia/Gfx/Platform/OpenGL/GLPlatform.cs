using System;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;
using OpenTK.Graphics.OpenGL;

namespace Zia.Gfx.Platform.OpenGL
{
	internal class GLPlatform : IPlatform
	{
		private OpacityMaskRenderer _OpacityMaskRenderer = new OpacityMaskRenderer ();
		private SurfaceRenderer _SurfaceRenderer = new SurfaceRenderer ();
		private GeometryRenderer _GeometryRenderer = new GeometryRenderer ();
		private EffectRenderer _EffectRenderer = new EffectRenderer ();
		private FramebufferTransfer _FramebufferTransfer = new FramebufferTransfer(20);

		public GLPlatform ()
		{
		}

		public Transform CreateTransform ()
		{
			return new GLTransform (OpenTK.Matrix4.Identity);
		}



	

		public ISurface CreateSurface (int width, int height, byte[] data, ColorFormat format, SurfaceExtendMode extendMode, SurfaceFilterType filterType)
		{
			return new GLTextureSurface (format, extendMode, filterType, width, height, data, false, true);
		}

		public IRenderableTarget CreateRenderableTarget (int width, int height, ColorFormat format, SurfaceExtendMode extendMode, SurfaceFilterType filterType)
		{
			return new GLTextureSurface (format, extendMode, filterType, width, height, null, true);
		}

		public IMesh CreateMesh ()
		{
			return new GLMesh ();
		}

		private List<List<RenderList.IRenderCommand>> Batch (List<RenderList.IRenderCommand> commands)
		{
			var buckets = new List<List<RenderList.IRenderCommand>> ();
			var currentBucket = new List<RenderList.IRenderCommand> ();
			Type currentBucketType = null;

			Action reset = () => {

				if (currentBucket.Count > 0)
					buckets.Add (currentBucket);
				currentBucket = new List<Zia.Gfx.RenderList.IRenderCommand> ();
				currentBucketType = null;
			};

			foreach (var command in commands) {
				if (command is RenderList.FillOpacityMask || 
					command is RenderList.GeometryCommand || 
					command is RenderList.ImageCommand) {

					currentBucket.Add (command);

					if (currentBucketType != null && currentBucketType != command.GetType ()) {
						reset ();
						currentBucket.Add (command);
					}

					currentBucketType = command.GetType ();

				} else {
					reset ();
					buckets.Add (new List<Zia.Gfx.RenderList.IRenderCommand> {
						command
					});
				}
			}

			reset ();

			return buckets;
		}

		public void Render (List<RenderList.IRenderCommand> commands)
		{
			List<List<RenderList.IRenderCommand>> buckets = null;
			buckets = Batch (commands).Where (bucket => bucket.Any ()).ToList ();

			ITarget lastTarget = null;



			foreach (var bucket in buckets) {
				var commandType = bucket [0].GetType ();


				if (commandType != typeof(RenderList.SetTarget) && lastTarget == null) {
					throw new InvalidOperationException ("Can not render without target.");
				}

				var typeSwitch = new TypeSwitch ();

				typeSwitch.Case<RenderList.GeometryCommand> ((command) => {
					_GeometryRenderer.Render (bucket.OfType<RenderList.GeometryCommand> ().ToList (), lastTarget.Format ());
				});

				typeSwitch.Case<RenderList.FillOpacityMask> ((command) => {
					_OpacityMaskRenderer.Render (bucket.OfType<RenderList.FillOpacityMask> ().ToList (), lastTarget.Format ());
				});

				typeSwitch.Case<RenderList.ImageCommand> ((command) => {
					_SurfaceRenderer.Render (bucket.OfType<RenderList.ImageCommand> ().ToList (), lastTarget.Format ());
				});

				typeSwitch.Case<RenderList.SetTarget> ((command) => {


					if(lastTarget != null)
						_FramebufferTransfer.EndFrame();

					var target = (IGLTarget)((RenderList.SetTarget)command).Target;
					lastTarget = target;

					var width = target.Width ();
					GL.Viewport (0, 0, width, target.Height ());

					_FramebufferTransfer.BeginFrame(Tuple.Create(target.Width(), target.Height()), target.FramebufferHandle());
				});

				typeSwitch.Case<RenderList.ClearCommand> ((command) => {
					var color = ((RenderList.ClearCommand)command).ClearColor;

					if (lastTarget.Format () == ColorFormat.Alpha) {
						GL.ClearColor (color.A, 0, 0, 1);
					} else {
						GL.ClearColor (color.R, color.G, color.B, color.A);
					}

					GL.Clear (ClearBufferMask.ColorBufferBit);
				});

				typeSwitch.Case<RenderList.ClipCommand> ((command) => {
					var clipCommand = (RenderList.ClipCommand)command;
                    GL.Scissor (clipCommand.X, lastTarget.Height() - (clipCommand.Height + clipCommand.Y), clipCommand.Width, clipCommand.Height);
				});

                typeSwitch.Case<RenderList.EnableClipCommand> ((command) => {
                    GL.Enable(EnableCap.ScissorTest);
                });

                typeSwitch.Case<RenderList.DisableClipCommand> ((command) => {
                    GL.Disable(EnableCap.ScissorTest);
                });

				if(commandType == typeof(RenderList.SetTarget))
					typeSwitch.Run (bucket.Last());
				else
					typeSwitch.Run (bucket [0]);

			}

			_FramebufferTransfer.EndFrame ();
		}
	}
}

