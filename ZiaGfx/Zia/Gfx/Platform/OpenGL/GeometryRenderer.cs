using System;
using OpenTK.Graphics.OpenGL;
using Zia.Common;
using Zia.Gfx.RenderList;
using System.Collections.Generic;
using System.Linq;

namespace Zia.Gfx.Platform.OpenGL
{
	internal class GeometryRenderer : IDisposable
	{
		private ShaderBrushManager _BrushManager;

		public GeometryRenderer ()
		{
			_BrushManager = new ShaderBrushManager (false);
		}

		public void Dispose ()
		{
			_BrushManager.Dispose ();
		}

		private Rect CalcBoundingBox(List<Vec3> vertices)
		{
			float maxX = 0, maxY = 0, minX = 0, minY = 0;

			vertices.Each (vert => {
				maxX = MathUtil.Max (maxX, vert.X);
				maxY = MathUtil.Max (maxY, vert.Y);
				minX = MathUtil.Min (minX, vert.X);
				minY = MathUtil.Min (minY, vert.Y);
			});

			return new Rect (minX, minY, maxX - minX, maxY - minY);
		}

		public void Render(ICollection<GeometryCommand> commands, ColorFormat targetFormat)
		{
			GeometryCommand lastCommand = null;

			_BrushManager.Bind ();
			_BrushManager.UpdateColorFormat (targetFormat);



			foreach (var command in commands) {
				var mesh = (GLMesh)command.Mesh;
				var boundingBox = CalcBoundingBox (mesh.Vertices);

                if(lastCommand == null || command.Opacity != lastCommand.Opacity)
                    _BrushManager.UpdateOpacity (command.Opacity);

				if(lastCommand == null || command.Brush != lastCommand.Brush)
					_BrushManager.UpdateFromBrush (command.Brush);

				if(lastCommand == null || command.Transform != lastCommand.Transform)
					_BrushManager.UpdateTransform (((GLTransform)(command.Transform)).Matrix);

				_BrushManager.UpdateBoundingBox (boundingBox);

				mesh.Bind ();
				GL.DrawElements (PrimitiveType.Triangles, command.ElementCount,
				                 DrawElementsType.UnsignedInt, IntPtr.Zero);
				mesh.Unbind ();



			}

		

			_BrushManager.Unbind ();
		}
	}
}

