using System;
using System.Linq;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using Zia.Gfx.Platform.OpenGL.Helper;
using Zia.Common;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Zia.Gfx.Platform.ShaderManager;

namespace Zia.Gfx.Platform.OpenGL
{
	internal class SurfaceRenderer : IDisposable
	{
		private int _Program;
		private Mesh<Vertex, uint> _Mesh;

        private EffectRenderer _EffectRenderer = new EffectRenderer();

		[StructLayout(LayoutKind.Sequential)]
		struct Vertex
		{
			public float X, Y, Z;
			public float U, V;

			public Vertex (Vec3 location, Vec2 uv)
			{
				X = location.X;
				Y = location.Y;
				Z = location.Z;

				U = uv.X;
				V = uv.Y;
			}
		}

		public SurfaceRenderer ()
		{
			var manager = new ShaderManager.ShaderBuilder (new GL440());
            var shaderData = new ShaderData();
            shaderData.Fragments.Add(Shaders.CommonShader.ColorFormatHelper);

            var vertexShader = manager.BuildShader (Shaders.SurfaceRendererShader.Vertex, new Dictionary<string, string>(), shaderData);
            var fragmentShader = manager.BuildShader (Shaders.SurfaceRendererShader.Fragment, new Dictionary<string, string>(), shaderData);

            _Program = Helper.ShaderBuilder.Build (new List<Helper.ShaderBuilder.Shader> {
				new Helper.ShaderBuilder.Shader(vertexShader, OpenTK.Graphics.OpenGL.ShaderType.VertexShader),
				new Helper.ShaderBuilder.Shader(fragmentShader, OpenTK.Graphics.OpenGL.ShaderType.FragmentShader)
			});

			var inputLayout = new InputLayout ();
			inputLayout.Elements.Add (new InputLayout.Element (VertexAttribPointerType.Float, 3));
			inputLayout.Elements.Add (new InputLayout.Element (VertexAttribPointerType.Float, 2));

			_Mesh = new Mesh<Vertex, uint> (inputLayout);

			Vertex[] data = new Vertex[] {
				new Vertex(new Vec3(0, 0, 0), new Vec2(0, 1)),
				new Vertex(new Vec3(1, 0, 0), new Vec2(1, 1)),
				new Vertex(new Vec3(0, 1, 0), new Vec2(0, 0)),
				new Vertex(new Vec3(1, 1, 0), new Vec2(1, 0))

			};

			_Mesh.UpdateVertices (data);
		}

		public void Render (ICollection<RenderList.ImageCommand> commands, ColorFormat format)
		{
            var surfaceCommands = commands.Select(command => {
                if(command is RenderList.SurfaceCommand)
                    return command as RenderList.SurfaceCommand;

                var effectCommand = (command as RenderList.DrawEffectCommand);
                var effect = effectCommand.Effect as GLShaderEffect;

                if(effect.CacheSurface == null && !effect.RenderInvalid)
                    return null;

                if(effect.RenderInvalid) 
                {
                    if(effect.CacheSurface != null)
                    {
                        effect.CacheSurface.Dispose();
                    }

                    effect.CacheSurface = _EffectRenderer.RenderEffect(effect);
                    effect.RenderInvalid = false;
                }


                return new RenderList.SurfaceCommand {
                    Surface = effect.CacheSurface,
                    Transform = effectCommand.Transform
                };

            }).Where(command => command != null).ToList();

            //GL.BlendFunc (BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha);
			GL.UseProgram (_Program);
			_Mesh.Bind ();

			GL.Uniform1 (GL.GetUniformLocation (_Program, "Tex"), 0);

            foreach (var command in surfaceCommands) {
				var transform = ((GLTransform)command.Transform).Matrix;
				var glTextureSurface = ((GLTextureSurface)command.Surface);
				var texture = glTextureSurface.TextureHandle ();

                if (glTextureSurface.FramebufferHandle() != -1)
                {
                    GL.BlendFunc (BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha);
                }

				GL.ActiveTexture (TextureUnit.Texture0);
				GL.BindTexture (TextureTarget.Texture2D, texture);

				GL.UniformMatrix4 (GL.GetUniformLocation (_Program, "Transform"), false, ref transform);
				GL.Uniform1 (GL.GetUniformLocation (_Program, "FlipVertical"), Convert.ToInt32(glTextureSurface.FilpVertical()));
				GL.Uniform1 (GL.GetUniformLocation (_Program, "IsA8"), (glTextureSurface.Format() == ColorFormat.Alpha) ? 1 : 0);
				GL.Uniform1 (GL.GetUniformLocation (_Program, "OutColorFormat"), (int)format);
                GL.Uniform1 (GL.GetUniformLocation (_Program, "Opacity"), command.Opacity);

				GL.DrawArrays (PrimitiveType.TriangleStrip, 0, 4);

				GL.BindTexture (TextureTarget.Texture2D, 0);

                if (glTextureSurface.FramebufferHandle() != -1)
                {
                    GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                }
			}

			_Mesh.Unbind ();
			GL.UseProgram (0);
            //GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
		}



		public void Dispose ()
		{
			if (_Program != -1) {
				GL.DeleteProgram (_Program);
				_Program = -1;
			}
		}
	}
}


