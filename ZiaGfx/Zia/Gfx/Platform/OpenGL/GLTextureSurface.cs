using System;
using System.Runtime.InteropServices;
using OpenTK.Graphics.OpenGL;
using Zia.Common;

namespace Zia.Gfx.Platform.OpenGL
{
    [StructLayout(LayoutKind.Sequential)]
    struct Byte4
    {
        public byte R, G, B, A;
    }

    internal class GLTextureSurface : IRenderableTarget, IGLTarget
    {
        private int _Texture;

        private int _Framebuffer;

        private bool _FlipVertical;

        private int _Width;

        private int _Height;

        private ColorFormat _Format;

        private SurfaceExtendMode _ExtendMode;

        private SurfaceFilterType _FilterMode;

        public GLTextureSurface(ColorFormat format, SurfaceExtendMode extendMode, SurfaceFilterType filterType, int width, int height,
                           byte[] data, bool useFramebuffer, bool flipVertical = false)
        {
            _ExtendMode = extendMode;
            _FlipVertical = flipVertical;
            _Width = width;
            _Height = height;

            _Format = format;

            CreateTexture(format, width, height, data);

            if (useFramebuffer)
                CreateFramebuffer();
            else
                _Framebuffer = -1;
        }

        public GLTextureSurface(int textureHandle, int framebufferHandle, bool flipVertical, int width, int height)
        {
            _Texture = textureHandle;
            _Framebuffer = framebufferHandle;
            _FlipVertical = flipVertical;
            _Height = height;
            _Width = width;
        }

        private void CreateFramebuffer()
        {
            _Framebuffer = GL.GenFramebuffer();

            var framebufferRestore = new Helper.RestoreFramebuffer();

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _Framebuffer);

            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, 
                            FramebufferAttachment.ColorAttachment0, 
                            TextureTarget.Texture2D, _Texture, 0);

            if (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) 
                != FramebufferErrorCode.FramebufferComplete)
            {
                throw new PlatformException("Framebuffer error.");
            }

            framebufferRestore.Restore();
        }

        private void CreateTexture(ColorFormat format, int width, int height, byte[] data)
        {
            _Texture = GL.GenTexture();

            GL.BindTexture(TextureTarget.Texture2D, _Texture);

            var filterMode = _FilterMode == SurfaceFilterType.Linear ? 
				(int)TextureMagFilter.Linear : (int)TextureMagFilter.Nearest;

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, filterMode);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, filterMode);

            int wrapMode = 0;

            if (_ExtendMode == SurfaceExtendMode.Clamp)
            {
                wrapMode = (int)TextureWrapMode.Clamp;
            }
            else if (_ExtendMode == SurfaceExtendMode.Repeat)
            {
                wrapMode = (int)TextureWrapMode.Repeat;
            }

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapR, wrapMode);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, wrapMode);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, wrapMode);



            if (format == ColorFormat.RGBA)
            {
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, data);

                if (data == null)
                    GL.ClearTexImage(_Texture, 0, PixelFormat.Rgba, PixelType.UnsignedByte, new uint[] {0});
                   
            }
            else if (format == ColorFormat.Alpha)
            {
                GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.R8, width, height, 0, PixelFormat.Red, PixelType.UnsignedByte, data);
                GL.PixelStore(PixelStoreParameter.UnpackAlignment, 4);

                if (data == null)
                    GL.ClearTexImage(_Texture, 0, PixelFormat.Red, PixelType.UnsignedByte, new uint[] { 0 });
            }

            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public bool HitTest(Zia.Common.Vec2 point)
        {
            if (_Framebuffer == -1)
                CreateFramebuffer();

            var restoreFramebuffer = new Helper.RestoreFramebuffer(FramebufferTarget.ReadFramebuffer);

            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, _Framebuffer);

			

            var color = new Byte4();
            unsafe
            {
                GL.ReadPixels((int)point.X, _Height - (int)point.Y, 1, 1, _Format == ColorFormat.RGBA ? 
				               PixelFormat.Rgba : PixelFormat.Red, PixelType.UnsignedByte, ref color);

            }

            restoreFramebuffer.Restore();

            return color.A > 0;
        }

        public void Dispose()
        {


            if (_Texture != -1)
            {

                GL.DeleteTexture(_Texture);
                _Texture = -1;
            }

            if (_Framebuffer != -1)
            {
                GL.DeleteFramebuffer(_Framebuffer);	
                _Framebuffer = -1;
            }
        }

        public bool FilpVertical()
        {
            return _FlipVertical;
        }

        public int TextureHandle()
        {
            return _Texture;
        }

        public int FramebufferHandle()
        {
            return _Framebuffer;
        }

        public Tuple<int, int> Size()
        {
            return Tuple.Create(_Width, _Height);
        }

        public ColorFormat Format()
        {
            return _Format;
        }

        int ITarget.Width()
        {
            return _Width;
        }

        int ITarget.Height()
        {
            return _Height;
        }

        ColorFormat ITarget.Format()
        {
            return _Format;
        }

        public override string ToString()
        {
            return string.Format("[GLTextureSurface: _Texture={0}, _Framebuffer={1}]", _Texture, _Framebuffer);
        }
        
    }
}

