using System;

namespace Zia.Gfx.Platform.OpenGL
{
	public interface IGLTarget : ITarget
	{
		int FramebufferHandle ();
	}
}

