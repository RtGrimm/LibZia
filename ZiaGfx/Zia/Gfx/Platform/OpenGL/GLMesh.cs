using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using Zia.Gfx.Platform.OpenGL.Helper;
using Zia.Common;

namespace Zia.Gfx.Platform.OpenGL
{
	internal class GLMesh : IMesh
	{
		private Mesh<Vec3, uint> _Mesh;

		public List<Vec3> Vertices {
			get;
			private set;
		}

		public GLMesh ()
		{
			var inputLayout = new InputLayout ();
			inputLayout.Elements.Add (new InputLayout.Element(VertexAttribPointerType.Float, 3));

			_Mesh = new Mesh<Vec3, uint> (inputLayout, true);
		}

		public void Dispose ()
		{
			_Mesh.Dispose ();
		}

		internal void Bind()
		{
			_Mesh.Bind ();
		}

		internal void Unbind()
		{
			_Mesh.Unbind ();
		}

		public void Update (List<Vec3> vertices, List<uint> indices)
		{
			Vertices = vertices;
			_Mesh.Update (vertices.ToArray(), indices.ToArray());
		}
	}
}

