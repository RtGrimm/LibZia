using System;
using System.Linq;
using Zia.Gfx.Platform.OpenGL.Helper;
using Zia.Common;
using OpenTK.Graphics.OpenGL;

namespace Zia.Gfx.Platform.OpenGL
{
	public class FramebufferTransfer : IDisposable
	{
		class CacheItem
		{
			public int RenderbufferColorId {
				get;
				set;
			}
		}

		private Cache<Tuple<int, int>, CacheItem> _TextureCache;


		private Tuple<int, int> _CurrentSize;

		private int _Framebuffer;
		private int _TargetFramebuffer;

		private RestoreFramebuffer _FramebufferRestore = new RestoreFramebuffer ();

        int _MaxTextureCacheCount;

        public FramebufferTransfer (int maxTextureCacheCount)
		{
            _MaxTextureCacheCount = maxTextureCacheCount;
			_Framebuffer = GL.GenFramebuffer ();

			_TextureCache = new Cache<Tuple<int, int>, CacheItem> ((size) => {
				var colorRenderBuffer = GL.GenRenderbuffer();

				GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, colorRenderBuffer);
				GL.RenderbufferStorageMultisample(RenderbufferTarget.Renderbuffer, 8, RenderbufferStorage.Rgba32f, size.Item1, size.Item2);
				GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, 0);

				return new CacheItem {
					RenderbufferColorId = colorRenderBuffer
				};
			});
		}

		public void Dispose ()
		{
			if (_Framebuffer != -1) {
				GL.DeleteFramebuffer (_Framebuffer);
				_Framebuffer = -1;
			}

			ClearCache();


		}

        private void ClearCache()
        {
            _TextureCache.AllItems().Each(cacheItem => 
            {
                GL.DeleteRenderbuffer(cacheItem.RenderbufferColorId);
            });
            _TextureCache.Clear();
        }

        private void CleanCache()
        {
            if (_TextureCache.AllItems().Count > _MaxTextureCacheCount)
            {
                ClearCache();
            }
        }

		public void BeginFrame(Tuple<int, int> size, int targetFramebuffer)
		{
           
            GL.Enable (EnableCap.Blend);
            GL.Enable (EnableCap.Multisample);

            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            //GL.BlendFuncSeparate (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha, BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha); 

            CleanCache();

			_TargetFramebuffer = targetFramebuffer;

			GL.BindFramebuffer (FramebufferTarget.DrawFramebuffer, targetFramebuffer);
			if (_TargetFramebuffer == 0)
				return;

            GL.BlendFuncSeparate (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha, BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha);


			_FramebufferRestore.CaptureCurrentFramebuffer ();

			GL.BindFramebuffer (FramebufferTarget.Framebuffer, _Framebuffer);

			if (_CurrentSize != size) {
				var cacheItem = _TextureCache.GetItems (1, size, (texture) => true).Single();
				_CurrentSize = size;

				GL.FramebufferRenderbuffer (FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, 
				                            RenderbufferTarget.Renderbuffer, cacheItem.RenderbufferColorId);
			}

			GL.ClearColor (0, 0, 0, 0);
			GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
		}

		public void EndFrame()
		{
			if (_TargetFramebuffer == 0)
				return;

           
			

			GL.BindFramebuffer (FramebufferTarget.DrawFramebuffer, _TargetFramebuffer);
			GL.BindFramebuffer (FramebufferTarget.ReadFramebuffer, _Framebuffer);

			GL.ClearColor (0, 0, 0, 0);
			GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

			GL.BlitFramebuffer (0, 0, _CurrentSize.Item1, _CurrentSize.Item2, 0, 0, _CurrentSize.Item1, _CurrentSize.Item2, 
			                    ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Linear);

			_FramebufferRestore.Restore ();


            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
		}
	}
}

