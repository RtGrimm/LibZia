using System;
using System.Linq;
using System.Collections.Generic;

namespace Zia.Gfx.Platform.OpenGL
{
	class Cache<TKey, TValue>
	{
		private Dictionary<TKey, List<TValue>> _Cache = 
			new Dictionary<TKey, List<TValue>> ();


		private Func<TKey, TValue> _CreateTextureCallback;

		public Cache (Func<TKey, TValue> createTextureCallback)
		{
			_CreateTextureCallback = createTextureCallback;
		}

		public List<TValue> PullItems(int count, TKey key, Func<TValue, bool> pred)
		{
			var items = GetItems (count, key, pred);
			foreach (var item in items) {
				_Cache [key].Remove (item);
			}
			return items;
		}

		public void Clear()
		{
			_Cache.Clear ();
		}

		public List<TValue> AllItems()
		{
			return _Cache.Values.SelectMany (list => list).ToList();
		}

		public List<TValue> GetItems(int count, TKey key, Func<TValue, bool> pred)
		{

			if (!_Cache.ContainsKey (key)) {
				_Cache [key] = new List<TValue>();
			}

			var cacheList = _Cache[key];

			var items = cacheList.Where (pred).ToList();

			var newItems = new List<TValue>();

			for (int i = 0; i < count - items.Count; i++) {
				newItems.Add (_CreateTextureCallback(key));
			}

			cacheList.AddRange (newItems);

			return items.Concat (newItems).Take(count).ToList();
		}
	}

}

