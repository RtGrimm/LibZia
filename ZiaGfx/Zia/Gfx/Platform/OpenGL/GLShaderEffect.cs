using System;
using System.Linq;
using System.Collections.Generic;
using Zia.Gfx.Effect;
using Zia.Gfx.Platform.OpenGL.Helper;
using OpenTK.Graphics.OpenGL;

namespace Zia.Gfx.Platform.OpenGL
{
	public abstract class GLShaderEffect : IShaderEffect
	{
		internal UniformBuffer UniformBuffer {
			get;
			private set;
		}

        int _Program;
		internal int Program
        {
            get
            {
                return _Program;
            }
            set
            {
                _Program = value;
                if(value != -1)
                    UniformBuffer.BindBlock (Program, "Data", 0);
            }
        }

		public IList<IEffectInput> Inputs {
			get;
			set;
		}

		internal string FragmentShader {
			get;
			private set;
		}

		internal string VertexShader {
			get;
			private set;
		}

        internal bool RenderInvalid {
            get;
            set;
        }

        internal ISurface CacheSurface
        {
            get;
            set;
        }

		public virtual Tuple<int, int> Size ()
		{
			var size = Tuple.Create (Inputs.Max (input => input.Size ().Item1), Inputs.Max (input => input.Size ().Item2));
			return size;
		}

		public GLShaderEffect (string fragmentShader, string vertexShader)
		{
			Program = -1;

			FragmentShader = fragmentShader;
			VertexShader = vertexShader;

			Inputs = new List<IEffectInput>();
			UniformBuffer = new UniformBuffer ();

		}

        public void Render()
        {
            RenderInvalid = true;
        }

		public void Dispose ()
		{
			if (Program != -1) {
				GL.DeleteProgram (Program);
				Program = -1;
			}
		}

		public void SetData<T> (T data) where T : struct
		{
			UniformBuffer.UpdateUniformBuffer (data);
		}
	}
}

