using System;
using Zia.Common;

namespace Zia.Gfx
{
	public abstract class Transform
	{
		public abstract Transform Mul(Transform right);
		public abstract Vec2 Mul(Vec2 right);
		public abstract Vec3 Mul(Vec3 right);

		public abstract Transform Invert();

		public abstract Transform RotateX(float degAngle);
		public abstract Transform RotateY(float degAngle);
		public abstract Transform RotateZ(float degAngle);

		public abstract Transform Ortho(float left, float right, float bottom, float top, float znear, float zfar);
		public abstract Transform Perspective (float fovy, float aspect, float zNear, float zFar);
		public abstract Transform LookAt (Vec3 eye, Vec3 target, Vec3 up); 

		public abstract Transform Translate(Vec3 offset);
		public abstract Transform Scale(Vec3 scale);

		public static Transform operator*(Transform left, Transform right)
		{
			return left.Mul (right);
		}

		public static Vec3 operator*(Transform left, Vec3 right)
		{
			return left.Mul (right);
		}

		public static Vec2 operator*(Transform left, Vec2 right)
		{
			return left.Mul (right);
		}
	}
}

