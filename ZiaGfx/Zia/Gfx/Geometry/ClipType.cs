using System;

namespace Zia.Gfx.Geometry
{
	public enum ClipType { Intersection, Union, Difference, Xor };
}

