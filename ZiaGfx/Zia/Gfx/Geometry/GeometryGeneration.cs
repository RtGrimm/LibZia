using System;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.Gfx.Geometry
{
	public class GeometryGeneration
	{

		public static List<Vec2> Ellipse(Vec2 center, float radiusX, float radiusY,
		                                 float resolution)
		{
			List<Vec2> points = new List<Vec2>();
			var step = resolution / (radiusX < radiusY ? radiusX : radiusY);

			for (var angle = 0.0f; angle < 360.0f; angle += step)
			{
				points.Add(
					new Vec2((float)Math.Cos(MathUtil.ToRadians(angle)) * radiusX,
				          (float)Math.Sin(MathUtil.ToRadians(angle)) * radiusY)
					+ center);
			}

			return points;
		}

		public static List<Vec2> Arc(Vec2 center, float radiusX, float radiusY,
		                             float angleBegin, float angleEnd, float resolution)
		{

			Func<float, float, float> mod =  (float left, float right) =>
			{
				var result = left % right;
				if(result < 0) return result + right;
				return result;
			};

			List<Vec2> points = new List<Vec2>();
			var step = resolution / (radiusX < radiusY ? radiusX : radiusY);


			var modStart = mod(angleBegin, 360.0f);
			var modEnd = mod(angleEnd, 360.0f);

			var start = modStart;
			var end = modEnd;

			if ((start == 0 || start == 360) && (end == 0 || end == 360))
				return Ellipse(center, radiusX, radiusY,  resolution);

			if(start > end) end += 360;

			for (var angle = start; angle < end; angle += step)
			{
				var modAngle = mod(angle, 360.0f);

				points.Add(
					new Vec2((float)Math.Cos(MathUtil.ToRadians(modAngle)) * radiusX,
				                (float)Math.Sin(MathUtil.ToRadians(modAngle)) * radiusY)
					+ center);
			}
			return points;
		}


		public static List<Vec2> QuadCurve(int resolution, Vec2 start,
		                                 Vec2 controlPoint, Vec2 end)
		{
			List<Vec2> points = new List<Vec2>();
			for (int i = 0; i < resolution; ++i)
			{
				float t = i / (float)(resolution);
				float u = 1 - t;
				Vec2 point = start * (u * u) + controlPoint * (2 * u * t)
					+ end * (t * t);
				points.Add(point);
			}
			return points;
		}

		public static List<Vec2> CubicCurve(int resolution, Vec2 start,
		                                  Vec2 controlPoint1, Vec2 controlPoint2, Vec2 end)
		{
			List<Vec2> points = new List<Vec2>();
			for (int i = 0; i < resolution; ++i)
			{
				float t = i / (float)(resolution);
				float u = 1 - t;
				Vec2 point = start * (u * u * u) + controlPoint1 * 3 * t * (u * u)
					+ controlPoint2 * 3 * (t * t) * u + end * (t * t * t);
				points.Add(point);
			}
			return points;
		}

		public static List<Vec2> Rectangle(Vec2 location, Vec2 size)
		{
			return new List<Vec2> {
				location,
				new Vec2(location.X + size.X, location.Y),
				location + size,
				new Vec2(location.X, location.Y + size.Y)
			};


		}

		public static List<Vec2> RoundRect(float x, float y, float w, float h,
		                                 float topLeftRadius,
		                                 float topRightRadius,
		                                 float bottomRightRadius,
            float bottomLeftRadius, float resolution)
		{


			if (w < 0.0f)
			{
				x += w;
				w *= -1.0f;
			}

			if (h < 0.0f)
			{
				y += h;
				h *= -1.0f;
			}


			float maxRadius = MathUtil.Min(w / 2.0f, h / 2.0f);
			topLeftRadius = MathUtil.Min(topLeftRadius, maxRadius);
			topRightRadius = MathUtil.Min(topRightRadius, maxRadius);
			bottomRightRadius = MathUtil.Min(bottomRightRadius, maxRadius);
			bottomLeftRadius = MathUtil.Min(bottomLeftRadius, maxRadius);


			if ((Math.Abs(topLeftRadius) < float.Epsilon) &&
			    (Math.Abs(topRightRadius) < float.Epsilon) &&
			    (Math.Abs(bottomRightRadius) < float.Epsilon) &&
			    (Math.Abs(bottomLeftRadius) < float.Epsilon))
			{


				return Rectangle(new Vec2(x, y), new Vec2(w, h));
			}

			List<Vec2> points = new List<Vec2>();

			

			float left = x;
			float right = x + w;
			float top = y;
			float bottom = y + h;

			points.Add(new Vec2(left + topLeftRadius, top));


			if (Math.Abs(topRightRadius) >= float.Epsilon)
			{
				var arc = Arc(new Vec2(right - topRightRadius, top
				                               + topRightRadius),
                    topRightRadius, topRightRadius, 270, 360, resolution);

				points.AddRange (arc);
			}
			else
			{
				points.Add(new Vec2(right, top));
			}

			points.Add(new Vec2(right, bottom - bottomRightRadius));

			if (Math.Abs(bottomRightRadius) >= float.Epsilon)
			{
				var arc = Arc(new Vec2(right - bottomRightRadius, bottom
				                               - bottomRightRadius),
                    bottomRightRadius, bottomRightRadius, 0, 90, resolution);

				points.AddRange (arc);
			}

			points.Add(new Vec2(left + bottomLeftRadius, bottom));


			if (Math.Abs(bottomLeftRadius) >= float.Epsilon)
			{
				var arc = Arc(new Vec2(left + bottomLeftRadius, bottom
				                               - bottomLeftRadius),
                    bottomLeftRadius, bottomLeftRadius, 90, 180, resolution);

				points.AddRange (arc);
			}

			points.Add(new Vec2(left, top + topLeftRadius));


			if (Math.Abs(topLeftRadius) >= float.Epsilon)
			{
				var arc = Arc(new Vec2(left + topLeftRadius, top
				                               + topLeftRadius),
                    topLeftRadius, topLeftRadius, 180, 270, resolution);

				points.AddRange (arc);
			}

			return points;
		}

	}

}

