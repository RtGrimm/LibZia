using System;
using System.Collections.Generic;

using Zia.Common;
using ClipperLib;
using System.Linq;



namespace Zia.Gfx.Geometry
{


	internal class Polyline
	{
		public ICollection<List<Vec2>> Figures {
			get;
			private set;
		}

		public StrokeStyle StorkeStyle {
			get;
			set;
		}

		public WindingRule WindingRule {
			get;
			set;
		}

		public Polyline ()
		{
			Figures = new List<List<Vec2>> ();
		}

		public void AddFigure(List<Vec2> points)
		{
			Figures.Add (points);
		}


		public void Tesselate (out List<uint> outElements, out List<Vec3> outVertices)
		{
			Tesselate (Figures.ToList(), out outElements, out outVertices);
		}

		public void TesselateOutline (out List<uint> outElements, out List<Vec3> outVertices)
		{
			var outline = GetOutline ();
			Tesselate (outline, out outElements, out outVertices);
		}

		public List<List<Vec2>> GetOutline ()
		{
			var intPolygons = Figures.Select ((polygon) => polygon.Select ((point) => 
			                                                               new IntPoint (
				ClipperConverter.ToClipperValue (point.X), 
				ClipperConverter.ToClipperValue (point.Y))).ToList ()).ToList (); 

			var offset = new ClipperOffset ();


			var endType = (ClipperLib.EndType)StorkeStyle.EndType;
			offset.AddPaths (intPolygons, (ClipperLib.JoinType)StorkeStyle.JoinType, endType);

			var outPoints = new List<List<IntPoint>> ();
			offset.Execute (ref outPoints, ClipperConverter.ToClipperValue (StorkeStyle.Thickness));

			return outPoints.Select ((polygon) => polygon
			                         .Select ((value) => new Vec2 (ClipperConverter
			                              .FromClipperValue (value.X), ClipperConverter.FromClipperValue (value.Y))).ToList ()).ToList ();
		}

		private void Tesselate (List<List<Vec2>> figures, out List<uint> outElements, out List<Vec3> outVertices)
		{
			LibTessDotNet.Tess tess = new LibTessDotNet.Tess ();
			foreach (var figure in figures) {
				var points = figure.Select ((point) => new LibTessDotNet.ContourVertex { 
					Position = new LibTessDotNet.Vec3 { X = (float)point.X, Y = (float)point.Y, Z = 0 }
				}).ToArray ();

				tess.AddContour (points);
			}

			tess.Tessellate ((LibTessDotNet.WindingRule)WindingRule, LibTessDotNet.ElementType.Polygons, 3);

			if (tess.Elements != null) {
				var elements = tess.Elements.Select (element => (uint)element);
				var verts = tess.Vertices.Select (vert => new Vec3 (vert.Position.X, vert.Position.Y, vert.Position.Z));

				outElements = elements.ToList ();
				outVertices = verts.ToList ();
			} else {
				outElements = new List<uint> ();
				outVertices = new List<Vec3> ();
			}
		}
	}
}

