using System;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;

namespace Zia.Gfx.Geometry
{
	public class CompositeGeometry : GeometryBase
	{
        private List<List<Vec2>> _Points;

		public CompositeGeometry (GeometryBase geometry1, GeometryBase geometry2, ClipType clipType)
		{
			Combine (geometry1, geometry2, clipType);
		}

		private void Combine(GeometryBase geometry1, GeometryBase geometry2, ClipType clipType)
		{
			var clipper = new ClipperLib.Clipper ();

			clipper.AddPaths (geometry1.GetPolyline().Figures.Select(
                figure => ClipperConverter.ToClipperList(figure)).ToList(), ClipperLib.PolyType.ptSubject, true);

			clipper.AddPaths (geometry2.GetPolyline().Figures.Select(
                figure => ClipperConverter.ToClipperList(figure)).ToList(), ClipperLib.PolyType.ptClip, true);

			var outPoints = new List<List<ClipperLib.IntPoint>>();
			clipper.Execute ((ClipperLib.ClipType)clipType, outPoints);

			_Points = outPoints.Select (figure => ClipperConverter.FromClipperList(figure)).ToList();
		}

		protected override List<List<Vec2>> Figures ()
		{
			return _Points;
		}

		
	}
}

