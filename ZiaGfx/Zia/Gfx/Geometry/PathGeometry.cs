using System;
using System.Linq;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.Gfx.Geometry
{


	public class PathGeometry : GeometryBase
	{
		private ICollection<ICollection<Vec2>> _Figures;
		private List<Vec2> _CurrentFigure;

		public PathGeometry ()
		{
			_Figures = new List<ICollection<Vec2>> ();
			_CurrentFigure = new List<Vec2> ();
		}

		public void Clear()
		{
			_Figures.Clear ();
			_CurrentFigure.Clear ();

			Invalidate ();
		}

		public void Arc(Vec2 center, float radiusX, float radiusY,
		                  float angleBegin, float angleEnd, float curveLength)
		{
			AddPoints(GeometryGeneration.Arc(center, radiusX, radiusY, angleBegin, angleEnd, curveLength));
		}

		public void QuadCurveTo(int resolution,
		                                   Vec2 controlPoint, Vec2 end)
		{
			var startPoint = _CurrentFigure.Count > 0 ? _CurrentFigure.Last () : new Vec2 ();
			AddPoints(GeometryGeneration.QuadCurve(resolution, startPoint, controlPoint, end));
		}

		public void CubicCurveTo(int resolution,
		                         Vec2 controlPoint1, Vec2 controlPoint2, Vec2 end)
		{
			var startPoint = _CurrentFigure.Count > 0 ? _CurrentFigure.Last () : new Vec2 ();
			AddPoints(GeometryGeneration.CubicCurve(resolution, startPoint, controlPoint1, controlPoint2, end));
		}

        public void RoundRectangle(Rect rect, float radius, float resolution)
        {
            RoundRectangle(rect.Location, rect.Size, radius, resolution);
        }

        public void RoundRectangle(Vec2 location, Vec2 size, float radius, float resolution)
		{
			AddFigure (GeometryGeneration.RoundRect(location.X, location.Y, size.X, size.Y, 
			                                        radius, radius, 
                radius, radius, resolution));
		}

		public void RoundRectangle(Vec2 location, Vec2 size,
		                      float topLeftRadius,
		                      float topRightRadius,
		                      float bottomRightRadius,
            float bottomLeftRadius, 
            float resolution)
		{
			AddFigure (GeometryGeneration.RoundRect(location.X, location.Y, size.X, size.Y, 
			                                        topLeftRadius, topRightRadius, 
                bottomRightRadius, bottomLeftRadius, resolution));
		}

		public void Rectangle(Vec2 location, Vec2 size)
		{
			AddFigure (GeometryGeneration.Rectangle (location, size));
		}

		public void Rectangle(Rect rect)
		{
			AddFigure (GeometryGeneration.Rectangle (rect.Location, rect.Size));
		}

		public void Ellipse(Vec2 center, float radiusX, float radiusY,
		                    float resolution)
		{
			AddFigure (GeometryGeneration.Ellipse(center, radiusX, radiusY, resolution));
		}

		public void NewFigure()
		{

			_Figures.Add (_CurrentFigure);
			_CurrentFigure = new List<Vec2> ();
			Invalidate ();
		}

		public void AddPoint(Vec2 point)
		{
			_CurrentFigure.Add (point);
			Invalidate ();
		}

		public void AddPoints(ICollection<Vec2> points)
		{
			_CurrentFigure.AddRange (points);
			Invalidate ();
		}

		public void AddFigure(ICollection<Vec2> figure)
		{
			_Figures.Add (figure);
			Invalidate ();
		}

		protected override List<List<Vec2>> Figures ()
		{
			return _Figures.Concat(new[] { _CurrentFigure }).Select(list => list.ToList()).ToList();
		}
	}
}

