using System;
using System.Linq;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.Gfx.Geometry
{
	static internal class ClipperConverter
	{
		private const long Factor = 100;

		public static float FromClipperValue(long value)
		{
			var f = (float)(value / (float)Factor);
			return f;
		}

		public static long ToClipperValue(float value)
		{
			var i = (long)(value * Factor);
			return i;
		}

		public static List<ClipperLib.IntPoint> ToClipperList(IEnumerable<Vec2> points)
		{
			return points.Select (point => new ClipperLib.IntPoint(
				ToClipperValue(point.X), ToClipperValue(point.Y))).ToList();
		}

		public static List<Vec2> FromClipperList(List<ClipperLib.IntPoint> points)
		{
			return points.Select (point => new Vec2(
				FromClipperValue(point.X), FromClipperValue(point.Y))).ToList();
		}
	}
}

