using System;
using System.Linq;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.Gfx.Geometry
{
	public abstract class GeometryBase : IDisposable
	{
		internal bool StrokeInvalid {
			get;
			set;
		}

		internal bool Invalid {
			get;
			set;
		}

		internal Platform.IMesh FillMesh {
			get;
			set;
		}

		internal Platform.IMesh StrokeMesh {
			get;
			set;
		}

		internal int StrokeElementCount {
			get;
			set;
		}

		internal int FillElementCount {
			get;
			set;
		}

		private StrokeStyle _StrokeStyle;
		public StrokeStyle StrokeStyle {
			get { return _StrokeStyle; } 
			set {
				_StrokeStyle = value; 
				StrokeInvalid = true;
			}
		}

		private WindingRule _WindingRule;
		public WindingRule WindingRule {
			get {
				return _WindingRule;
			}
			set {
				Invalid = true;
				StrokeInvalid = true;

				_WindingRule = value;
			}
		}

		public GeometryBase ()
		{
			_StrokeStyle = new StrokeStyle ();
			Invalid = true;
			StrokeInvalid = true;
			WindingRule = WindingRule.NonZero;
		}

		public void Dispose ()
		{
			if (FillMesh != null) {
				FillMesh.Dispose ();
			}

			if (StrokeMesh != null) {
				StrokeMesh.Dispose ();
			}
		}


		private bool Contains(List<Vec2> verts, Vec2 point)
		{
			var nvert = verts.Count;
			var testx = point.X;
			var testy = point.Y;

			int i, j;
			bool c = false;
			for (i = 0, j = nvert - 1; i < nvert; j = i++)
			{
				if (((verts[i].Y > testy) != (verts[j].Y > testy))
				    && (testx
				    < (verts[j].X - verts[i].X) * (testy - verts[i].Y)
				    / (verts[j].Y - verts[i].Y) + verts[i].X))
					c = !(c);
			}
			return c;
		}
	

        public Rect BoundingBox()
        {
            var points = Figures().SelectMany(figure => figure);

            float maxX = float.MinValue, maxY = float.MinValue, minX = float.MaxValue, minY = float.MaxValue;

            points.Each (vert => {
                maxX = MathUtil.Max (maxX, vert.X);
                maxY = MathUtil.Max (maxY, vert.Y);
                minX = MathUtil.Min (minX, vert.X);
                minY = MathUtil.Min (minY, vert.Y);
            });

            return new Rect (minX, minY, maxX - minX, maxY - minY);
        }

		public bool Contains(Vec2 point)
		{
			return Figures ().Any (figure => Contains(figure, point));
		}



		internal Polyline GetPolyline ()
		{
			var polyline = new Polyline {
				StorkeStyle = _StrokeStyle,
				WindingRule = _WindingRule
			};
			Figures ().ForEach (list => polyline.Figures.Add (list));
			return polyline;
		}

		protected abstract List<List<Vec2>> Figures ();

		protected void Invalidate() 
		{
			Invalid = true;
			StrokeInvalid = true;
		}

	}
}

