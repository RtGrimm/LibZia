using System;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;

namespace Zia.Gfx.Geometry
{
	public class PolygonGeometry : GeometryBase
	{
		public ICollection<ICollection<Vec2>> Polygons {
			get;
			set;
		}

		protected override List<List<Vec2>> Figures ()
		{
			return Polygons.Select (list => list.ToList()).ToList();
		}

		public void Update()
		{
			Invalidate ();
		}

		public PolygonGeometry ()
		{
			Polygons = new List<ICollection<Vec2>> ();
		}
	}
}

