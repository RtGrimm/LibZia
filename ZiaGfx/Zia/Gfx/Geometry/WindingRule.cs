using System;

namespace Zia.Gfx.Geometry
{
	public enum WindingRule
	{
		EvenOdd,
		NonZero,
		Positive,
		Negative,
		AbsGeqTwo
	}
}

