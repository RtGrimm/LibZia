using System;

namespace Zia.Gfx.Geometry
{
	public enum EndType { ClosedPolygon, ClosedLine, OpenButt, OpenSquare, OpenRound };
}

