using System;

namespace Zia.Gfx.Effect
{
	public interface IEffectInput
	{
		Tuple<int, int> Size();
	}
}

