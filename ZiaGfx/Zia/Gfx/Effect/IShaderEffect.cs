using System;
using System.Collections.Generic;

namespace Zia.Gfx.Effect
{
	public interface IShaderEffect : IDisposable, IEffectInput
	{
		IList<IEffectInput> Inputs {
			get;
			set;
		}

		new Tuple<int, int> Size();

		void SetData<T> (T data) where T : struct;
        void Render();
	}

}

