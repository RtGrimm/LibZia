using System;
using Zia.Common;
using System.Threading.Tasks;

namespace Zia.Gfx.Text
{
	public class Font : IDisposable
	{
		Vec2 _Dpi;
		public Vec2 Dpi {
			get {
				return _Dpi;
			}
			set {
				_Dpi = value;
				UpdateSize (CurrentSize, false);
			}
		}

		internal SharpFont.Face Face {
			get;
			private set;
		}

        internal float CurrentSize
        {
            get;
            private set;
        }

		public float FontHeight {
			get {
				return (float)Face.Glyph.Metrics.Height;
			}
		}

		public Font (byte[] fontData)
		{
			Face = Singleton<SharpFont.Library>.Instance.NewMemoryFace (fontData, 0);
			Dpi = new Vec2 (0, 96);
		}

        Font(SharpFont.Face face)
        {
            Face = face;
            Dpi = new Vec2 (0, 96);
        }

        public static async Task<Font> CreateAsync(byte[] fontData)
        {
            return await Task.Run(() => new Font(Singleton<SharpFont.Library>
                .Instance.NewMemoryFace(fontData, 0)));
        }

		public void Dispose ()
		{
			Face.Dispose ();
		}


		internal void UpdateSize(float size, bool updateIfChanged = true)
		{
            if(CurrentSize != size || !updateIfChanged)
			    Face.SetCharSize (0, size, (uint)_Dpi.X, (uint)_Dpi.Y);

            CurrentSize = size;
		}


	}
}

