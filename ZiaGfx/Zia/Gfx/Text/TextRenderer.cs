using System;
using Zia.Common;
using System.Collections.Generic;
using System.Linq;

namespace Zia.Gfx.Text
{
	internal class TextRenderer : IDisposable
	{
		class CacheItem
		{
			public char C;
			public ISurface Texture;
			public float Size;
			public Vec2 Offset;
			public SharpFont.Face Face;
		}

		private Dictionary<float, Dictionary<SharpFont.Face, Dictionary<char, CacheItem>>> _Cache;

		public TextRenderer ()
		{
			_Cache = new Dictionary<float, Dictionary<SharpFont.Face, Dictionary<char, CacheItem>>> ();
		}

		public void Dispose ()
		{
			var textures = _Cache.Values
				.SelectMany (dict => dict.Values)
				.SelectMany (dict => dict.Values)
				.Select (item => item.Texture);

			foreach (var texture in textures) {
				texture.Dispose ();
			}
		}

		void UpdateCache (Context context, char[] text, SharpFont.Face face, float size)
		{

            var fontCache = _Cache
                .GetValueOrDefault(size, () => new Dictionary<SharpFont.Face, Dictionary<char, CacheItem>> ())
                .GetValueOrDefault(face, () => new Dictionary<char, CacheItem>());

			var cacheItems = text.Distinct ().Where (c => !fontCache.ContainsKey (c))
				.Select (c => {
				face.LoadChar (c, SharpFont.LoadFlags.Default, SharpFont.LoadTarget.Normal);

				if (c == ' ') {
					return new CacheItem {
						C = c,
						Offset = new Vec2(),
						Texture = null,
						Size = size,
						Face = face
					};
				}

				face.Glyph.RenderGlyph (SharpFont.RenderMode.Normal);

				var bitmap = face.Glyph.Bitmap;
				var charTexture = context.CreateSurface (bitmap.Width, bitmap.Rows, bitmap.BufferData, ColorFormat.Alpha,
				                                                  SurfaceExtendMode.Clamp, SurfaceFilterType.Nearest);

				var yOffset = ((float)face.Size.Metrics.Ascender + (float)face.Size.Metrics.Descender) - (float)face.Glyph.BitmapTop;
                var xOffset = face.Glyph.BitmapLeft;

				return new CacheItem {
					C = c,
                    Offset = new Vec2(xOffset, yOffset),
					Texture = charTexture,
					Size = size,
					Face = face
				};
			});

			foreach (var cacheItem in cacheItems) {
				fontCache [cacheItem.C] = cacheItem;
			}
		}

        public void DrawText (Context context, Brush brush, Transform projection, string text, ICollection<Vec2> locations, Font font, float size, float opacity)
        {
            DrawText(context, brush, projection, text.ToArray(), locations, font, size, opacity);
        }

        public void DrawText (Context context, Brush brush, Transform projection, char[] text, ICollection<Vec2> locations, Font font, float size, float opacity)
		{
			var face = font.Face;
            font.UpdateSize(size);

			UpdateCache (context, text, face, size);

			var fontCache = _Cache [size] [face];

			var i = 0;

			foreach (var c in text) {
				var charItem = fontCache [c];

				if (charItem.Texture == null) {
					i++;
					continue;
				}

				var location = locations.ElementAt (i);
				var pos = location + charItem.Offset;

				var transform = context.CreateTransform().Translate (
					new Vec3 ((int)pos.X, (int)pos.Y, 0)).Mul (projection);

				context.FillOpacityMask (charItem.Texture, brush, transform, opacity);

				i++;
			}
		}
	}
}

