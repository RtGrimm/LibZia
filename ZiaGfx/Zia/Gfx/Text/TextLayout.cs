using System;
using System.Linq;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.Gfx.Text
{
    class GlyphMetricCache
    {
        SharpFont.Face _Face;

        private Dictionary<char, Metric> _Metrics = new Dictionary<char, Metric>();

        public struct Metric
        {
            public float Advance
            {
                get;
                set;
            }

            public float Height
            {
                get;
                set;
            }
        }

        public GlyphMetricCache(SharpFont.Face face)
        {
            _Face = face;
        }

        public GlyphMetricCache.Metric GetMetric(char c)
        {
            if (!_Metrics.ContainsKey(c))
            {
                _Face.LoadChar(c, SharpFont.LoadFlags.NoBitmap, 
                    SharpFont.LoadTarget.Normal);

                _Metrics[c] = new Metric
                {
                    Advance = (_Face.Glyph.Advance.X.Value >> 6),
                    Height = (float)_Face.Glyph.Metrics.Height
                };
            }

            return _Metrics[c];
        }
    }



    public class HitTestMetrics
    {
        public TextLayout.CharData CharData
        {
            get;
            set;
        }

        public bool Hit
        {
            get;
            set;
        }

        public bool IsLeft
        {
            get;
            set;
        }
    }

    public class TextLayout
    {

        public class LineData
        {
            public ICollection<CharData> Chars
            {
                get;
                set;
            }

            public Rect BoundingBox
            {
                get;
                set;
            }
        }

        public class CharData
        {
            public char C
            {
                get;
                set;
            }

            public Vec2 Location
            {
                get;
                set;
            }

            public LineData Line
            {
                get;
                set;
            }

            public Vec2 Size
            {
                get;
                set;
            }

            public int LineIndex
            {
                get;
                set;
            }
        }

        public bool Wrap
        {
            get;
            set;
        }

        public bool SingleLine
        {
            get;
            set;
        }

        public Font Font
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public float Size
        {
            get;
            set;
        }

        internal float RealSize
        {
            get;
            private set;
        }

        public Vec2 MaxSize
        {
            get;
            set;
        }

        public Vec2 TextSize
        {
            get;
            private set;
        }

        public ICollection<CharData> CharsList
        {
            get;
            private set;
        }

        public ICollection<LineData> LinesList
        {
            get;
            private set;
        }

        private GlyphMetricCache _GlyphMetricCache;

        float _FaceHeight;

       

        public TextLayout()
        {
            CharsList = new List<CharData>();
            LinesList = new List<LineData>();
        }

        List<String> WrapWord(string text, float width)
        {
            var chars = text.ToCharArray();

            List<string> lines = new List<String>();

            string currentLine = "";
            float currentLineWidth = 0;

			

            var i = 0;

            while (i < chars.Length)
            {
                var metric = _GlyphMetricCache.GetMetric(chars[i]);


                var charWidth = metric.Advance;

                if (charWidth > width)
                {
                    lines.Add(currentLine);
                    currentLine = "";
                    currentLineWidth = 0;
                    i++;
                    continue;
                }

                if (currentLineWidth + charWidth > width)
                {
                    lines.Add(currentLine);
                    currentLine = "";
                    currentLineWidth = 0;
                    continue;
                }

                currentLineWidth += charWidth;
                currentLine += chars[i];

                i++;
            }

            lines.Add(currentLine);

            return lines;
        }

        List<string> WrapText(string text, float width)
        {
            if (text.Length == 0)
                return new List<string> {
                ""
            };

           

            List<String> words = text.Split(' ').ToList();
            words = words.AsEnumerable()
				.Reverse().Skip(1).Reverse()
					.Select(word => word + " ")
					.Concat(new string[] { words.Last() }).ToList();



            List<string> lines = new List<String>();
            string currentLine = "";
            float currentLineSize = 0;

            var i = 0;

          

            while (i < words.Count)
            {
                var word = words[i];
                var wordSize = SizeOfLine(word);


                if (wordSize.X > width)
                {
                    var wordLines = WrapWord(word, width);
                    if (currentLine.Length > 0)
                    {
                        lines.Add(currentLine);
                        currentLine = "";
                        currentLineSize = 0;
                    }

                    lines.AddRange(wordLines);
                    i++;

                    continue;
                }

                if (currentLineSize + wordSize.X > width)
                {
                    lines.Add(currentLine);
                    currentLine = "";
                    currentLineSize = 0;
                    continue;
                }

                currentLine += word;
                currentLineSize += wordSize.X;

                i++;
            }

            lines.Add(currentLine);

            return lines;
        }

        public Vec2 SizeOfLine(string text)
        {
			
            Vec2 size = new Vec2(0, _FaceHeight);
            foreach (var c in text)
            {
                var metric = _GlyphMetricCache.GetMetric(c);
                size.X += metric.Advance;
            }

            return size;
        }

        public HitTestMetrics HitTest(Vec2 point)
        {
            if (!CharsList.Any())
                return new HitTestMetrics {
                Hit = false
            };


            var boundingBoxes = LinesList.Select(line => line.BoundingBox).ToList();

            var i = 0;
            foreach (var box in boundingBoxes)
            {
                var bbox = box;
                bbox.Size.X = float.MaxValue;

                if (bbox.Contains(point))
                {
                    var line = LinesList.ElementAt(i);

                    var charCenters = line.Chars.Select((charData, index) => {
                        var metric = _GlyphMetricCache.GetMetric(charData.C);
                        var location = charData.Location;

                        var charRect = new Rect(
                            location, new Vec2(metric.Advance, metric.Height));

                        return charRect.Location.X + (charRect.Size.X / 2.0f);
                    }).ToList();

                    var charDistances = charCenters.Select(
                        (center, index) => new {
						Distance = point.X - center, 
							Index = index
					}).OrderBy(item => Math.Abs(item.Distance)).ToList();

                    var distanceItem = charDistances.Take(1).Single();

                    return new HitTestMetrics
                    {
                        CharData = line.Chars.ElementAt(distanceItem.Index),
                        Hit = true,
                        IsLeft = distanceItem.Distance <= 0
                    };
                }
                i++;
            }

            return new HitTestMetrics
            {
                Hit = false
            };
        }

        void LayoutLines(float maxSize, ICollection<string> lines)
        {

            LinesList = new List<LineData>();
            CharsList = new List<CharData>();

            Vec2 pen = new Vec2(0, 0);

            var i = 0;

            foreach (var line in lines)
            {
                var maxHeight = 0.0f;
                uint lastGlyphIndex = 0;

                var lineData = new LineData();
                List<CharData> chars = new List<CharData>();

               line.Each((char c, int index) => {
                    var metric = _GlyphMetricCache.GetMetric(c);

                    chars.Add(new CharData {
                        C = c,
                        Location = pen,
                        Line = lineData,
                        Size = new Vec2(metric.Advance, _FaceHeight),
                        LineIndex = index
                    });

                    var hasKerning = Font.Face.HasKerning;

                    var glyphIndex = Font.Face.GetCharIndex(c);
                    var kerning = Font.Face.GetKerning(
                        lastGlyphIndex, glyphIndex, SharpFont.KerningMode.Unfitted).X.ToSingle();

                    pen.X += metric.Advance + kerning;
                    maxHeight = MathUtil.Max((float)maxHeight, metric.Height);

                    lastGlyphIndex = glyphIndex;
                });

                var lineSize = SizeOfLine(line);

                var location = new Vec2(0, pen.Y);
                var size = new Vec2(lineSize.X, (float)Font.Face.Size.Metrics.Height + (float)Font.Face.Size.Metrics.Descender);

                pen.X = 0;
                pen.Y += ((float)Font.Face.Size.Metrics.Height + (float)Font.Face.Size.Metrics.Descender);

                lineData.BoundingBox = new Rect(location, size);
                lineData.Chars = chars;

                LinesList.Add(lineData);

                i++;

                if (pen.Y > maxSize)
                    break;
			
            }

            LinesList = LinesList.Take(i).ToList();
            CharsList = LinesList.Select(line => line.Chars).SelectMany(list => list).ToList();
        }

        public void UpdateLayout()
        {
            Font.UpdateSize(Size);
            RealSize = Size;

            _GlyphMetricCache = new GlyphMetricCache(Font.Face);
            _FaceHeight = Font.Face.Size.Metrics.Ascender.ToSingle() + Font.Face.Size.Metrics.Descender.ToSingle();



            if (Wrap)
            {
                var preWrapLines = Text.Split(new[] {
                    "\n"
                }, StringSplitOptions.None);
                var lines = preWrapLines
                    .Select(line => WrapText(line, MaxSize.X))
                    .SelectMany(list => list)
                        .ToList();

                LayoutLines(MaxSize.Y, lines);

            }
            else
            {
                var lines = SingleLine ? new[] {Text.Replace("\n", "")}.ToList() : Text.Split('\n').ToList();
                LayoutLines(float.MaxValue, lines);
            }

            var boundingBoxes = LinesList
                .Select(line => line.BoundingBox)
                    .Concat(new[] {new Rect()});

            TextSize = new Vec2(boundingBoxes.Max(lineSize => lineSize.Size.X), 
                                boundingBoxes.Sum(lineSize => lineSize.Size.Y));

			
        }
    }
}

