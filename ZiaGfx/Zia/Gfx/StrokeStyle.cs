using System;
using Zia.Gfx.Geometry;

namespace Zia.Gfx
{
	public class StrokeStyle
	{
		public JoinType JoinType {
			get;
			set;
		}

		public EndType EndType {
			get;
			set;
		}

		public float Thickness {
			get;
			set;
		}

		public StrokeStyle (JoinType joinType, EndType endType, float thickness)
		{
			JoinType = joinType;
			EndType = endType;
			Thickness = thickness;
		}

		public StrokeStyle ()
		{
			JoinType = JoinType.Round;
			EndType = EndType.ClosedPolygon;
		}
	}
}

