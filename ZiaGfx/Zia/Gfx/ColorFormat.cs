using System;

namespace Zia.Gfx
{
	public enum ColorFormat
	{
		RGBA,
		Alpha
	}
}

