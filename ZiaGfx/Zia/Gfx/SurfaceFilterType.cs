using System;

namespace Zia.Gfx
{
	public enum SurfaceFilterType
	{
		Nearest,
		Linear
	}
}

