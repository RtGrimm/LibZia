using System;

namespace Zia.Gfx
{
	public enum SurfaceExtendMode
	{
		Clamp,
		Repeat
	}
}

