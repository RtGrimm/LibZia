using System;

namespace Zia.Gfx.RenderList
{
	internal class SetTarget : IRenderCommand
	{
		public ITarget Target {
			get;
			set;
		}
	}
}

