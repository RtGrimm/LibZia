using System;
using System.Collections.Generic;
using Zia.Common;
using Zia.Gfx.Platform;

namespace Zia.Gfx.RenderList
{
	internal class GeometryCommand : IRenderCommand
	{
		public GeometryCommand ()
		{
		}

		public int ElementCount {
			get;
			set;
		}

		public Transform Transform {
			get;
			set;
		}

		public Brush Brush {
			get;
			set;
		}

        public float Opacity
        {
            get;
            set;
        }

		public IMesh Mesh {
			get;
			set;
		}
	}
}

