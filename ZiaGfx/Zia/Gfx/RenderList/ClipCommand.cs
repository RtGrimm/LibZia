using System;

namespace Zia.Gfx.RenderList
{
    public class EnableClipCommand : IRenderCommand
    {

    }

    public class DisableClipCommand : IRenderCommand
    {

    }

	public class ClipCommand : IRenderCommand
	{
		public int X {
			get;
			set;
		}
		public int Y {
			get;
			set;
		}

		public int Width {
			get;
			set;
		}

		public int Height {
			get;
			set;
		}

		public ClipCommand ()
		{
		}
	}
}

