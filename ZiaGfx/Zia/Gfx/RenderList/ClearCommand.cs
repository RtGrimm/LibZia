using System;
using Zia.Common;

namespace Zia.Gfx.RenderList
{
	public class ClearCommand : IRenderCommand
	{
		public Color ClearColor {
			get;
			set;
		}

		public ClearCommand ()
		{
		}

	}
}

