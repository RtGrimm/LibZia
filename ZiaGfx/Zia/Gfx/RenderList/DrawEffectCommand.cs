using System;

namespace Zia.Gfx.RenderList
{
    public class DrawEffectCommand : ImageCommand
    {
        public Effect.IShaderEffect Effect
        {
            get;
            set;
        }
    }
}

