using System;

namespace Zia.Gfx.RenderList
{
	public class FillOpacityMask : IRenderCommand
	{
		public ISurface Mask {
			get;
			set;
		}

		public Brush Brush {
			get;
			set;
		}

		public Transform Transform {
			get;
			set;
		}

        public float Opacity
        {
            get;
            set;
        }

		public FillOpacityMask ()
		{
		}
	}
}

