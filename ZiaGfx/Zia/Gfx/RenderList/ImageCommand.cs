using System;

namespace Zia.Gfx.RenderList
{
    public class ImageCommand : IRenderCommand
    {
        public Transform Transform {
            get;
            set;
        }

        public float Opacity
        {
            get;
            set;
        }
    }
}

