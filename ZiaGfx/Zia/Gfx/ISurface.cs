using System;
using Zia.Common;

namespace Zia.Gfx
{
	public interface ISurface : IDisposable, Effect.IEffectInput
	{
		new Tuple<int, int> Size();

		ColorFormat Format();
		bool HitTest(Vec2 point);
	}
}

