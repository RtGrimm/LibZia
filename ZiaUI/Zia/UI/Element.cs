using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Zia.Common;
using Zia.Gfx;
using Zia.UI.Data;
using Zia.UI.Controls;

namespace Zia.UI
{
  

    public enum HorizontalAlignment
    {
        Left,
        Center,
        Right,

    }

    public enum VerticalAlignment
    {
        Top,
        Center,
        Bottom
    }

    public interface ILength
    {

    }

    public class Length : ILength
    {
        public static readonly ILength MatchSlot = new Length();
        public static readonly ILength FillSlot = new Length();
        public static readonly ILength Auto = new Length();
    }



    public struct Exact : ILength
    {
        public float Length
        {
            get;
            set;
        }

        public Exact(float length) : this()
        {
            Length = length;
        }


        public static Exact operator*(Exact left, float right)
        {
            return new Exact(left.Length * right);
        }

        public static Exact operator+(Exact left, Exact right)
        {
            return new Exact(left.Length + right.Length);
        }

        public static Exact operator-(Exact left, Exact right)
        {
            return new Exact(left.Length - right.Length);
        }

        public static Exact operator/(Exact left, Exact right)
        {
            return new Exact(left.Length / right.Length);
        }

        public static Exact operator*(Exact left, Exact right)
        {
            return new Exact(left.Length * right.Length);
        }
    }

    public class Element : PropertyBase
    {
        public static readonly Tag LayoutInvalidationTag = 
            new Tag(() => LayoutInvalidationTag);
      
        public static readonly Tag RenderInvalidationTag = 
            new Tag(() => RenderInvalidationTag);

        public static readonly Tag RenderInvalidationChildrenTag = 
            new Tag(() => RenderInvalidationChildrenTag);

        public static readonly Tag EffectInvalidationTag = 
            new Tag(() => EffectInvalidationTag);

        public static RoutedEvent<RoutedEventArgs<Element>, Element> TreeChangedEvent = 
            new RoutedEvent<RoutedEventArgs<Element>, Element>(() => TreeChangedEvent, RoutingStrategy.Bubble);

        public event Action<RoutedEventArgs<Element>> TreeChanged
        {
            add { TreeChangedEvent.AddHandler(this, value); }
            remove { TreeChangedEvent.RemoveHandler(this, value); }
        }

        #region DependencyProperties

        public static readonly Property<float, Element> OpacityProperty =
            Property<float, Element>
                        .New()
                        .DefaultValue(1)
            .Tags(RenderInvalidationChildrenTag, RenderInvalidationTag)
                        .Name(() => OpacityProperty);

        public float Opacity
        {
            get
            {
                return OpacityProperty.GetValue(this);
            }
            set
            {
                OpacityProperty.SetValue(this, value);
            }
        }

        public static readonly Property<ICollection<Element>, Element> ChildrenProperty = 
                    Property<ICollection<Element>, Element>
                        .New()
                        .Name(() => ChildrenProperty)
                        .ChangedFromCollection(true)
                        .ChangedFromValue(true)
                        .CoerceToObservableCollection()
                        .Tags(RenderInvalidationTag, LayoutInvalidationTag);

        public ICollection<Element> Children
        {
            get
            {
                return ChildrenProperty.GetValue(this);
            }
            set
            {
                ChildrenProperty.SetValue(this, value);
            }
        }

        public static readonly Property<int, Element> ZOrderProperty = 
                    Property<int, Element>
                        .New()
                        .Name(() => ZOrderProperty);

        public int ZOrder
        {
            get
            {
                return ZOrderProperty.GetValue(this);
            }
            set
            {
                ZOrderProperty.SetValue(this, value);
            }
        }


        public static readonly Property<bool, Element> LayoutRoundingProperty = 
                    Property<bool, Element>
                        .New()
                        .DefaultValue(true)
                        .Name(() => LayoutRoundingProperty);

        public bool LayoutRounding
        {
            get
            {
                return LayoutRoundingProperty.GetValue(this);
            }
            set
            {
                LayoutRoundingProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, Element> RenderLocationProperty = 
                    Property<Vec2, Element>
                        .New()
                        .Name(() => RenderLocationProperty)
                        .Tags(RenderInvalidationTag);

        public Vec2 RenderLocation
        {
            get
            {
                return RenderLocationProperty.GetValue(this);
            }
            set
            {
                RenderLocationProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, Element> RenderSizeProperty = 
                    Property<Vec2, Element>
                        .New()
                        .Name(() => RenderSizeProperty)
                        .Tags(RenderInvalidationTag);

        public Vec2 RenderSize
        {
            get
            {
                return RenderSizeProperty.GetValue(this);
            }
            set
            {
                RenderSizeProperty.SetValue(this, value);
            }
        }


        public static readonly Property<bool, Element> ClipProperty = 
                    Property<bool, Element>
                        .New()
                        .DefaultValue(true)
                        .Name(() => ClipProperty)
                        .Tags(RenderInvalidationTag);

        public bool Clip
        {
            get
            {
                return ClipProperty.GetValue(this);
            }
            set
            {
                ClipProperty.SetValue(this, value);
            }
        }

        public static readonly Property<IUIEffect, Element> EffectProperty = 
                    Property<IUIEffect, Element>
                        .New()
                        .Name(() => EffectProperty)
                        .Tags(RenderInvalidationTag)
                        .ChangedFromValue(true);

        public IUIEffect Effect
        {
            get
            {
                return EffectProperty.GetValue(this);
            }
            set
            {
                EffectProperty.SetValue(this, value);
            }
        }


        public static readonly Property<ICollection<ElementTransform>, Element> TransformsProperty = 
                    Property<ICollection<ElementTransform>, Element>
                        .New()
                        .Name(() => TransformsProperty)
                        .Tags(RenderInvalidationTag)
                        .CoerceToObservableCollection();

        public ICollection<ElementTransform> Transforms
        {
            get
            {
                return TransformsProperty.GetValue(this);
            }
            set
            {
                TransformsProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, Element> CacheEnabledProperty = 
                    Property<bool, Element>
                        .New()
                        .Tags(RenderInvalidationTag)
                        .Name(() => CacheEnabledProperty);

        public bool CacheEnabled
        {
            get
            {
                return CacheEnabledProperty.GetValue(this);
            }
            set
            {
                CacheEnabledProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, Element> CacheChildrenProperty = 
                    Property<bool, Element>
                        .New()
                        .Tags(RenderInvalidationTag)
                        .Name(() => CacheChildrenProperty);

        public bool CacheChildren
        {
            get
            {
                return CacheChildrenProperty.GetValue(this);
            }
            set
            {
                CacheChildrenProperty.SetValue(this, value);
            }
        }


        public static readonly Property<Margins, Element> MarginsProperty = 
                    Property<Margins, Element>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .Name(() => MarginsProperty);

        public Margins Margins
        {
            get
            {
                return MarginsProperty.GetValue(this);
            }
            set
            {
                MarginsProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Margins, Element> PaddingProperty = 
                    Property<Margins, Element>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .Name(() => PaddingProperty);

        public Margins Padding
        {
            get
            {
                return PaddingProperty.GetValue(this);
            }
            set
            {
                PaddingProperty.SetValue(this, value);
            }
        }

        public static readonly Property<HorizontalAlignment, Element> HorizontalAlignmentProperty = 
                    Property<HorizontalAlignment, Element>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .Name(() => HorizontalAlignmentProperty);

        public HorizontalAlignment HorizontalAlignment
        {
            get
            {
                return HorizontalAlignmentProperty.GetValue(this);
            }
            set
            {
                HorizontalAlignmentProperty.SetValue(this, value);
            }
        }

        public static readonly Property<VerticalAlignment, Element> VerticalAlignmentProperty = 
                    Property<VerticalAlignment, Element>
                        .New()
                        
                        .Tags(LayoutInvalidationTag)
                        .Name(() => VerticalAlignmentProperty);

        public VerticalAlignment VerticalAlignment
        {
            get
            {
                return VerticalAlignmentProperty.GetValue(this);
            }
            set
            {
                VerticalAlignmentProperty.SetValue(this, value);
            }
        }

        public static readonly Property<ILength, Element> WidthProperty = 
                    Property<ILength, Element>
                        .New()
                        .DefaultValue(Length.FillSlot)
                        .Tags(LayoutInvalidationTag)
                        .Name(() => WidthProperty);

        public ILength Width
        {
            get
            {
                return WidthProperty.GetValue(this);
            }
            set
            {
                WidthProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, Element> SizeToContentProperty = 
                    Property<bool, Element>
                        .New()
                        .DefaultValue(true)
                        .Tags(LayoutInvalidationTag)
                        .Name(() => SizeToContentProperty);

        public bool SizeToContent
        {
            get
            {
                return SizeToContentProperty.GetValue(this);
            }
            set
            {
                SizeToContentProperty.SetValue(this, value);
            }
        }

        public static readonly Property<ILength, Element> HeightProperty = 
            Property<ILength, Element>
                        .New()
                        .DefaultValue(Length.FillSlot)
                        .Tags(LayoutInvalidationTag)
                        .ChangedFromValue(true)
                        .Name(() => HeightProperty);

        public ILength Height
        {
            get
            {
                return HeightProperty.GetValue(this);
            }
            set
            {
                HeightProperty.SetValue(this, value);
            }
        }
        
        public static readonly Property<float, Element> MinHeightProperty = 
                   Property<float, Element>
                       .New()
                       .Tags(LayoutInvalidationTag)
                        .ChangedFromValue(true)
                       .Name(() => MinHeightProperty);

        public float MinHeight
        {
            get
            {
                return MinHeightProperty.GetValue(this);
            }
            set
            {
                MinHeightProperty.SetValue(this, value);
            }
        }

         public static readonly Property<float, Element> MinWidthProperty = 
                    Property<float, Element>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .Name(() => MinWidthProperty);
                        
        public float MinWidth
        {
            get 
            {
                return MinWidthProperty.GetValue(this);
            }
            set
            {
                MinWidthProperty.SetValue(this, value);
            }
        }
       
        public static readonly Property<float, Element> MaxHeightProperty = 
                    Property<float, Element>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .DefaultValue(float.MaxValue)
                        .Name(() => MaxHeightProperty);

        public float MaxHeight
        {
            get
            {
                return MaxHeightProperty.GetValue(this);
            }
            set
            {
                MaxHeightProperty.SetValue(this, value);
            }
        }



        public static readonly Property<float, Element> MaxWidthProperty = 
                    Property<float, Element>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .DefaultValue(float.MaxValue)
                        .Name(() => MaxWidthProperty);

        public float MaxWidth
        {
            get
            {
                return MaxWidthProperty.GetValue(this);
            }
            set
            {
                MaxWidthProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Action<Action>, Element> RemoveOverrideProperty =
            Property<Action<Action>, Element>
            .New()
            .DefaultValue(remove => remove())
            .Name(() => RemoveOverrideProperty);

        public Action<Action> RemoveOverride
        {
            get
            {
                return RemoveOverrideProperty.GetValue(this);
            }
            set
            {
                RemoveOverrideProperty.SetValue(this, value);
            }
        }



        #endregion DependencyProperties

        public event Action<Element> Adopted = (newParent) => {};
        public event Action Orphaned = () => {};

        private bool InFlux
        {
            get;
            set;
        }

        public Element Parent
        {
            get;
            private set;
        }

        public bool HasRendered
        {
            get;
            internal set;
        }

        public Vec2 DesiredSize
        {
            get;
            private set;
        }

        internal class RenderInvalidateEventArgs : RoutedEventArgs<Element>
        {
          
            public RenderInvalidateEventArgs()
            {
                
                
            }
        }

        internal static RoutedEvent<RenderInvalidateEventArgs, Element> RenderInvalidateEvent = 
            new RoutedEvent<RenderInvalidateEventArgs, Element>(() => RenderInvalidateEvent, RoutingStrategy.Bubble);

        internal event Action<RenderInvalidateEventArgs> RenderInvalidate
        {
            add { RenderInvalidateEvent.AddHandler(this, value); }
            remove { RenderInvalidateEvent.RemoveHandler(this, value); }
        }

        internal static RoutedEvent<RenderInvalidateEventArgs, Element> RenderInvalidateChildrenEvent = 
            new RoutedEvent<RenderInvalidateEventArgs, Element>(() => RenderInvalidateChildrenEvent, RoutingStrategy.Tunnel);

        internal event Action<RenderInvalidateEventArgs> RenderInvalidateChildren
        {
            add { RenderInvalidateChildrenEvent.AddHandler(this, value); }
            remove { RenderInvalidateChildrenEvent.RemoveHandler(this, value); }
        }

        internal static RoutedEvent<RoutedEventArgs<Element>, Element> LayoutInvalidateEvent = 
            new RoutedEvent<RoutedEventArgs<Element>, Element>(() => LayoutInvalidateEvent, RoutingStrategy.Bubble);

        internal event Action<RoutedEventArgs<Element>> LayoutInvalidate
        {
            add { LayoutInvalidateEvent.AddHandler(this, value); }
            remove { LayoutInvalidateEvent.RemoveHandler(this, value); }
        }


        internal static RoutedEvent<RoutedEventArgs<Element>, Element> ActivateEvent = 
            new RoutedEvent<RoutedEventArgs<Element>, Element>(() => ActivateEvent, RoutingStrategy.Direct);

        internal event Action<RoutedEventArgs<Element>> Activate
        {
            add { ActivateEvent.AddHandler(this, value); }
            remove { ActivateEvent.RemoveHandler(this, value); }
        }


        private ICollection<Element> _PhantomChildren = new List<Element>();  
        private List<Element> _FluxChildren = new List<Element>();
        private ICollection<Element> _InternalChildren = new List<Element>();

        private bool _IsActive = false;

        internal bool RenderInvalid
        {
            get; 
            set; 
        }

        internal bool EffectInvalid
        {
            get; 
            set; 
        }

        internal bool TransformInvalid 
        {
            get; 
            set; 
        }

        internal Transform Transform
        {
            get; 
            set; 
        }

        internal Transform LastRenderTransform
        {
            get;
            set;
        }

        internal ISurface EffectCache 
        {
            get; 
            set; 
        }

        internal IRenderableTarget RenderCache
        {
            get;
            set;
        }

        internal Tuple<int, int> RenderCacheSize
        {
            get;
            set;
        }

 
        public Element()
        {
            this.TransformInvalid = true;
            this.RenderInvalid = true;
            this.EffectInvalid = true;

            Children = new List<Element>();

            RenderCacheSize = Tuple.Create(0, 0);

            SubscribeChildrenEvents();
            SubscribeTransformsEvents();
            SubscribeRenderEvents();
            SubscribeLayoutEvents();
        }


        void SubscribeLayoutEvents()
        {
            AddListener(PropertyBase.Listener.New()
                        .WithCallback(() =>  LayoutInvalidateEvent.Raise(this))
                        .WithTags(LayoutInvalidationTag));
        }

        void SubscribeRenderEvents()
        {
            RenderInvalidate += OnRenderInvalid;
            RenderInvalidateChildren += OnRenderInvalidateChildren;

            AddListener(PropertyBase.Listener.New()
                .WithCallback(() => RenderInvalidateChildrenEvent.Raise(this))
                .WithTags(RenderInvalidationChildrenTag));


            AddListener(PropertyBase.Listener.New()
                        .WithCallback(() => {
                EffectInvalid = true;
                RenderInvalidateEvent.Raise(this, new RenderInvalidateEventArgs());
            })
                        .WithTags(EffectInvalidationTag));

            AddListener(PropertyBase.Listener.New()
                        .WithCallback(() => RenderInvalidateEvent.Raise(this))
                        .WithTags(RenderInvalidationTag));

        }

        void OnRenderInvalidateChildren (RenderInvalidateEventArgs args)
        {
            this.RenderInvalid = true;
        }

        void SubscribeTransformsEvents()
        {
            var transforms = new ObservableCollection<ElementTransform>();
            Transforms = transforms;
            transforms.CollectionChanged += (sender, e) => {
                TransformInvalid = true;
            };

            AddListener(Listener.New()
                        .WithTags(ElementTransform.TransformParamTag)
                        .WithCallback(() => TransformInvalid = true));
        }

        void SubscribeChildrenEvents()
        {
            ChildrenProperty.AddChangedCallback(this, args => {
                var newValue = args.NewValue;

                var oldValue = args.OldValue;

                oldValue = oldValue.Except(newValue).ToList();
                newValue = newValue.Except(args.OldValue).ToList();


                ChildrenChanged (newValue, oldValue);
            });
		}


        void ChildrenChanged (IEnumerable<Element> newChildren, IEnumerable<Element> oldChildren, bool isPhantom = false)
		{
            newChildren.Each (item => {
                item.Parent = this;
                if(this._IsActive)
				    item.ActivateCore ();
				
                item.Adopted(this);

                if(isPhantom)
                    _PhantomChildren.Add(item);
                else
                    _InternalChildren.Add(item);
			});

			oldChildren.Each (item => {
                item.RemoveOverride(() => {

                    if(isPhantom)
                        _PhantomChildren.Remove(item);
                    else
                        _InternalChildren.Remove(item);
                    
                    item.Parent = null;

                    if(this._IsActive)
                        item.DeactivateCore ();

                    item.Orphaned();
                });

			});

            TreeChangedEvent.Raise(this);
		}


        protected void AddPhantomChildren(IEnumerable<Element> children)
        {
            _PhantomChildren.AddRange(children);
            ChildrenChanged(children, new Element[] {});
        }

        protected void RemovePhantomChildren(IEnumerable<Element> children)
        {
            children.Each(child => RemovePhantomChild(child));
        }

        protected void AddPhantomChild(Element child)
        {
            
            ChildrenChanged(new[] {child}, new Element[] {}, true);
        }

        protected void RemovePhantomChild(Element child)
        {
            
            ChildrenChanged(new Element[] {}, new [] {child}, true);
        }


        public ICollection<Element> GetChildren()
        {
            return _InternalChildren.Concat(_PhantomChildren).Concat(_FluxChildren).ToList();
        }

        internal ICollection<Element> GetChildrenSortedByZOrder()
        {
            return GetChildren().OrderBy(child => child.ZOrder).ToList();
        }

		internal void ActivateCore ()
		{
            if (_IsActive)
                return;


            
             ActivateOverride ();
            ActivateEvent.Raise(this);

            _IsActive = true;

            GetChildren().Each (child => child.ActivateCore ());
            TreeChangedEvent.Raise(this);
		}

		internal void DeactivateCore ()
		{
            if(_IsActive == true)
			    DeactivateOverride ();

            _IsActive = false;

            GetChildren().Each (child => child.DeactivateCore ());
		}

		protected virtual void ActivateOverride ()
		{

		}

        protected virtual void DeactivateOverride ()
		{
		}

        internal void RenderCore (Context context, Transform projection, Transform transform, float opacity)
        {
            RenderOverride(context, projection, transform, opacity);
        }

        public virtual void UpdateOverride()
        {
        }

        protected virtual void RenderOverride (Context context, Transform projection, Transform transform, float opacity)
		{
		}

		private void TransformChanged (PropertyMetadata args)
		{
			TransformInvalid = true;
		}

        private void OnRenderInvalid (RenderInvalidateEventArgs args)
		{
            if (args.Source == this || (args.Source != this && 
                                        (CacheChildren))) {
				RenderInvalid = true;
                EffectInvalid = true;
			} 
		}

		private void UpdateTransform (Context context)
		{
			if (TransformInvalid) {
				Transform = Transforms.Aggregate (
					context.CreateTransform (), 
					(accTransform, transform) => accTransform * transform.GetTransform (context));

				TransformInvalid = false;
			}
		}


        internal bool HitTest(Vec2 point, Vec2 localPoint)
        {
            return HitTestOveride(point, localPoint);
        }

        protected virtual bool HitTestOveride(Vec2 point, Vec2 localPoint)
        {
            var bbox = new Rect(0, 0, RenderSize.X, RenderSize.Y);
            var hit = bbox.Contains(localPoint);
            return hit;
        }

		

        public void Arrange(Rect finalRect)
        {
            
            var availableSize = finalRect.Size;

            var margins = new Vec2(
                Margins.Left + Margins.Right, 
                Margins.Top + Margins.Bottom);

            availableSize -= margins;

            availableSize = availableSize.Clamp(
                new Vec2(MinWidth, MinHeight), 
                new Vec2(MaxWidth, MaxHeight));

            var desiredSize = DesiredSize;

            if (Width == Length.MatchSlot)
                desiredSize.X = availableSize.X;

            if (Height == Length.MatchSlot)
                desiredSize.Y = availableSize.Y;

            desiredSize = desiredSize.Clamp(new Vec2(), availableSize);

            var padding = new Vec2(Padding.Left + Padding.Right, 
                                   Padding.Top + Padding.Bottom);

            var finalSize = ArrangeOverride(desiredSize - padding, 
                                            new Vec2(Padding.Left, Padding.Top))
                .Clamp(new Vec2(), availableSize) + padding;

            var location = finalRect.Location + new Vec2(
                Margins.Left, Margins.Top);

            var horizontalAlignmentMap = new Dictionary<HorizontalAlignment, float>
            {
                {HorizontalAlignment.Left, location.X},
                {HorizontalAlignment.Right, (location.X + finalRect.Size.X) - finalSize.X},
                {HorizontalAlignment.Center, location.X + (finalRect.Size.X / 2.0f) - (finalSize.X / 2f)}
            };

            var verticalAlignmentMap = new Dictionary<VerticalAlignment, float>
            {
                {VerticalAlignment.Top, location.Y},
                {VerticalAlignment.Bottom, (location.Y + finalRect.Size.Y) - finalSize.Y},
                {VerticalAlignment.Center, location.Y + (finalRect.Size.Y / 2.0f) - (finalSize.Y / 2f)}
            };

            location = new Vec2(horizontalAlignmentMap[this.HorizontalAlignment], 
                                verticalAlignmentMap[this.VerticalAlignment]);

            RenderSize = finalSize;
            RenderLocation = location;
        }

        protected virtual Vec2 ArrangeOverride(Vec2 finalSize, Vec2 finalLocation)
        {
            GetChildren().Each(child => child.Arrange(new Rect(finalLocation, finalSize)));
            return finalSize;
        }

        public void Measure(Vec2 availableSize)
        {
            availableSize -= new Vec2(
                Margins.Left + Margins.Right, 
                Margins.Top + Margins.Bottom);

            availableSize = availableSize.Clamp(
                new Vec2(MinWidth, MinHeight), 
                new Vec2(MaxWidth, MaxHeight));

            var desiredSize = MeasureOverride(availableSize);

            if (Width == Length.FillSlot)
                desiredSize.X = availableSize.X;

            if (Height == Length.FillSlot)
                desiredSize.Y = availableSize.Y;

            if (Width is Exact)
                desiredSize.X = ((Exact)Width).Length;

            if (Height is Exact)
                desiredSize.Y = ((Exact)Height).Length;

            desiredSize += new Vec2(Padding.Left + Padding.Right, Padding.Top + Padding.Bottom);
            desiredSize = desiredSize.Clamp(new Vec2(), Vec2.Max);
            DesiredSize = desiredSize;
        }

        protected virtual Vec2 MeasureOverride(Vec2 availableSize)
        {
            GetChildren().Each(child => child.Measure(availableSize));

            if (SizeToContent)
            {
                var sizes = GetChildren().Select(child => child.DesiredSize).Concat(new[] { new Vec2(0, 0) }).ToList();
                return new Vec2(sizes.Max(size => size.X), sizes.Max(size => size.Y));
            }
            else
            {
                return new Vec2(float.NaN, float.NaN);
            }
        }

        public override string ToString()
        {
            return string.Format("[Element: Type={0}]", this.GetType().Name);
        }
        
	}
}





















