using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;


namespace Zia.UI
{

	public class ScaleTransform :  ElementTransform
	{
		
        public static readonly Property<Vec3, ScaleTransform> ScaleProperty = 
                    Property<Vec3, ScaleTransform>
                        .New()
                        .Tags(TransformParamTag, Element.RenderInvalidationTag)
                        .Name(() => ScaleProperty);

        public Vec3 Scale
        {
            get
            {
                return ScaleProperty.GetValue(this);
            }
            set
            {
                ScaleProperty.SetValue(this, value);
            }
        }
		public override Transform GetTransform (Context context)
		{
			return context.CreateTransform ().Scale (Scale);
		}
	}
	
}
