using System;
using Zia.Gfx;
using Zia.Gfx.Geometry;
using Zia.Common;

namespace Zia.UI.Shapes
{
	public struct CornerRadius
	{
		public float UpperLeft, UpperRight, LowerRight, LowerLeft;

        public CornerRadius(float uniform) : this(uniform, uniform, uniform, uniform)
        {
            
        }

		public CornerRadius (float upperLeft, float upperRight, float lowerRight, float lowerLeft)
		{
			this.UpperLeft = upperLeft;
			this.UpperRight = upperRight;
			this.LowerRight = lowerRight;
			this.LowerLeft = lowerLeft;
		}
	}
	
}
