using System;
using Zia.Gfx.Geometry;
using Zia.UI.Data;

namespace Zia.UI.Shapes
{


	public abstract class Shape : AdvancedElement
	{
        internal static Tag GeometryInvalidationTag = 
            new Tag(() => GeometryInvalidationTag);

        public static readonly Property<Brushes.Brush, Shape> FillProperty = 
                    Property<Brushes.Brush, Shape>
                        .New()
                        .Tags(RenderInvalidationTag)
                        .Name(() => FillProperty);

        public Brushes.Brush Fill
        {
            get
            {
                return FillProperty.GetValue(this);
            }
            set
            {
                FillProperty.SetValue(this, value);
            }
        }

         public static readonly Property<Brushes.Brush, Shape> StrokeProperty = 
                    Property<Brushes.Brush, Shape>
                        .New()
                        .Tags(RenderInvalidationTag)
                        
                        .Name(() => StrokeProperty);
                        
        public Brushes.Brush Stroke
        {
            get 
            {
                return StrokeProperty.GetValue(this);
            }
            set
            {
                StrokeProperty.SetValue(this, value);
            }
        }

        public static readonly Property<EndType, Shape> EndTypeProperty = 
                    Property<EndType, Shape>
                        .New()
                        .Tags(GeometryInvalidationTag, RenderInvalidationTag, LayoutInvalidationTag)
                        .DefaultValue(EndType.ClosedLine)
                        .Name(() => EndTypeProperty);

        public EndType EndType
        {
            get
            {
                return EndTypeProperty.GetValue(this);
            }
            set
            {
                EndTypeProperty.SetValue(this, value);
            }
        }

         public static readonly Property<JoinType, Shape> JoinTypeProperty = 
                    Property<JoinType, Shape>
                        .New()
                        .Tags(GeometryInvalidationTag, RenderInvalidationTag, LayoutInvalidationTag)
                        .DefaultValue(JoinType.Square)
                        .Name(() => JoinTypeProperty);
                        
        public JoinType JoinType
        {
            get 
            {
                return JoinTypeProperty.GetValue(this);
            }
            set
            {
                JoinTypeProperty.SetValue(this, value);
            }
        }

        public static readonly Property<float, Shape> StrokeWidthProperty = 
                    Property<float, Shape>
                        .New()
                        .Tags(GeometryInvalidationTag, RenderInvalidationTag, LayoutInvalidationTag)
                        .Name(() => StrokeWidthProperty);

        public float StrokeWidth
        {
            get
            {
                return StrokeWidthProperty.GetValue(this);
            }
            set
            {
                StrokeWidthProperty.SetValue(this, value);
            }
        }


      

		private PathGeometry _Path;
		private bool _Invalid = true;


		public Shape ()
		{
            AddListener(Listener.New().WithCallback(() => {
                _Invalid = true;
            })
                        .WithTags(GeometryInvalidationTag));
		}

		protected abstract PathGeometry BuildGeometry ();

		private void UpdateGeometry()
		{
			if (_Invalid) {
				_Path = BuildGeometry ();
				_Path.StrokeStyle = new Zia.Gfx.StrokeStyle {
					EndType = EndType,
					JoinType = JoinType,
					Thickness = StrokeWidth
				};
				_Invalid = false;
			}
		}

        protected override void ActivateOverride ()
		{
			_Invalid = true;
		}

        protected void InvalidateShape()
        {
            _Invalid = true;
        }

        protected override void DeactivateOverride ()
		{
            if(_Path != null)
			    _Path.Dispose ();

			_Path = null;
			_Invalid = true;
		}

        protected override bool HitTestOveride(Zia.Common.Vec2 point, Zia.Common.Vec2 localPoint)
        {
            UpdateGeometry();
            return Fill != null && _Path.Contains(localPoint);
        }

        protected override void RenderOverride (Zia.Gfx.Context context, Zia.Gfx.Transform projection, Zia.Gfx.Transform transform, float opacity)
		{

			UpdateGeometry ();

			var geometryTransform = transform * projection;

			var fill = Fill;

			if(fill != null)
                context.FillGeometry (_Path, fill.GetBrush (), geometryTransform, opacity);

			var stroke = Stroke;

			if(stroke != null)
                context.StrokeGeometry (_Path, stroke.GetBrush (), geometryTransform, opacity);
		}
	}
}

