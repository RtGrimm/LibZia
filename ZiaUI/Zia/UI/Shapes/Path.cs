using Zia.Gfx.Geometry;
using Zia.Gfx;
using Zia.Common;
using System;
using System.Collections.Generic;
using Zia.UI.Data;

namespace Zia.UI.Shapes
{
    public abstract class Figure : PropertyBase
	{
		public abstract void AddToPath(PathGeometry path, Vec2 scale);
	}

    public abstract class Segment : PropertyBase
	{
		public abstract void AddToPath(PathGeometry path, Vec2 scale);
	}

	public class Line : Segment
	{
        public static readonly Property<Vec2, Line> ToProperty = 
                    Property<Vec2, Line>
                        .New()
                        .Tags()
                        .Name(() => ToProperty);

        public Vec2 To
        {
            get
            {
                return ToProperty.GetValue(this);
            }
            set
            {
                ToProperty.SetValue(this, value);
            }
        }


		public override void AddToPath(PathGeometry path, Vec2 scale)
		{
            path.AddPoint (To * scale);
		}
	}

	public class CubicCurve : Segment
	{
        public static readonly Property<Vec2, CubicCurve> Point1Property = 
                    Property<Vec2, CubicCurve>
                        .New()
                        .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                        .Name(() => Point1Property);

        public Vec2 Point1
        {
            get
            {
                return Point1Property.GetValue(this);
            }
            set
            {
                Point1Property.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, CubicCurve> Point2Property = 
                    Property<Vec2, CubicCurve>
                        .New()
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                        .Name(() => Point2Property);

        public Vec2 Point2
        {
            get
            {
                return Point2Property.GetValue(this);
            }
            set
            {
                Point2Property.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, CubicCurve> Point3Property = 
                    Property<Vec2, CubicCurve>
                        .New()
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                        .Name(() => Point3Property);

        public Vec2 Point3
        {
            get
            {
                return Point3Property.GetValue(this);
            }
            set
            {
                Point3Property.SetValue(this, value);
            }
        }

        public static readonly Property<int, CubicCurve> ResolutionProperty = 
                    Property<int, CubicCurve>
                        .New()
                        .DefaultValue(100)
                        .Name(() => ResolutionProperty);

        public int Resolution
        {
            get
            {
                return ResolutionProperty.GetValue(this);
            }
            set
            {
                ResolutionProperty.SetValue(this, value);
            }
        }

		public override void AddToPath(PathGeometry path, Vec2 scale)
		{
            path.CubicCurveTo (Resolution, Point1 * scale, Point2 * scale, Point3 * scale);
		}
	}

	public class QuadCurve : Segment
	{
		
		
        public static readonly Property<Vec2, QuadCurve> Point1Property = 
            Property<Vec2, QuadCurve>
                .New()
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                .Name(() => Point1Property);

        public Vec2 Point1
        {
            get
            {
                return Point1Property.GetValue(this);
            }
            set
            {
                Point1Property.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, QuadCurve> Point2Property = 
            Property<Vec2, QuadCurve>
                .New()
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                .Name(() => Point2Property);

        public Vec2 Point2
        {
            get
            {
                return Point2Property.GetValue(this);
            }
            set
            {
                Point2Property.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, QuadCurve> Point3Property = 
            Property<Vec2, QuadCurve>
                .New()
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                .Name(() => Point3Property);

        public Vec2 Point3
        {
            get
            {
                return Point3Property.GetValue(this);
            }
            set
            {
                Point3Property.SetValue(this, value);
            }
        }

        public static readonly Property<int, QuadCurve> ResolutionProperty = 
            Property<int, QuadCurve>
                .New()
                .DefaultValue(100)
                .Name(() => ResolutionProperty);

        public int Resolution
        {
            get
            {
                return ResolutionProperty.GetValue(this);
            }
            set
            {
                ResolutionProperty.SetValue(this, value);
            }
        }


		public override void AddToPath(PathGeometry path, Vec2 scale)
		{
            

            path.QuadCurveTo (Resolution, Point1 * scale, Point2 * scale);
		}
	}

	public class SegmentFigure : Figure
	{

        public static readonly Property<ICollection<Segment>, SegmentFigure> SegmentsProperty = 
                    Property<ICollection<Segment>, SegmentFigure>
                        .New()
                        .Tags(Shape.GeometryInvalidationTag, Element.RenderInvalidationTag) 
                        .ChangedFromCollection(true)
                        .ChangedFromCollectionItems(true)
                        .Name(() => SegmentsProperty);

        public ICollection<Segment> Segments
        {
            get
            {
                return SegmentsProperty.GetValue(this);
            }
            set
            {
                SegmentsProperty.SetValue(this, value);
            }
        }
		
        public static readonly Property<Vec2, SegmentFigure> StartPointProperty = 
                    Property<Vec2, SegmentFigure>
                        .New()
                        .Tags(Shape.GeometryInvalidationTag, 
                              Element.RenderInvalidationTag, 
                              Element.LayoutInvalidationTag)
                        .Name(() => StartPointProperty);

        public Vec2 StartPoint
        {
            get
            {
                return StartPointProperty.GetValue(this);
            }
            set
            {
                StartPointProperty.SetValue(this, value);
            }
        }

	

		public SegmentFigure ()
		{
			Segments = new List<Segment> ();
		}

		public override void AddToPath(PathGeometry path, Vec2 scale)
		{

			path.NewFigure ();
			path.AddPoint (StartPoint * scale);
			Segments.Each (segment => { 
				segment.AddToPath (path, scale);
			});
		}
	}


	public class EllipseFigure : SegmentFigure
	{
        public static readonly Property<Vec2, EllipseFigure> LocationProperty = 
                    Property<Vec2, EllipseFigure>
                        .New()
                        .Tags(Shape.GeometryInvalidationTag, 
                              Element.RenderInvalidationTag, 
                              Element.LayoutInvalidationTag)
                        .Name(() => LocationProperty);

        public Vec2 Location
        {
            get
            {
                return LocationProperty.GetValue(this);
            }
            set
            {
                LocationProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, EllipseFigure> RadiusProperty = 
            Property<Vec2, EllipseFigure>
                        .New()
                        .Tags(Shape.GeometryInvalidationTag, 
                              Element.RenderInvalidationTag, 
                              Element.LayoutInvalidationTag)
                        .Name(() => RadiusProperty);

        public Vec2 Radius
        {
            get
            {
                return RadiusProperty.GetValue(this);
            }
            set
            {
                RadiusProperty.SetValue(this, value);
            }
        }
		

		public override void AddToPath(PathGeometry path, Vec2 scale)
		{
            var radius = Radius * (scale);
            path.Ellipse (Location * scale, radius.X, radius.Y, 400);
		}
	}

	public class RectangleFigure : SegmentFigure
	{
        public static readonly Property<CornerRadius, RectangleFigure> RadiusProperty = 
                    Property<CornerRadius, RectangleFigure>
                        .New()  
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                        .Name(() => RadiusProperty);

        public CornerRadius Radius
        {
            get
            {
                return RadiusProperty.GetValue(this);
            }
            set
            {
                RadiusProperty.SetValue(this, value);
            }
        }

         public static readonly Property<Vec2, RectangleFigure> SizeProperty = 
                    Property<Vec2, RectangleFigure>
                        .New()
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                        .Name(() => SizeProperty);
                        
        public Vec2 Size
        {
            get 
            {
                return SizeProperty.GetValue(this);
            }
            set
            {
                SizeProperty.SetValue(this, value);
            }
        }

         public static readonly Property<Vec2, RectangleFigure> LocationProperty = 
                    Property<Vec2, RectangleFigure>
                        .New()
                .Tags(Shape.GeometryInvalidationTag, 
                      Element.RenderInvalidationTag, 
                      Element.LayoutInvalidationTag)
                        .Name(() => LocationProperty);
                        
        public Vec2 Location
        {
            get 
            {
                return LocationProperty.GetValue(this);
            }
            set
            {
                LocationProperty.SetValue(this, value);
            }
        }

		public override void AddToPath(PathGeometry path, Vec2 scale)
		{
            path.Rectangle (Location * scale, Size * scale);
		}
	}

	public class Path : Shape
	{
        public static readonly Property<ICollection<Figure>, Path> FiguresProperty = 
                    Property<ICollection<Figure>, Path>
                        .New()
                        .ChangedFromCollection(true)
                        .ChangedFromCollectionItems(true)
                        .Name(() => FiguresProperty);

        public ICollection<Figure> Figures
        {
            get
            {
                return FiguresProperty.GetValue(this);
            }
            set
            {
                FiguresProperty.SetValue(this, value);
            }
        }

		public Path ()
		{
			Figures = new List<Figure> ();

            RenderSizeProperty.AddChangedCallback(this, args => {
                InvalidateShape();
            });
		}

		protected override PathGeometry BuildGeometry ()
		{
			var path = new PathGeometry ();
			Figures.Each (figure => figure.AddToPath (path, new Vec2(RenderSize.X, RenderSize.Y)));
			return path;
		}
	}
}

