using System;
using Zia.Gfx;
using Zia.Gfx.Geometry;
using Zia.Common;

namespace Zia.UI.Shapes
{
	public class Ellipse : Shape
	{
		public Ellipse ()
		{
            RenderSizeProperty.AddChangedCallback(this, args => {
                if(args.OldValue != args.NewValue)
                {
                    InvalidateShape();
                }
            });
		}

		protected override Zia.Gfx.Geometry.PathGeometry BuildGeometry ()
		{
			var size = RenderSize;

			var path = new PathGeometry ();
			path.Ellipse (new Vec2(size.X / 2.0f, size.Y / 2.0f), size.X / 2.0f, size.X / 2.0f, 100);

			return path;
		}

	}
}

