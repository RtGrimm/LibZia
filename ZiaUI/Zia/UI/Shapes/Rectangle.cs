using System;
using Zia.Gfx;
using Zia.Gfx.Geometry;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI.Shapes
{

	public class Rectangle : Shape
	{

        public static readonly Property<CornerRadius, Rectangle> CornerRadiusProperty = 
                    Property<CornerRadius, Rectangle>
                        .New()
                        .Tags(GeometryInvalidationTag)
                        .Name(() => CornerRadiusProperty);

        public CornerRadius CornerRadius
        {
            get
            {
                return CornerRadiusProperty.GetValue(this);
            }
            set
            {
                CornerRadiusProperty.SetValue(this, value);
            }
        }
		
		public Rectangle ()
		{
            RenderSizeProperty.AddChangedCallback(this, args => {
                if(args.OldValue != args.NewValue)
                {
                    InvalidateShape();
                }
            });
		}

		protected override PathGeometry BuildGeometry ()
		{
			var path = new PathGeometry ();

			path.RoundRectangle (new Vec2(), RenderSize, 
			                     CornerRadius.UpperLeft, CornerRadius.UpperRight, 
			                     CornerRadius.LowerRight, CornerRadius.LowerLeft, 100);

			return path;

		}
	}
}

