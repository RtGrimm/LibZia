using System;
using System.Collections.Generic;
using Zia.UI.Animation;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI
{
    public class ClockTriggerAction : PropertyBase, ITriggerAction
    {
        public static readonly Property<ICollection<ITimeable>, ClockTriggerAction> AnimationsProperty = 
                    Property<ICollection<ITimeable>, ClockTriggerAction>
                        .New()
                        .Name(() => AnimationsProperty);

        public ICollection<ITimeable> Animations
        {
            get
            {
                return AnimationsProperty.GetValue(this);
            }
            set
            {
                AnimationsProperty.SetValue(this, value);
            }
        }
        public static readonly Property<TimeSpan, ClockTriggerAction> LengthProperty = 
                    Property<TimeSpan, ClockTriggerAction>
                        .New()
                        .Name(() => LengthProperty);

        public TimeSpan Length
        {
            get
            {
                return LengthProperty.GetValue(this);
            }
            set
            {
                LengthProperty.SetValue(this, value);
            }
        }

        private TimeSpanClock _Clock;

        public ClockTriggerAction()
        {
            Animations = new List<ITimeable>();
        }

        public void Execute(AdvancedElement element)
        {
            _Clock = new TimeSpanClock(Length);

            Animations.Each(anim => _Clock.AddTimeable(anim));
            _Clock.Start();
        }
    }
}

