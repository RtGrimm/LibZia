using System;

namespace Zia.UI
{
    public interface ITemplate
    {
        AdvancedElement CreateTree();
    }

    public class CallbackTemplate : ITemplate
    {
        Func<AdvancedElement> _Callback;

        public CallbackTemplate(Func<AdvancedElement> callback)
        {
            this._Callback = callback;
            
        }

        public AdvancedElement CreateTree()
        {
            return _Callback();
        }
    }
}

