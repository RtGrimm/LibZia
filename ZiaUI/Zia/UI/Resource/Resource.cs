﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zia.Common;
using System.Linq;
using System.IO;
using Zia.Gfx.Text;

namespace Zia.UI.Resource
{
    public interface IResourceLocator
    {
        bool CanResolve(string path);
        Task<byte[]> Resolve(string path);
    }

    public interface IResourceLoader
    {
    }

    public interface IResourceLoader<T> : IResourceLoader
    {
        Task<T> Load(byte[] data);
    }

    public class FileLocator : IResourceLocator
    {
        private string _Directory;

        public FileLocator(string directory)
        {
            if (!Directory.Exists(directory))
                throw new ArgumentException("Directory does not exist.");

            _Directory = directory;
        }

        public bool CanResolve (string path)
        {
            var fullPath = Path.Combine(_Directory, path);
            return File.Exists(fullPath);
        }

        public Task<byte[]> Resolve (string path)
        {
            var fullPath = Path.Combine(_Directory, path);
            return Task<byte[]>.Run(() => File.ReadAllBytes(fullPath));
        }
    }

    class ResourceDirectoryLoactor : FileLocator
    {
        public ResourceDirectoryLoactor() :
        base(Path.Combine(Directory.GetCurrentDirectory(), "Resources"))
        {
        }
        
    }

    public class FontLoader : IResourceLoader<Font>
    {
        public async Task<Font> Load (byte[] data)
        {
            return await Font.CreateAsync(data);
        }
    }

    public class Resource
    {
        private ICollection<IResourceLocator> _Locators = 
            new List<IResourceLocator>();

        public Resource()
        {
            _Locators.Add(new ResourceDirectoryLoactor());
        }

        private async Task<T> LoadImpl<Loader, T>(string path) where Loader : IResourceLoader<T>, new()
        {
            var data = await _Locators.First(loc => loc.CanResolve(path)).Resolve(path);

            var loader = new Loader();
            return await loader.Load(data);
        }

        public static async Task<T> Load<Loader, T>(string path) where Loader : IResourceLoader<T>, new()
        {
            return await Singleton<Resource>.Instance.LoadImpl<Loader, T>(path);
        }
    }
}

