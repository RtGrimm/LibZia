using System;
using System.Collections.Generic;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI.Animation
{


    public class PropertyAnimationTarget<T, TTarget> : IAnimationTarget<T> 
        where T : struct  
            where TTarget : PropertyBase
    {

        public T CurrentValue
        {
            get
            {
                return _Property.GetValue(_Target);
            }
        }

        TTarget _Target;
        Property<T, TTarget> _Property;

        public PropertyAnimationTarget(TTarget target, Property<T, TTarget>  property)
        {
            this._Property = property;
            this._Target = target;
        }

        public void Enable()
        {

        }

        public void Disable()
        {

        }

        public void SetValue(T value)
        {
                _Property.SetValue(_Target, value);
        }
    }

}
