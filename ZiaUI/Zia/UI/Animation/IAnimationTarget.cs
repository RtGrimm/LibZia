using System;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.UI.Animation
{
     
    public interface IAnimationTarget<T> where T : struct
    {
        void Enable();
        void Disable();
        void SetValue(T value);
        T CurrentValue { get; }
    }

}
