using System;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.UI.Animation
{

    public class TimeSpanClock : UpdateDependentClock, IClock
    {
        private DateTime _Start;
        private DateTime _End;

        private TimeSpan _PauseOffset;
        public bool IsPaused
        {
            get;
            private set;
        }

        public override bool IsComplete
        {
            get
            {
                return  DateTime.Now > _End;
            }
        }

        private ICollection<ITimeable> _UpdateTargets = 
            new List<ITimeable>();

        public event Action Ended = () => {};

        public override void  Update()
        {
            if (DateTime.Now > _Start && DateTime.Now < _End)
            {
                var timeFraction = GetTimeFraction();
                _UpdateTargets.Each(target => target.Update(timeFraction));
            }
        }



        TimeSpan _Span;

        public TimeSpanClock(TimeSpan span)
        {
            this._Span = span;
        }

        public override void Complete()
        {
            Ended();
            _UpdateTargets.Each(target => target.Disable());
        }

        public void Start()
        {
            _Start = DateTime.Now;
            _End = DateTime.Now + _Span;

            IsPaused = false;
            _UpdateTargets.Each(target => target.Enable());
            base.Resume();
        }

        public override void Pause()
        {
            _PauseOffset = DateTime.Now - _Start;
            _UpdateTargets.Each(target => target.Disable());
            IsPaused = true;
            base.Pause();
        }

        public override void Resume()
        {
            _Start = (DateTime.Now) - _PauseOffset;
            _End = (DateTime.Now + _Span) - _PauseOffset;

            _UpdateTargets.Each(target => target.Enable());
            IsPaused = false;

            base.Resume();
        }

        public void Reset()
        {
            _Start = new DateTime();
            _End = new DateTime();

            IsPaused = true;

            base.Pause();
        }

        public void AddTimeable(ITimeable updateTarget)
        {
            _UpdateTargets.Add(updateTarget);
        }

        public void RemoveTimeable(ITimeable updateTarget)
        {
            _UpdateTargets.Remove(updateTarget);
        }

        private float GetTimeFraction()
        {
            var currentTime = DateTime.Now;
            var length = (_End - _Start).TotalMilliseconds;

            var localTime = (currentTime - _Start).TotalMilliseconds;
            var timeFraction = (float)(localTime / length);
            return MathUtil.Clamp(timeFraction, 0, 1);
        }
    }

}