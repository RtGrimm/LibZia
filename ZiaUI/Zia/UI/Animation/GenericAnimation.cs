using System;
using System.Collections.Generic;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI.Animation
{
  

    public class GenericAnimation<T> : PropertyBase, ITimeable where T : struct
    {
        public static readonly Property<IAnimationTarget<T>, GenericAnimation<T>> TargetProperty = 
                    Property<IAnimationTarget<T>, GenericAnimation<T>>
                        .New()
                        .Name(() => TargetProperty);

        public IAnimationTarget<T> Target
        {
            get
            {
                return TargetProperty.GetValue(this);
            }
            set
            {
                TargetProperty.SetValue(this, value);
            }
        }

        public static readonly Property<IInterpolator, GenericAnimation<T>> InterpolatorProperty = 
                    Property<IInterpolator, GenericAnimation<T>>
                        .New()
                        .Name(() => InterpolatorProperty);

        public IInterpolator Interpolator
        {
            get
            {
                return InterpolatorProperty.GetValue(this);
            }
            set
            {
                InterpolatorProperty.SetValue(this, value);
            }
        }

        public static readonly Property<T, GenericAnimation<T>> StartProperty = 
                   Property<T, GenericAnimation<T>>
                       .New()
                       .Name(() => StartProperty);

        public T Start
        {
            get
            {
                return StartProperty.GetValue(this);
            }
            set
            {
                StartProperty.SetValue(this, value);
            }
        }

        public static readonly Property<T, GenericAnimation<T>> EndProperty = 
                    Property<T, GenericAnimation<T>>
                        .New()
                        .Name(() => EndProperty);

        public T End
        {
            get
            {
                return EndProperty.GetValue(this);
            }
            set
            {
                EndProperty.SetValue(this, value);
            }
        }


        public static readonly Property<bool, GenericAnimation<T>> FromCurrentValueProperty = 
                    Property<bool, GenericAnimation<T>>
                        .New()
                        .Name(() => FromCurrentValueProperty);

        public bool FromCurrentValue
        {
            get
            {
                return FromCurrentValueProperty.GetValue(this);
            }
            set
            {
                FromCurrentValueProperty.SetValue(this, value);
            }
        }

        public GenericAnimation(T startValue, T endValue, IInterpolator function, IAnimationTarget<T> target)
        {
            Start = startValue;
            End = endValue;
            Target = target;
            Interpolator = function;

            FromCurrentValue = true;
        }

        public GenericAnimation()
        {
            FromCurrentValue = true;
        }

        public void Enable()
        {
            if (Target != null && FromCurrentValue)
            {
                Start = Target.CurrentValue;
            }
        }

        public void Disable()
        {

        }

        public void Update(float timeFraction)
        {
            if (Target != null && Interpolator != null)
            {
                

                var interpolator = new AnimationInterpolator<T>(Interpolator);
                var value = interpolator.Eval(Start, End, timeFraction);

                //Console.WriteLine("Setting value {0} on {1} with start a value of{2} and an end value of {3}.", value, Target, Start, End);

                Target.SetValue(value);
            }
        }
    }
}

