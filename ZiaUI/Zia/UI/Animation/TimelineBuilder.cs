using System;
using System.Collections.Generic;
using Zia.UI.Data;


namespace Zia.UI.Animation
{
    
    public class AnimationBuilder<T, Sub> 
        where T : struct 
        where Sub : AnimationBuilder<T, Sub>
    {
        protected GenericAnimation<T> Anim
        {
            get;
            private set;
        }

        private Sub _Sub;

        public AnimationBuilder()
        {
            Anim = new GenericAnimation<T>();
            _Sub = (Sub)this;
        }

        public Sub Target<TOwner>(TOwner owner, Property<T, TOwner> property) where TOwner : PropertyBase
        {
            Anim.Target = new PropertyAnimationTarget<T, TOwner>(owner, property);
            return _Sub;
        }

        public Sub Target(IAnimationTarget<T> target)
        {
            Anim.Target = target;
            return _Sub;
        }

        public Sub End(T end)
        {
            Anim.End = end;
            return _Sub;
        }

        public Sub Start(T start)
        {
            Anim.FromCurrentValue = false;
            Anim.Start = start;
            return _Sub;
        }

        public Sub Interpolator(Func<float, float> interpolator)
        {
            Anim.Interpolator = new InterpolatorFunction(interpolator);
            return _Sub;
        }

        public Sub Interpolator(IInterpolator interpolator)
        {
                Anim.Interpolator = interpolator;
            return _Sub;
        }

        public GenericAnimation<T> Animation()
        {
            return Anim;
        }

        public static implicit operator GenericAnimation<T>(AnimationBuilder<T, Sub> builder)
        {
            return builder.Animation();
        }
    }

    public class AnimationBuilder<T> : 
    AnimationBuilder<T, AnimationBuilder<T>>  where T : struct 
    {
        public static AnimationBuilder<T> New()
        {
            return new AnimationBuilder<T>();
        }
    }

    public class ClockAnimationBuilder<T> : AnimationBuilder<T, ClockAnimationBuilder<T>> where T : struct
    {
        private TimeSpan _Length;

        public ClockAnimationBuilder<T> Length(TimeSpan length)
        {
            _Length = length;
            return this;
        }

        public TimeSpanClock Clock()
        {
            var clock = new TimeSpanClock(_Length);
            clock.AddTimeable(Anim);

            return clock;
        }

        public TimeSpanClock Start()
        {
            var clock = Clock();
            clock.Start();

            return clock;
        }

        public static ClockAnimationBuilder<T> New()
        {
            return new ClockAnimationBuilder<T>();
        }
    }

    public class TimelineBuilder
    {

        public class TimelineAnimationBuilder<T> : AnimationBuilder<T, TimelineAnimationBuilder<T>> where T : struct
        {
            TimelineBuilder _Parent;
            private TimelineItem _Item = new TimelineItem();

            public TimelineAnimationBuilder(TimelineBuilder parent)
            {
                this._Parent = parent;   
                _Parent._Timeline.Items.Add(_Item);
                _Item.Target = Anim;
            }

            public TimelineAnimationBuilder<T> TimeStart(float start)
            {
                _Item.Start = start;
                return this;
            }

            public TimelineAnimationBuilder<T> TimeEnd(float end)
            {
                _Item.End = end;
                return this;
            }

            public TimelineAnimationBuilder<U> Animation<U>() where U : struct
            {
                return new TimelineAnimationBuilder<U>(_Parent);
            }

            public TimelineBuilder Timeline()
            {
                return _Parent;
            }
        }

        private TimeSpan _Length;
        private Timeline _Timeline = new Timeline();

        public TimelineBuilder()
        {
        }

        public TimelineBuilder Length(TimeSpan length)
        {
            _Length = length;
            return this;
        }

        public TimelineAnimationBuilder<U> Animation<U>() where U : struct
        {
            return new TimelineAnimationBuilder<U>(this);
        }

        TimeSpanClock Clock()
        {
            var clock = new TimeSpanClock(_Length);
            clock.AddTimeable(_Timeline);
            return clock;
        }

        public static TimelineBuilder New()
        {
            return new TimelineBuilder();
        }

        public TimeSpanClock Start()
        {
            var clock = Clock();

            clock.Start();

            return clock;
        }
    }
}

