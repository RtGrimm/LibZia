using System;
using Zia.Common;

namespace Zia.UI.Animation
{
    public abstract class UpdateDependentClock
    {
        public abstract bool IsComplete { get; }

        public UpdateDependentClock()
        {

        }

        public virtual void Complete()
        {
            
        }

        public virtual void Resume()
        {
            Singleton<ClockManager>.Instance.RegisterClock(this);
        }

        public virtual void Pause()
        {
            Singleton<ClockManager>.Instance.UnregisterClock(this);
        }

        public abstract void Update();
    }
}

