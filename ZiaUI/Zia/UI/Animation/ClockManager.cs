using System;
using System.Linq;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.UI.Animation
{
    internal class ClockManager
    {
        private ICollection<UpdateDependentClock> _Clocks = new List<UpdateDependentClock>();

        public void Update()
        {
            var removeClocks = _Clocks.Where(clock => clock.IsComplete).ToList();
            removeClocks.Each(clock => {
                _Clocks.Remove(clock);
                clock.Complete();
            });

            _Clocks.Each(clock => clock.Update());

        }

        public void RegisterClock(UpdateDependentClock clock)
        {
            _Clocks.Add(clock);
        }

        public void UnregisterClock(UpdateDependentClock clock)
        {
            _Clocks.Remove(clock);
        }
    }
}

