using System;
using Zia.UI.Data;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;

namespace Zia.UI.Animation
{
    public class TimelineItem : PropertyBase
    {
        public static readonly Property<ITimeable, TimelineItem> TargetProperty = 
                    Property<ITimeable, TimelineItem>
                        .New()
                        .Name(() => TargetProperty);

        public ITimeable Target
        {
            get
            {
                return TargetProperty.GetValue(this);
            }
            set
            {
                TargetProperty.SetValue(this, value);
            }
        }

        public static readonly Property<float, TimelineItem> StartProperty = 
                    Property<float, TimelineItem>
                        .New()
                        .Name(() => StartProperty);

        public float Start
        {
            get
            {
                return StartProperty.GetValue(this);
            }
            set
            {
                StartProperty.SetValue(this, value);
            }
        }

        public static readonly Property<float, TimelineItem> EndProperty = 
                    Property<float, TimelineItem>
                        .New()
                        .Name(() => EndProperty);

        public float End
        {
            get
            {
                return EndProperty.GetValue(this);
            }
            set
            {
                EndProperty.SetValue(this, value);
            }
        }

        internal bool Enabled
        {
            get;
            set;
        }

        public TimelineItem(Zia.UI.Animation.ITimeable target, float start, float end)
        {
            this.Target = target;
            this.Start = start;
            this.End = end;
        }
        

        public TimelineItem()
        {
            
        }
    }

    public class Timeline : PropertyBase, ITimeable
    {
        public static readonly Property<ICollection<TimelineItem>, Timeline> ItemsProperty = 
                    Property<ICollection<TimelineItem>, Timeline>
                        .New()
                        .Name(() => ItemsProperty);

        public ICollection<TimelineItem> Items
        {
            get
            {
                return ItemsProperty.GetValue(this);
            }
            set
            {
                ItemsProperty.SetValue(this, value);
            }
        }

        public Timeline()
        {
            Items = new List<TimelineItem>();
        }

        public void Disable()
        {

        }

        public void Enable()
        {

        }

        public void Update(float timeFraction)
        {
            Items.Where(item => !(item.Start < timeFraction && item.End > timeFraction)).Each(item => {
                if(item.Enabled)
                {
                    item.Target.Disable();
                    item.Enabled = false;
                }
            });


            var items = Items.Where(item => item.Start < timeFraction && item.End > timeFraction);



            items.Each(item => {
                if(!item.Enabled)
                {
                    item.Target.Enable();
                    item.Enabled = true;
                }

                var localLength = item.End - item.Start;
                var localTime = (timeFraction - item.Start) / localLength;
                item.Target.Update(localTime);
            });
        }
    }
}

