using System;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.UI.Animation
{

    public interface ITimeable
    {
        void Disable();
        void Enable();

        void Update(float timeFraction);

    }
    
}
