using System;
using System.Linq.Expressions;
using Zia.Common;

namespace Zia.UI.Animation
{
    public interface IInterpolator
    {
        float Eval(float time);
    }

    public class InterpolatorFunction : IInterpolator
    {
        Func<float, float> _Callback;

        public InterpolatorFunction(Func<float, float> callback)
        {
            this._Callback = callback;
            
        }

        public float Eval(float time)
        {
            return _Callback(time);
        }
    }


    public class AnimationInterpolator<T>
    {
        private static MathOpGenerator<T, T> _SameTypeOpGenerator =
            new MathOpGenerator<T, T>(true, true, false, false);

        private static MathOpGenerator<T, float> _FloatTypeOpGenerator =
            new MathOpGenerator<T, float>(false, false, true, false);

        public IInterpolator InterpolatorFunction
        {
            get;
            set;
        }

        public AnimationInterpolator(IInterpolator interpolatorFunction)
        {
            InterpolatorFunction = interpolatorFunction;
        }


        public T Eval(T startValue, T endValue, float timeFraction)
        {
            return _SameTypeOpGenerator.Add(startValue, _FloatTypeOpGenerator.Multiply(
                _SameTypeOpGenerator.Subtract(endValue, startValue), 
                InterpolatorFunction.Eval(MathUtil.Clamp(timeFraction, 0, 1))));
        }
    }
}

