using System;
using System.Linq.Expressions;
using Zia.Common;

namespace Zia.UI.Animation
{
    class MathOpGenerator<T, U>
    {
        public Func<T, U, T> Add
        {
            get;
            private set;
        }

        public Func<T, U, T> Subtract
        {
            get;
            private set;
        }

        public Func<T, U, T> Multiply
        {
            get;
            private set;
        }

        public Func<T, U, T> Divide
        {
            get;
            private set;
        }

        public MathOpGenerator(bool add = true, bool sub = true, bool mul = true, bool div = true)
        {
            if (add)
                Add = GenAdd();
            if (sub)
                Subtract = GenSubtract();
            if (mul)
                Multiply = GenMultiply();
            if (div)
                Divide = GenDivide();
        }

        private Func<T, U, T> GenAdd()
        {
            return BinOp((left, right) => Expression.Add(left, right));
        }

        private Func<T, U, T> GenSubtract()
        {
            return BinOp((left, right) => Expression.Subtract(left, right));
        }

        private Func<T, U, T> GenMultiply()
        {
            return BinOp((left, right) => Expression.Multiply(left, right));
        }

        private Func<T, U, T> GenDivide()
        {
            return BinOp((left, right) => Expression.Divide(left, right));
        }

        private Func<T, U, T> BinOp(Func<Expression, Expression, Expression> opFactory)
        {
            var left = Expression.Parameter(typeof(T), "left");
            var right = Expression.Parameter(typeof(U), "right");

            return Expression.Lambda<Func<T, U, T>>
                (opFactory(left, right), new[] { left, right }).Compile();
        }
    }
    
}
