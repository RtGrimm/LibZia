using System;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.UI.Animation
{
    public interface IClock
    {
        void AddTimeable(ITimeable updateTarget);
        void RemoveTimeable(ITimeable updateTarget);


    }
    
}
