using System;
using Zia.Common;
using System.Linq;
using System.Collections.Generic;
using Zia.UI.Data;

namespace Zia.UI
{

    public interface ISetter<TOwner>
    {
        void ApplyValue(TOwner owner);
    }

    public class Setter<T, TOwner> : PropertyBase, ISetter<TOwner> where TOwner : PropertyBase
    {
        public static readonly Property<Property<T,TOwner>, Setter<T,TOwner>> TargetProperty = 
            Property<Property<T,TOwner>, Setter<T,TOwner>>
                .New()
                .Name(() => TargetProperty);

        public Property<T,TOwner> Target
        {
            get
            {
                return TargetProperty.GetValue(this);
            }
            set
            {
                TargetProperty.SetValue(this, value);
            }
        }

        public static readonly Property<T, Setter<T, TOwner>> ValueProperty = 
                    Property<T, Setter<T, TOwner>>
                        .New()
                        .Name(() => ValueProperty);

        public T Value
        {
            get
            {
                return ValueProperty.GetValue(this);
            }
            set
            {
                ValueProperty.SetValue(this, value);
            }
        }
       
        public Setter(Property<T, TOwner> target, T value)
        {
            this.Target = target;
            this.Value = value;
        }

        public Setter()
        {
            
        }

        public void ApplyValue(TOwner owner)
        {
            Target.SetValue(owner, Value);
        }
    }

    public interface IStyle
    {
        void Apply(AdvancedElement element);
    }

    public class Style<TOwner> : PropertyBase, IStyle where TOwner : AdvancedElement
    {
        public static readonly Property<ICollection<ISetter<TOwner>>, Style<TOwner>> SettersProperty = 
                    Property<ICollection<ISetter<TOwner>>, Style<TOwner>>
                        .New()
                        .Name(() => SettersProperty);

        public ICollection<ISetter<TOwner>> Setters
        {
            get
            {
                return SettersProperty.GetValue(this);
            }
            set
            {
                SettersProperty.SetValue(this, value);
            }
        }

        public Style()
        {
            Setters = new List<ISetter<TOwner>>();
        }

        public void Apply(AdvancedElement element)
        {
            Apply((TOwner)element);
        }

        public void Apply(TOwner target)
        {
            Setters.Each(setter => setter.ApplyValue(target));
        }
    }
}

