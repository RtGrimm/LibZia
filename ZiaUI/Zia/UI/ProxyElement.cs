using System;
using System.Linq;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI
{
    public class ProxyOverride : PropertyBase
    {
         public static readonly Property<string, ProxyOverride> NameProperty = 
                    Property<string, ProxyOverride>
                        .New()
                        .Name(() => NameProperty);
                        
        public string Name
        {
            get 
            {
                return NameProperty.GetValue(this);
            }
            set
            {
                NameProperty.SetValue(this, value);
            }
        }

         public static readonly Property<ITemplate, ProxyOverride> TemplateProperty = 
                    Property<ITemplate, ProxyOverride>
                        .New()
                        .Name(() => TemplateProperty);
                        
        public ITemplate Template
        {
            get 
            {
                return TemplateProperty.GetValue(this);
            }
            set
            {
                TemplateProperty.SetValue(this, value);
            }
        }
                
        public ProxyOverride(string name, ITemplate template)
        {
            this.Name = name;
            this.Template = template;
        }

        public ProxyOverride()
        {
        }
        
    }

    public class ProxyElement<Target> : AdvancedElement where Target : ProxyRoot<Target>
    {

        public static readonly Property<string, ProxyElement<Target>> ProxyNameProperty = 
            Property<string, ProxyElement<Target>>
                        .New()
                        .Name(() => ProxyNameProperty);

        public static readonly DataContext<Target, Element> ProxyDataContext = 
            new DataContext<Target, Element>();

        public string ProxyName
        {
            get
            {
                return ProxyNameProperty.GetValue(this);
            }
            set
            {
                ProxyNameProperty.SetValue(this, value);
            }
        }

        private Element _ProxyRoot;
        private ITemplate _CurrentTemplate;

        public ProxyElement()
        {
            Adopted += parent => 
                UpdateProxy(ProxyName);

            ProxyNameProperty.AddChangedCallback(this, args => UpdateProxy(ProxyName));
            ProxyRoot<Target>.UpdateProxyOverrideEvent.AddHandler(this, args => UpdateProxy(args.Name));
        }

        public ProxyElement(string proxyName) : this()
        {
            this.ProxyName = proxyName;
        }
        

        protected override void ActivateOverride()
        {
            UpdateProxy(ProxyName);
        }


        private ProxyRoot<Target> Root() => this
            .Ancestors()
            .OfType<ProxyRoot<Target>>()
            .FirstOrDefault();
        
        private void UpdateProxy(string name)
        {
            if (name != this.ProxyName )
                return;

            var proxyOverride = Root()?.ResolveOverride(ProxyName);

            if (proxyOverride == null || proxyOverride.Template == _CurrentTemplate)
                return;
            
                if(_ProxyRoot != null)
                    RemovePhantomChild(_ProxyRoot);
                
                _ProxyRoot = proxyOverride.Template.CreateTree();

                AddPhantomChild(_ProxyRoot);
            _CurrentTemplate = proxyOverride.Template;

        }
    }
}

