using System;

namespace Zia.UI
{
    public interface ITriggerAction
    {
        void Execute(AdvancedElement element);
    }

    public class TriggerActionCallback : ITriggerAction
    {
        Action<Element> _Callback;

        public TriggerActionCallback(Action<Element> callback)
        {
            this._Callback = callback;   
        }

        public void Execute(AdvancedElement element)
        {
            _Callback(element);
        }
    }
}

