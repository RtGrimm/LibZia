using System;
using System.Collections.Generic;
using Zia.UI.Data;

namespace Zia.UI
{
    public class EventTrigger<TEventOwner, TArgs> : PropertyBase, ITrigger 
        where TEventOwner : AdvancedElement
            where TArgs : RoutedEventArgs<TEventOwner>, new()
    {
        public ICollection<ITriggerAction> EnterActions
        {
            get;
            set;
        }

        public ICollection<ITriggerAction> ExitActions
        {
            get;
            set;
        }

        public event Action<ITrigger> Enter;
        public event Action<ITrigger> Exit;



        public static readonly Property<RoutedEvent<TArgs, TEventOwner>, EventTrigger<TEventOwner, TArgs>> TargetEventProperty = 
            Property<RoutedEvent<TArgs, TEventOwner>, EventTrigger<TEventOwner, TArgs>>
                        .New()
                        .Name(() => TargetEventProperty);

        public RoutedEvent<TArgs, TEventOwner> TargetEvent
        {
            get
            {
                return TargetEventProperty.GetValue(this);
            }
            set
            {
                TargetEventProperty.SetValue(this, value);
            }
        }

        public void OnAttach(AdvancedElement target)
        {
            TargetEvent.AddHandler((TEventOwner)target, OnEvent);
        }

        private void OnEvent(TArgs args)
        {
            Enter(this);
        }

        public void OnDetach(AdvancedElement target)
        {
            TargetEvent.RemoveHandler((TEventOwner)target, OnEvent);
        }


    }
}

