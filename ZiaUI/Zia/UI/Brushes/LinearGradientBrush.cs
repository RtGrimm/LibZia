using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI.Brushes
{
	public class LinearGradientBrush : GradientBrush
	{
        public static readonly Property<Vec2, LinearGradientBrush> StartProperty = 
                    Property<Vec2, LinearGradientBrush>
                        .New()
                        .Tags(Element.RenderInvalidationTag)
                        .Name(() => StartProperty);

        public Vec2 Start
        {
            get
            {
                return StartProperty.GetValue(this);
            }
            set
            {
                StartProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, LinearGradientBrush> EndProperty = 
            Property<Vec2, LinearGradientBrush>
                .New()
                .Tags(Element.RenderInvalidationTag)
                .Name(() => EndProperty);

        public Vec2 End
        {
            get
            {
                return EndProperty.GetValue(this);
            }
            set
            {
                EndProperty.SetValue(this, value);
            }
        }
		
		internal override Zia.Gfx.Brush GetBrush ()
		{
			var brush = new Zia.Gfx.LinearGradientBrush  {
				Start = Start,
				End = End
			};

			UpdateStops (brush);

			return brush;
		}
	}
}

