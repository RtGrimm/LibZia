using System;
using Zia.Gfx;
using Zia.Common;
using System.Collections.Generic;
using System.Linq;
using Zia.UI.Data;


namespace Zia.UI.Brushes
{
	public class GradientStop : PropertyBase
	{
		
        public static readonly Property<Color, GradientStop> ColorProperty = 
                    Property<Color, GradientStop>
                        .New()
                        .Tags(Element.RenderInvalidationTag)
                        .Name(() => ColorProperty);

        public Color Color
        {
            get
            {
                return ColorProperty.GetValue(this);
            }
            set
            {
                ColorProperty.SetValue(this, value);
            }
        }

         public static readonly Property<float, GradientStop> OffsetProperty = 
                    Property<float, GradientStop>
                        .New()
                        .Tags(Element.RenderInvalidationTag)
                        .Name(() => OffsetProperty);
                        
        public float Offset
        {
            get 
            {
                return OffsetProperty.GetValue(this);
            }
            set
            {
                OffsetProperty.SetValue(this, value);
            }
        }

        public GradientStop()
        {
            
        }

        public GradientStop(Color color, float offset)
        {
            Color = color;
            Offset = offset;
        }
	}

	public abstract class GradientBrush : Brush
	{
		
        public static readonly Property<ICollection<GradientStop>, GradientBrush> StopsProperty = 
                    Property<ICollection<GradientStop>, GradientBrush>
                        .New()
                        .Tags(Element.RenderInvalidationTag)
                        .ChangedFromCollectionItems(true)
                        .ChangedFromCollection(true)
                        .Name(() => StopsProperty);

        public ICollection<GradientStop> Stops
        {
            get
            {
                return StopsProperty.GetValue(this);
            }
            set
            {
                StopsProperty.SetValue(this, value);
            }
        }

		protected void UpdateStops(Zia.Gfx.GradientBrush brush)
		{
			brush.Stops = Stops.Select (
				stop => new Zia.Gfx.GradientStop(stop.Color, stop.Offset))
				.ToList ();
			;
		}
	}
}

