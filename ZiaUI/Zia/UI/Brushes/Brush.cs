using System;
using Zia.Gfx;
using Zia.UI.Data;


namespace Zia.UI.Brushes
{
	public abstract class Brush : PropertyBase
	{
		


		public Brush ()
		{
		}

		protected void UpdateBrush(Zia.Gfx.Brush brush)
		{
		}

		internal abstract Zia.Gfx.Brush GetBrush ();

        public static implicit operator Zia.Gfx.Brush(Brush brush)
        {
            return brush.GetBrush();
        }
	}
}

