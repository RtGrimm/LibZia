using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI.Brushes
{
	public class RadialGradientBrush : GradientBrush
	{
		
        public static readonly Property<Vec2, RadialGradientBrush> CenterProperty = 
                    Property<Vec2, RadialGradientBrush>
                        .New()
                        .Tags(Element.RenderInvalidationTag)
                        .Name(() => CenterProperty);

        public Vec2 Center
        {
            get
            {
                return CenterProperty.GetValue(this);
            }
            set
            {
                CenterProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, RadialGradientBrush> RadiusProperty = 
            Property<Vec2, RadialGradientBrush>
                .New()
                .Tags(Element.RenderInvalidationTag)
                .Name(() => RadiusProperty);

        public Vec2 Radius
        {
            get
            {
                return RadiusProperty.GetValue(this);
            }
            set
            {
                RadiusProperty.SetValue(this, value);
            }
        }

		internal override Zia.Gfx.Brush GetBrush ()
		{
			var brush = new Zia.Gfx.RadialGradientBrush  {
				Center = Center,
				Radius = Radius
			};

			UpdateStops (brush);

			return brush;
		}
	}
}

