using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI.Brushes
{
	public class SolidBrush : Brush
	{
        public static readonly Property<Color, SolidBrush> ColorProperty = 
                    Property<Color, SolidBrush>
                        .New()
                        .Tags(Element.RenderInvalidationTag)
                        .Name(() => ColorProperty);

        public Color Color
        {
            get
            {
                return ColorProperty.GetValue(this);
            }
            set
            {
                ColorProperty.SetValue(this, value);
            }
        }
		
        public SolidBrush()
        {
            
        }

        public SolidBrush(Color color)
        {
            this.Color = color;
        }
        

		internal override Zia.Gfx.Brush GetBrush ()
		{
			return new Zia.Gfx.SolidBrush (Color);
		}
	}
}

