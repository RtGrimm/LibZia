using System;
using System.Collections.Generic;

namespace Zia.UI
{
    public interface ITrigger
    {
        event Action<ITrigger> Enter;
        event Action<ITrigger> Exit;

        void OnAttach(AdvancedElement target);
        void OnDetach(AdvancedElement target);

        ICollection<ITriggerAction> EnterActions { get; }
        ICollection<ITriggerAction> ExitActions { get; }
    }
}

