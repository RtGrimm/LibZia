using System;
using Zia.Gfx.Text;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;


namespace Zia.UI
{
	public class TextBlock : AdvancedElement
	{
		internal static Tag TextLayoutInvalidationTag = new Tag("TextLayoutInvalidation");

        public static readonly Property<string, TextBlock> TextProperty = 
                    Property<string, TextBlock>
                        .New()
                        .Tags(RenderInvalidationTag, TextLayoutInvalidationTag)
                        .Name(() => TextProperty);

        public string Text
        {
            get
            {
                return TextProperty.GetValue(this);
            }
            set
            {
                TextProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, TextBlock> WrapProperty = 
                    Property<bool, TextBlock>
                        .New()
                        .Tags(RenderInvalidationTag, TextLayoutInvalidationTag)
                        .Name(() => WrapProperty);

        public bool Wrap
        {
            get
            {
                return WrapProperty.GetValue(this);
            }
            set
            {
                WrapProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Font, TextBlock> FontProperty = 
                    Property<Font, TextBlock>
                        .New()
                        .Tags(RenderInvalidationTag, TextLayoutInvalidationTag) 
                        .Name(() => FontProperty);

        public Font Font
        {
            get
            {
                return FontProperty.GetValue(this);
            }
            set
            {
                FontProperty.SetValue(this, value);
            }
        }


        public static readonly Property<float, TextBlock> FontSizeProperty = 
            Property<float, TextBlock>
                        .New()
                        .DefaultValue(20)
                        .Name(() => FontSizeProperty);
                        
        public float FontSize
        {
            get 
            {
                return FontSizeProperty.GetValue(this);
            }
            set
            {
                FontSizeProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Brushes.Brush, TextBlock> BrushProperty = 
                  Property<Brushes.Brush, TextBlock>
                      .New()
                      .Name(() => BrushProperty);

        public Brushes.Brush Brush
        {
            get
            {
                return BrushProperty.GetValue(this);
            }
            set
            {
                BrushProperty.SetValue(this, value);
            }
        }


		protected TextLayout TextLayout
        {
            get;
            private set;
        }

		public TextBlock ()
		{
			
            SubscribeTextEvents();



            TextLayout = new TextLayout();

			Text = "";
		}

        void SubscribeTextEvents()
        {
            AddListener(Listener.New().WithTags(TextLayoutInvalidationTag).WithCallback(() => {
                LayoutInvalidateEvent.Raise(this);
            }));


            WrapProperty.AddChangedCallback(this, args => TextLayout.Wrap = Wrap);
            TextProperty.AddChangedCallback(this, args => TextLayout.Text = Text);

            FontSizeProperty.AddChangedCallback(this, args => 
            {
                if (Font != null)
                    TextLayout.Size = FontSize;
            });

            FontProperty.AddChangedCallback(this, args => 
            {
                TextLayout.Font = Font;
                TextLayout.Size = FontSize;
            });

            /*FillWidthProperty.OverrideMetadata(typeof(TextBlock), new PropertyMetadata {
                DefaultValue = false
            });

            FillHeightProperty.OverrideMetadata(typeof(TextBlock), new PropertyMetadata {
                DefaultValue = false
            });*/

        }

        protected virtual void UpdateText()
        {
            TextLayout.UpdateLayout();
        }

        protected override Vec2 MeasureOverride(Vec2 availableSize)
        {
            TextLayout.MaxSize = availableSize;
            UpdateText();

            return TextLayout.TextSize;
        }

        protected override Vec2 ArrangeOverride(Vec2 finalSize, Vec2 finalLocation)
        {
            TextLayout.MaxSize = finalSize;
            UpdateText();

            return finalSize;
        }



        protected override void RenderOverride (Context context, Transform projection, Transform transform, float opacity)
		{
            var brush = Brush;

            if (brush != null && Font != null) {
                context.DrawTextLayout (TextLayout, brush.GetBrush (), transform * projection, opacity);
            }

		}
	}
}

