﻿using System;
using Zia.UI.Data;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;

namespace Zia.UI
{
    public class ProxyRoot<Sub> : ProxyElement<Sub> where Sub : ProxyRoot<Sub>
    {
        public static readonly Property<ICollection<ProxyOverride>, AdvancedElement> ProxyOverridesProperty = 
            Property<ICollection<ProxyOverride>, AdvancedElement>
            .New()
            .CoerceToObservableCollection()
            .Name(() => ProxyOverridesProperty);

        public ICollection<ProxyOverride> ProxyOverrides
        {
            get
            {
                return ProxyOverridesProperty.GetValue(this);
            }
            set
            {
                ProxyOverridesProperty.SetValue(this, value);
            }
        }

        public class ProxyUpdateEventArgs : RoutedEventArgs<ProxyElement<Sub>>
        {
            public string Name
            {
                get;
                set;
            }

            public ProxyUpdateEventArgs(string name)
            {
                this.Name = name;
            }

            public ProxyUpdateEventArgs()
            {
                
            }
            
        }

        public static RoutedEvent<ProxyUpdateEventArgs, ProxyElement<Sub>> UpdateProxyOverrideEvent = 
            new RoutedEvent<ProxyUpdateEventArgs, ProxyElement<Sub>>(() => UpdateProxyOverrideEvent, RoutingStrategy.Tunnel);

        public event Action<RoutedEventArgs<ProxyElement<Sub>>> UpdateProxyOverride
        {
            add { UpdateProxyOverrideEvent.AddHandler(this, value); }
            remove { UpdateProxyOverrideEvent.RemoveHandler(this, value); }
        }

        private ICollection<ProxyOverride> _InternalProxyOverrides = new List<ProxyOverride>();


        public ProxyRoot() : base("Root")
        {
            ProxyOverrides = new List<ProxyOverride>();

            ProxyDataContext.Set(this, (Sub)this);
            ProxyOverridesProperty.AddChangedCallback(this, args => {
                var newides = args.NewValue.Except(args.OldValue);
                newides.Each(ov => UpdateProxyOverrides(ov.Name));
            });

        }


        public ICollection<ProxyElement<Sub>> FindProxies(string name)
        {
            return this
                .Descendants()
                .OfType<ProxyElement<Sub>>()
                .Where(proxy => proxy.ProxyName == name)
                .ToList();
        }

        private void UpdateProxyOverrides(string name)
        {
            UpdateProxyOverrideEvent.Raise(this, new ProxyUpdateEventArgs(name));
        }

        protected void OverrideProxy(ProxyOverride proxyOverride)
        {
            _InternalProxyOverrides.Add(proxyOverride);
            UpdateProxyOverrides(proxyOverride.Name);
        }

        internal ProxyOverride ResolveOverride(string name) 
        {
            return ProxyOverrides?.LastOrDefault(ov => ov.Name == name) ?? 
                _InternalProxyOverrides.LastOrDefault(ov => ov.Name == name);
        }

    }
}

