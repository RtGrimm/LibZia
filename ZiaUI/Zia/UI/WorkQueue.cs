using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Input;
using Zia.UI.Animation;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Zia.UI
{
    internal class WorkItem
    {
        public Action Callback
        {
            get;
            set;
        }
    }


    internal class WorkQueue
    {
        private ConcurrentStack<WorkItem> _Queue = new ConcurrentStack<WorkItem>();

        public void Execute()
        {
            _Queue.ToArray().Each(item => {
                item.Callback();
            });

            _Queue.Clear();
        }

        public void Queue(Action callback)
        {
            _Queue.Push(new WorkItem {
                Callback = callback
            });
        }

        public void Queue(WorkItem item)
        {
            _Queue.Push(item);
        }
    }
	
}
