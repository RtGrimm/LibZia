using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Input;
using Zia.UI.Animation;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Zia.UI
{


    class SceneWorkQueue : WorkQueue
    {

    }

	public class Scene
	{
		private Context _Context = new Context();
		private ITarget	_Target;

		private Element _Root;

        private PointerInputManager _PointerInputManager =
            new PointerInputManager();

        private KeyInputManager _KeyInputManager =
            new KeyInputManager();

        private RenderManager _RenderManager = new RenderManager();

        private bool _LayoutInvalid;

        private int _Width;
        private int _Height;

		public Scene (ITarget target)
		{
			_Target = target;
		}

		public void SetRoot(Element root)
		{
            _PointerInputManager.SetRoot(root);
            _KeyInputManager.SetRoot(root);

			_Root = root;
            _LayoutInvalid = true;

            _Root.LayoutInvalidate += 
                (args) => _LayoutInvalid = true;

            _Root.ActivateCore();
		}

        public void SetPointerInputSource(IPointerInputSource source)
        {
            _PointerInputManager.SetSource(source);
        }

        public void SetKeyInputSource(IKeyInputSource source)
        {
            _KeyInputManager.SetSource(source);
        }

		public void SizeChanged(int width, int height)
		{
            _Width = width;
            _Height = height;

            if(_Root != null)
                Element.LayoutInvalidateEvent.Raise(_Root);
		}

		public void Shutdown()
		{
			_Root.DeactivateCore ();
		}

        private void UpdateClock()
        {
            Singleton<ClockManager>.Instance.Update();
        }

        private void Update(Element target)
        {
            target.UpdateOverride();
            target.GetChildren().Each(child => Update(child));
        }

        void UpdateLayout()
        {
            if (!_LayoutInvalid)
                return;


            _Root.Measure(new Vec2(_Width, _Height));
            _Root.Arrange(new Rect(0, 0, _Width, _Height));

            _LayoutInvalid = false;
        }

        public void Update()
        {
            if (_Root == null)
            {
                _Context.PushTarget (_Target);
                _Context.Clear (new Color(1, 1, 1, 1));
                _Context.PopTarget();
                _Context.Flush();
                return;
            }

            Singleton<SceneWorkQueue>.Instance.Queue(new WorkItem
            {
                Callback = () => UpdateLayout()
            });

            Singleton<SceneWorkQueue>.Instance.Queue(new WorkItem
            {
                Callback = () => {
                    UpdateClock();
                    Update(_Root);
                }
            });

            Singleton<SceneWorkQueue>.Instance.Queue(new WorkItem
                                                     {
                Callback = () => Render()
            });

            Singleton<SceneWorkQueue>.Instance.Execute();
        }

		private void Render()
		{


			_Context.PushTarget (_Target);
            _Context.PushClip(new Rect(0, 0, _Target.Width(), _Target.Height()));

            _Context.Clear (new Color(1, 1, 1, 1));

			_RenderManager.Render (_Root, _Context, 
			             _Context.CreateTransform ().Ortho (0, _Target.Width (), _Target.Height (), 0, -1, 1), 
			             _Context.CreateTransform (), 1);

            _Context.PopClip();
			_Context.Flush ();
		}
	}
}

