using System;
using System.Collections.Generic;
using Zia.Common;
using System.Linq;
namespace Zia.UI
{


    public class SetterTriggerAction : DependencyBase, ITriggerAction
    {
        
       

        
        public static DependencyProperty SettersProperty = DependencyProperty
            .Register<ICollection<Setter>, SetterTriggerAction>("Setters", null);

        public ICollection<Setter> Setters
        {
            get
            {
                return GetValue<ICollection<Setter>>(SettersProperty);
            }
            set
            {
                SetValue(SettersProperty, value);
            }
        }

        
        public static DependencyProperty TargetProperty = DependencyProperty
            .Register<DependencyBase, SetterTriggerAction>("Target", null);

        public DependencyBase Target
        {
            get
            {
                return GetValue<DependencyBase>(TargetProperty);
            }
            set
            {
                SetValue(TargetProperty, value);
            }
        }

        public SetterTriggerAction(DependencyBase target)
        {
            Target = target;
            Setters = new List<Setter>();
        }

        public SetterTriggerAction()
        {

            Setters = new List<Setter>();
        }



        public void Execute(AdvancedElement element)
        {
            Setters.Each(setter => (Target == null ? element : Target)
                         .SetValue(setter.Target, setter.Value));
        }
    }
}

