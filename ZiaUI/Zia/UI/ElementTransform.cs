using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;


namespace Zia.UI
{
	public abstract class ElementTransform : PropertyBase
	{
        internal static Tag TransformParamTag = new Tag(() => TransformParamTag);

		public abstract Transform GetTransform(Context context);
	}

}

