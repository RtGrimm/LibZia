using System;

namespace Zia.UI
{
    public interface IUIEffect
    {
        Zia.Gfx.Effect.IShaderEffect ShaderEffect { get; }
        void SetInput(Zia.Gfx.ISurface surface);
    }
}

