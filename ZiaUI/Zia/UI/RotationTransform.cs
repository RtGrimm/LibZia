using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;


namespace Zia.UI
{

	public class RotationTransform : ElementTransform
	{
        public static readonly Property<Vec3, RotationTransform> AngleProperty = 
                    Property<Vec3, RotationTransform>
                        .New()
                        .Tags(TransformParamTag, Element.RenderInvalidationTag)
                        .Name(() => AngleProperty);

        public Vec3 Angle
        {
            get
            {
                return AngleProperty.GetValue(this);
            }
            set
            {
                AngleProperty.SetValue(this, value);
            }
        }

		public override Transform GetTransform (Context context)
		{
			var angle = Angle;
			return context.CreateTransform ()
				.RotateX (angle.X)
				.RotateY (angle.Y)
				.RotateZ (angle.Z);
		}
	}
}
