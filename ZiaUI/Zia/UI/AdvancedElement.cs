using System;
using System.Linq;
using System.Collections.Generic;
using Zia.UI.Input;
using Zia.Common;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Zia.UI.Data;

namespace Zia.UI
{
  
    public class AdvancedElement : Element
    {
        public static RoutedEvent<PointerEventArgs, Element> PointerDownEvent = 
                    new RoutedEvent<PointerEventArgs, Element>(() => PointerDownEvent, RoutingStrategy.Bubble);

        public event Action<PointerEventArgs> PointerDown
        {
            add { PointerDownEvent.AddHandler(this, value); }
            remove { PointerDownEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<PointerEventArgs, Element> PointerUpEvent = 
                    new RoutedEvent<PointerEventArgs, Element>(() => PointerUpEvent, RoutingStrategy.Bubble);

        public event Action<PointerEventArgs> PointerUp
        {
            add { PointerUpEvent.AddHandler(this, value); }
            remove { PointerUpEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<PointerEventArgs, Element> PointerMoveEvent = 
                    new RoutedEvent<PointerEventArgs, Element>(() => PointerMoveEvent, RoutingStrategy.Bubble);

        public event Action<PointerEventArgs> PointerMove
        {
            add { PointerMoveEvent.AddHandler(this, value); }
            remove { PointerMoveEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<PointerEventArgs, Element> PointerEnterEvent = 
                    new RoutedEvent<PointerEventArgs, Element>(() => PointerEnterEvent, RoutingStrategy.Direct);

        public event Action<PointerEventArgs> PointerEnter
        {
            add { PointerEnterEvent.AddHandler(this, value); }
            remove { PointerEnterEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<PointerEventArgs, Element> PointerLeaveEvent = 
                    new RoutedEvent<PointerEventArgs, Element>(() => PointerLeaveEvent, RoutingStrategy.Direct);

        public event Action<PointerEventArgs> PointerLeave
        {
            add { PointerLeaveEvent.AddHandler(this, value); }
            remove { PointerLeaveEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<KeyPressEventArgs, Element> KeyPressEvent = 
                    new RoutedEvent<KeyPressEventArgs, Element>(() => KeyPressEvent, RoutingStrategy.Bubble);

        public event Action<KeyPressEventArgs> KeyPress
        {
            add { KeyPressEvent.AddHandler(this, value); }
            remove { KeyPressEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<KeyEventArgs, Element> KeyDownEvent = 
            new RoutedEvent<KeyEventArgs, Element>(() => KeyDownEvent, RoutingStrategy.Bubble);

        public event Action<KeyEventArgs> KeyDown
        {
            add { KeyDownEvent.AddHandler(this, value); }
            remove { KeyDownEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<KeyEventArgs, Element> KeyUpEvent = 
            new RoutedEvent<KeyEventArgs, Element>(() => KeyUpEvent, RoutingStrategy.Bubble);

        public event Action<KeyEventArgs> KeyUp
        {
            add { KeyUpEvent.AddHandler(this, value); }
            remove { KeyUpEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<RoutedEventArgs<Element>, Element> RequestFocusEvent = 
            new RoutedEvent<RoutedEventArgs<Element>, Element>(() => RequestFocusEvent, RoutingStrategy.Bubble);

        public event Action<RoutedEventArgs<Element>> RequestFocus
        {
            add { RequestFocusEvent.AddHandler(this, value); }
            remove { RequestFocusEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<RoutedEventArgs<AdvancedElement>, AdvancedElement> ReceivedFocusEvent = 
            new RoutedEvent<RoutedEventArgs<AdvancedElement>, AdvancedElement>(() => ReceivedFocusEvent, RoutingStrategy.Direct);

        public event Action<RoutedEventArgs<AdvancedElement>> ReceivedFocus
        {
            add { ReceivedFocusEvent.AddHandler(this, value); }
            remove { ReceivedFocusEvent.RemoveHandler(this, value); }
        }

        public static RoutedEvent<RoutedEventArgs<AdvancedElement>, AdvancedElement> LostFocusEvent = 
            new RoutedEvent<RoutedEventArgs<AdvancedElement>, AdvancedElement>(() => LostFocusEvent, RoutingStrategy.Direct);

        public event Action<RoutedEventArgs<AdvancedElement>> LostFocus
        {
            add { LostFocusEvent.AddHandler(this, value); }
            remove { LostFocusEvent.RemoveHandler(this, value); }
        }


        public static readonly Property<ICollection<ITrigger>, AdvancedElement> TriggersProperty = 
                    Property<ICollection<ITrigger>, AdvancedElement>
                        .New()
                        .CoerceToObservableCollection()
                        .Name(() => TriggersProperty);

      

        public ICollection<ITrigger> Triggers
        {
            get
            {
                return TriggersProperty.GetValue(this);
            }
            set
            {
                TriggersProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, AdvancedElement> IsPointerDownProperty = 
            Property<bool, AdvancedElement>
                        .New()
                        .Name(() => IsPointerDownProperty);

        public bool IsPointerDown
        {
            get
            {
                return IsPointerDownProperty.GetValue(this);
            }
            set
            {
                IsPointerDownProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, AdvancedElement> IsPointerOverProperty = 
            Property<bool, AdvancedElement>
                        .New()
                        .Name(() => IsPointerOverProperty);
                        
        public bool IsPointerOver
        {
            get 
            {
                return IsPointerOverProperty.GetValue(this);
            }
            set
            {
                IsPointerOverProperty.SetValue(this, value);
            }
        }
        

        public static readonly Property<IStyle, AdvancedElement> StyleProperty = 
                    Property<IStyle, AdvancedElement>
                        .New()
                        .Name(() => StyleProperty);

        public IStyle Style
        {
            get
            {
                return StyleProperty.GetValue(this);
            }
            set
            {
                StyleProperty.SetValue(this, value);
            }
        }



        internal bool HasFocus
        {
            get;
            set;
        }

        public AdvancedElement()
        {

            SubscribePointerEvents();
            SubscribeTriggerEvents();

            StyleProperty.AddChangedCallback(this, UpdateStyle);


           
        }
    

        void SubscribePointerEvents()
        {
            PointerEnter += (args) => IsPointerOver = true;
            PointerLeave += (args) => IsPointerOver = false;
            PointerDown += (args) => IsPointerDown = true;
            PointerUp += (args) => IsPointerDown = false;
        }

     

        private void SubscribeTriggerEvents()
        {
            NotifyCollectionChangedEventHandler 
            triggersChangedCallback = (sender, e) => {
                OnTriggerCollectionChanged(e.OldItems.OfType<ITrigger>(), 
                                           e.NewItems.OfType<ITrigger>());
            };

            TriggersProperty.AddChangedCallback(this, args => {
                OnTriggerCollectionChanged(
                    args.OldValue, 
                    args.NewValue);

                if(args.OldValue != null)
                    (args.OldValue as ObservableCollection<ITrigger>)
                        .CollectionChanged += triggersChangedCallback;

                (args.NewValue as ObservableCollection<ITrigger>)
                    .CollectionChanged += triggersChangedCallback;
            });

        }

        private void OnTriggerEnter(ITrigger trigger)
        {
            trigger.EnterActions.Each(action => action.Execute(this));
        }

        private void OnTriggerExit(ITrigger trigger)
        {
            trigger.ExitActions.Each(action => action.Execute(this));
        }

        private void OnTriggerCollectionChanged(IEnumerable<ITrigger> oldTriggers, IEnumerable<ITrigger> newTriggers)
        {
            if(oldTriggers != null)
            oldTriggers.Each(trigger => {
                trigger.OnDetach(this);

                trigger.Enter -= OnTriggerEnter;
                trigger.Exit -= OnTriggerExit;
            });

            if(newTriggers != null)
            newTriggers.Each(trigger => {
                trigger.OnAttach(this); 

                trigger.Enter += OnTriggerEnter;
                trigger.Exit += OnTriggerExit;
            });
        }

        void UpdateStyle(PropertyChangedArgs<IStyle, AdvancedElement> args)
        {
            Style.Apply(this);
        }
            
    }
}

