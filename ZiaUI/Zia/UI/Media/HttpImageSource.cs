using System;
using System.Threading.Tasks;
using Zia.Gfx;
using Zia.Common;
using System.IO;
using System.Net.Http;
using Zia.UI.Data;

namespace Zia.UI.Media
{

	public class HttpImageSource : StreamImageSource
	{


        public static readonly Property<string, HttpImageSource> UrlProperty = 
                    Property<string, HttpImageSource>
                        .New()
                        .Tags(Image.ImageInvalidationTag)
                        .Name(() => UrlProperty);

        public string Url
        {
            get
            {
                return UrlProperty.GetValue(this);
            }
            set
            {
                UrlProperty.SetValue(this, value);
            }
        }

		public HttpImageSource (string url)
		{
			Url = url;
		}

		public HttpImageSource ()
		{

		}

		protected override async Task<Stream> GetStream ()
		{
			var client = new HttpClient ();
			var message = await client.GetAsync (Url);
			return await message.Content.ReadAsStreamAsync ();
		}
	}
	
}
