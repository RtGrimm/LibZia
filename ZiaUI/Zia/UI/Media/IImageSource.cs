using System;
using System.Threading.Tasks;
using Zia.Gfx;

namespace Zia.UI.Media
{
	public interface IImageSource
	{
		Task<Func<Context, ISurface>> GetSurface();
	}
}

