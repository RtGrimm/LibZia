using System;
using System.Threading.Tasks;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;

namespace Zia.UI.Media
{




	public class Image : AdvancedElement
	{
        public static readonly Property<IImageSource, Image> ImageSourceProperty = 
                    Property<IImageSource, Image>
                        .New()
                        .Tags(RenderInvalidationTag, Element.LayoutInvalidationTag)
                        .Name(() => ImageSourceProperty);

        public IImageSource ImageSource
        {
            get
            {
                return ImageSourceProperty.GetValue(this);
            }
            set
            {
                ImageSourceProperty.SetValue(this, value);
            }
        }


        public static Tag ImageInvalidationTag = new Tag (() => ImageInvalidationTag);

		private bool _ImageInvalid = true;
		private ISurface _Image;
		private Task<Func<Context, ISurface>> _ImageLoadTask;

		public Image ()
		{
            ImageSourceProperty.AddChangedCallback 
                (this, (args) => _ImageInvalid = true);

            AddListener(Listener.New()
                        .WithTags(ImageInvalidationTag)
                        .WithCallback(() => _ImageInvalid = true));
		}

		private void UpdateImage(Context context) 
		{
			if (_ImageInvalid) {
				_ImageLoadTask = ImageSource.GetSurface ();
				_ImageLoadTask.ContinueWith ((callback) => 
                                             Element.RenderInvalidateEvent.Raise(this));
				_Image = null;
				_ImageInvalid = false;
			}
		}

        protected override void RenderOverride (Context context, Transform projection, Transform transform, float opacity)
		{
			UpdateImage (context);

			if (_ImageLoadTask != null && _ImageLoadTask.IsCompleted) {
				if (_Image == null) {
					_Image = _ImageLoadTask.Result(context);
				}

                context.DrawSurface (_Image, transform * projection, RenderSize.X, RenderSize.Y, opacity);
			}
		}
	}
}

