using System;
using System.Threading.Tasks;
using Zia.Gfx;
using Zia.Common;
using System.IO;
using Zia.UI.Data;

namespace Zia.UI.Media
{
    public abstract class StreamImageSource : PropertyBase, IImageSource
	{
		protected abstract Task<Stream> GetStream ();

		public async Task<Func<Zia.Gfx.Context, Zia.Gfx.ISurface>> GetSurface ()
		{
			byte[] buffer;
			int width = 0, height = 0;

			using (var byteStream = new MemoryStream ()) {
				using (var imageStream = await GetStream ()) {
					await imageStream.CopyToAsync (byteStream);
				}

				var bitmap = new System.Drawing.Bitmap (byteStream);

				width = bitmap.Width;
				height = bitmap.Height;

				var bitmapData = bitmap.LockBits (new System.Drawing.Rectangle (0, 0, bitmap.Width, bitmap.Height), 
				                                  System.Drawing.Imaging.ImageLockMode.ReadOnly, 
				                                  System.Drawing.Imaging.PixelFormat.Format32bppArgb);

				buffer = new byte[bitmapData.Stride * bitmapData.Height];

				System.Runtime.InteropServices.Marshal.Copy (bitmapData.Scan0, buffer, 0, buffer.Length);
				bitmap.UnlockBits (bitmapData);
			}

			return (context) => context.CreateSurface (width, height, buffer, Zia.Gfx.ColorFormat.RGBA);
		}
	}
	
}
