using System;
using System.Threading.Tasks;
using Zia.Gfx;
using Zia.Common;
using System.IO;
using Zia.UI.Data;

namespace Zia.UI.Media
{

	public class RawImageSource : StreamImageSource
	{
        public static readonly Property<byte[], RawImageSource> DataProperty = 
                    Property<byte[], RawImageSource>
                        .New()
                        .Tags(Image.ImageInvalidationTag)
                        .Name(() => DataProperty);

        public byte[] Data
        {
            get
            {
                return DataProperty.GetValue(this);
            }
            set
            {
                DataProperty.SetValue(this, value);
            }
        }

		protected override Task<Stream> GetStream ()
		{
			return Task.Run<Stream> (() => new MemoryStream (Data));
		}
	}
	
}
