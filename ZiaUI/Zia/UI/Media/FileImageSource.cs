using System;
using System.Threading.Tasks;
using Zia.Gfx;
using Zia.Common;
using System.IO;
using Zia.UI.Data;

namespace Zia.UI.Media
{

	public class FileImageSource : StreamImageSource
	{
        public static readonly Property<string, FileImageSource> PathProperty = 
                    Property<string, FileImageSource>
                        .New()
                        .Tags(Image.ImageInvalidationTag)
                        .Name(() => PathProperty);

        public string Path
        {
            get
            {
                return PathProperty.GetValue(this);
            }
            set
            {
                PathProperty.SetValue(this, value);
            }
        }

		public FileImageSource (string path)
		{
			Path = path;
		}

		protected override  Task<Stream> GetStream ()
		{
			return Task.Run<Stream> (() => new FileStream (Path, FileMode.Open));
		}
	}
	
}
