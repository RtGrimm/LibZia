using System;
using Zia.Gfx;
using Zia.Common;
using Zia.UI.Data;


namespace Zia.UI
{

	public class TranslateTransform : ElementTransform
	{
        public static readonly Property<Vec3, TranslateTransform> OffsetProperty = 
                    Property<Vec3, TranslateTransform>
                        .New()
                        .Tags(TransformParamTag, Element.RenderInvalidationTag)
                        .Name(() => OffsetProperty);

        public Vec3 Offset
        {
            get
            {
                return OffsetProperty.GetValue(this);
            }
            set
            {
                OffsetProperty.SetValue(this, value);
            }
        }

		public override Transform GetTransform (Context context)
		{
            return context.CreateTransform ().Translate (Offset);
		}
	}


}
