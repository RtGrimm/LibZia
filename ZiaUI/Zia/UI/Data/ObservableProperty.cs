using System;

namespace Zia.UI.Data
{
    public class ObservableProperty<T> : IBindingSource<T>
    {
        public event Action Changed = () => {};

        T _Value;
        public T Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
                Changed();
            }
        }

        public ObservableProperty()
        {
                


        }

        public ObservableProperty(ObservableProperty<T> other)
        {
            _Value = other.Value;
        }

        public static implicit operator ObservableProperty<T>(T value)
        {
            return new ObservableProperty<T>(value);
        }

        public static implicit operator T(ObservableProperty<T> target)
        {
            return target.Value;
        }

        public ObservableProperty(T value)
        {
            _Value = value;
        }
        
        public T GetValue()
        {
            return Value;
        }
    }
}

