﻿using System;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.UI.Data
{
    public class DataContext<T, TOwner> where TOwner : Element where T : class
    {


        private Dictionary<TOwner, Option<T>> _DataMap = 
            new Dictionary<TOwner, Option<T>>();

        private Dictionary<TOwner, List<Action>> _TrackerMap = 
            new Dictionary<TOwner, List<Action>>();

        private static RoutedEvent<RoutedEventArgs<Element>, Element> UpdateDataContextEvent = 
            new RoutedEvent<RoutedEventArgs<Element>, Element>(() => UpdateDataContextEvent, RoutingStrategy.Tunnel);


        private void CallHandlers(TOwner owner)
        {
            _TrackerMap[owner].Each(trackerItem => trackerItem());
        }

        public void Track(TOwner owner, Action tracker)
        {
            if (!_TrackerMap.ContainsKey(owner))
            {
                _TrackerMap[owner] = new List<Action>();

                owner.Orphaned += () => CallHandlers(owner);
                owner.Adopted += args => CallHandlers(owner);
                owner.TreeChanged += (args) => CallHandlers(owner);
                owner.Activate += (args) => CallHandlers(owner);

                UpdateDataContextEvent.AddHandler(owner, args => CallHandlers(owner));
            }

            _TrackerMap[owner].Add(tracker);
        }
        

        private T ResolveValue(Element target)
        {
            if (target == null)
                return null;

            var owner = target as TOwner;

            if (owner == null || !_DataMap.ContainsKey(owner))
                return ResolveValue(target.Parent);

            return Option<T>.GetValue(_DataMap[owner]);
        }

        public void Set(TOwner owner, T data)
        {
            _DataMap[owner] = new Option<T>.Some(data);
            UpdateDataContextEvent.Raise(owner);
        }


        public T Get(TOwner owner)
        {
            if (!_DataMap.ContainsKey(owner))
            {
                return ResolveValue(owner.Parent);
            }

            return Option<T>.GetValue(_DataMap[owner]);
        }
    }
}

