using System;

namespace Zia.UI.Data
{
    public interface IBindingTarget<T>
    {
        void SetValue(T value);

        void Clear();

        void Set();
    }
    
}
