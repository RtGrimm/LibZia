using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using Zia.Common;
using System.Linq;

namespace Zia.UI.Data
{
    static class ExpressionEx
    {
        class Visitor : ExpressionVisitor
        {
            private ICollection<Expression> _Output;

            public Visitor(ICollection<Expression> output)
            {
                _Output = output;
            }

            public override Expression Visit (Expression node)
            {
                _Output.Add(node);
                return base.Visit(node);
            }
        }

        public static IEnumerable<Expression> AllNodes(this Expression target)
        {
            var output = new List<Expression>();
            var visitor = new Visitor(output);

            visitor.Visit(target);

            return output;
        }
    }

    public class ExpressionTracker
    {
        interface IPropertyWrapper
        {
            void Subscribe(Action subscriber);
            void Unsubscribe(Action subscriber);
        }

        class BindingSourceWrapper<T> : IPropertyWrapper
        {
            IBindingSource<T> _Source;

            public BindingSourceWrapper(IBindingSource<T> source)
            {
                _Source = source;
            }

            public void Subscribe (Action subscriber)
            {
                _Source.Changed += subscriber;
            }

            public void Unsubscribe (Action subscriber)
            {
                _Source.Changed -= subscriber;
            }
        }

        class PropertyWrapper<T, TOwner> : IPropertyWrapper where TOwner :PropertyBase
        {
            TOwner _Owner;
            Property<T, TOwner> _Property;

            private Dictionary<Action, Action<PropertyChangedArgs<T, TOwner>>> _SubscriberMap = 
                new Dictionary<Action, Action<PropertyChangedArgs<T, TOwner>>>();

            public PropertyWrapper(TOwner owner, Property<T, TOwner> property)
            {
                this._Property = property;
                this._Owner = owner;
            }

            public void Subscribe (Action subscriber)
            {
                if (!_SubscriberMap.ContainsKey(subscriber))
                {
                    _SubscriberMap[subscriber] = args => subscriber();
                }

                _Property.AddChangedCallback(_Owner, _SubscriberMap[subscriber]);
            }

            public void Unsubscribe (Action subscriber)
            {
                if (!_SubscriberMap.ContainsKey(subscriber))
                {
                    return;
                }

                _Property.RemoveChangedCallback(_Owner, _SubscriberMap[subscriber]);
            }
        }

        class PropertySubscriber<U, TOwner> where TOwner : PropertyBase
        {
            public static void Subscribe(ExpressionTracker tracker,
                TOwner owner, Property<U, TOwner> property)
            {
                tracker._PropertyWrappers.Add(new PropertyWrapper<U, TOwner>(owner, property));
            }
        }

        class BindingSourceSubscriber<T>
        {
            public static void Subscribe(ExpressionTracker tracker, IBindingSource<T> source)
            {
                tracker._PropertyWrappers.Add(new BindingSourceWrapper<T>(source));
            }
        }

        class Visitor : ExpressionVisitor
        {
            ExpressionTracker _Parent;

            public Visitor(ExpressionTracker parent)
            {
                this._Parent = parent;
            }

            void SubscribeToBindingSource(MemberExpression node)
            {
                var args = node.AllNodes().OfType<ParameterExpression>().ToList();

                var property = Expression.Lambda(node, args)
                    .Compile().DynamicInvoke(_Parent._ArgValues);

                var type = node.Type.GetInterfaces()
                    .Single(@interface => @interface.IsGenericType && 
                        @interface.GetGenericTypeDefinition() == typeof(IBindingSource<>))
                    .GetGenericArguments()[0];

                typeof(BindingSourceSubscriber<>)
                    .MakeGenericType(type)
                    .GetMethod("Subscribe")
                    .Invoke(null, new object[] {_Parent, property});
            }
    
            void SubscribeToProperty(MemberExpression node)
            {
                var args = node.AllNodes().OfType<ParameterExpression>().ToList();

                var owner = Expression.Lambda(node.Expression, args)
                    .Compile().DynamicInvoke(_Parent._ArgValues);

                var propertyDef = node.Expression.Type
                    .GetField(node.Member.Name + "Property",
                        System.Reflection.BindingFlags.Static | 
                        System.Reflection.BindingFlags.Public | 
                        System.Reflection.BindingFlags.FlattenHierarchy);

                if (propertyDef == null)
                    return;

                var property = propertyDef
                    .GetValue(owner);

                var type = typeof(PropertySubscriber<,>).GetGenericTypeDefinition();

                type
                    .MakeGenericType(node.Type, propertyDef.DeclaringType)
                    .GetMethod("Subscribe")
                    .Invoke(null, new object[] {_Parent, owner, property});
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                if (node.Type.GetInterfaces().Any(@interface => 
                    @interface.IsGenericType && 
                    @interface.GetGenericTypeDefinition() == typeof(IBindingSource<>)))
                {
                    SubscribeToBindingSource(node);
                } else if (typeof(PropertyBase).IsAssignableFrom(node.Expression.Type))
                {
                        SubscribeToProperty(node);
                }

                Visit(node.Expression);

                return node;
            }
        }

        private ICollection<IPropertyWrapper> _PropertyWrappers = 
            new List<IPropertyWrapper>();

        private object[] _ArgValues;

        private Visitor _Visitor;

        public ExpressionTracker()
        {
            _Visitor = new Visitor(this);
        }

        public void Subscribe(Action callback)
        {
            _PropertyWrappers.Each(wrapper => wrapper.Subscribe(callback));
        }

        public void Unsubscribe(Action callback)
        {
            _PropertyWrappers.Each(wrapper => wrapper.Unsubscribe(callback));
        }

        public void Parse(Expression expression, object[] argsValues)
        {
            _ArgValues = argsValues;
            _PropertyWrappers.Clear();

            _Visitor.Visit(expression);
        }

        public void Parse(Expression expression)
        {
            _PropertyWrappers.Clear();
            _Visitor.Visit(expression);
        }
    }

    public class ExpressionBinding<T> : IBindingSource<T>
    {
        private Func<T> _ValueFunction;
        private ExpressionTracker _Tracker = new ExpressionTracker();

        public ExpressionBinding (Expression<Func<T>> expression)
        {
            _ValueFunction = expression.Compile();
            _Tracker.Parse(expression);
            _Tracker.Subscribe(ValueChanged);
        }

        public event Action Changed;

        public T GetValue()
        {
            return _ValueFunction();
        }

        private void ValueChanged()
        {
            Changed();
        }
            
    }
}

