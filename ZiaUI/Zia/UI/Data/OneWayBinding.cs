using System;

namespace Zia.UI.Data
{
    public class OneWayBinding<TSource, TTarget>
    {
        private IBindingSource<TSource> _Source;
        private IBindingTarget<TTarget> _Target;
        private IBindingConverter<TSource, TTarget> _Converter;
        private Action _ChangedCallback;



        public OneWayBinding(IBindingSource<TSource> source, 
                             IBindingTarget<TTarget> target, 
                             IBindingConverter<TSource, TTarget> converter)
        {
            this._Converter = converter;

            _Source = source;
            _Target = target;

            _ChangedCallback = () => 
            {
                UpdateValue();
            };
        }

        public static OneWayBinding<TSource, TTarget> Create(IBindingSource<TSource> source,
                                                             IBindingTarget<TTarget> target, 
                                                             IBindingConverter<TSource, TTarget> converter)
        {
            return new OneWayBinding<TSource, TTarget> (source, target, converter);
        }

        public OneWayBinding<TSource, TTarget>  Clear()
        {
            _Source.Changed -= _ChangedCallback;
            _Target.Clear();

            return this;
        }

        void UpdateValue()
        {
            _Target.SetValue(_Converter.Convert(_Source.GetValue()));
        }

        public OneWayBinding<TSource, TTarget>  Set()
        {
            _Source.Changed += _ChangedCallback;
            _Target.Set();

            UpdateValue();

            return this;
        }
    }

    public class OneWayBinding<T> : OneWayBinding<T, T>
    {
        public OneWayBinding(IBindingSource<T> source, 
                             IBindingTarget<T> target) : base(source, target, 
                                         new CallbackBindingConverter<T, T>(value => value))
        {
        }
    }
}

