using System;

namespace Zia.UI.Data
{

    public interface IBindingSourceTag
    {
    }

    public interface IBindingSource<T> : IBindingSourceTag
    {
        event Action Changed;

        T GetValue();
    }
    
}
