﻿using System;
using System.Linq.Expressions;

namespace Zia.UI.Data
{
    public class DataContextSource<TDataContextValue, TBindingType, TOwner> : IBindingSource<TBindingType> 
        where TOwner : Element
        where TDataContextValue : class
    {
        TOwner _Owner;
        DataContext<TDataContextValue, TOwner> _DataContext;
        Expression<Func<TDataContextValue, TBindingType>> _SourceGetter;
        Func<TDataContextValue, TBindingType> _SourceGetterFunc;

        private ExpressionTracker _Tracker = new ExpressionTracker();

        public event Action Changed = () => {};

        private TDataContextValue _CurrentDataContextValue;

        public DataContextSource(
            TOwner owner,
            DataContext<TDataContextValue, TOwner> dataContext, 
            Expression<Func<TDataContextValue, TBindingType>> sourceGetter)
        {
            _Owner = owner;
            _DataContext = dataContext;
            _SourceGetter = sourceGetter;
            _SourceGetterFunc = sourceGetter.Compile();

            _DataContext.Track(owner, () => {
                var newDataContext = _DataContext.Get(_Owner);

                if(newDataContext != _CurrentDataContextValue)
                {
                    Console.WriteLine("Data context changed");

                    if(_CurrentDataContextValue != null)
                    {
                        _Tracker.Unsubscribe(ValueChanged);
                    }

                    if(newDataContext != null)
                    {
                        _Tracker.Parse(_SourceGetter, new [] {newDataContext});
                        _Tracker.Subscribe(ValueChanged);
                    }

                    _CurrentDataContextValue = newDataContext;
                    Changed();
                }
            });
        }

        void ValueChanged() => Changed();

        public TBindingType GetValue()
        {
            return _CurrentDataContextValue != null ? 
                _SourceGetterFunc(_CurrentDataContextValue) : default(TBindingType);
        }

    }
}

