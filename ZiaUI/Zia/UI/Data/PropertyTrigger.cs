using System;
using System.Collections.Generic;
using Zia.UI;

namespace Zia.UI.Data
{
    


    public class PropertyTrigger
    {
        public static PropertyTrigger<T, TOwner> New<T, TOwner>(Property<T, TOwner> property, T value)  where TOwner : PropertyBase
        {
            return new PropertyTrigger<T, TOwner>(property, value);
        }
    }

    public class PropertyTrigger<T, TOwner> : ITrigger where TOwner : PropertyBase
    {
        public ICollection<ITriggerAction> EnterActions
        {
            get;
            set;
        }

        public ICollection<ITriggerAction> ExitActions
        {
            get;
            set;
        }

        public event Action<ITrigger> Enter;
        public event Action<ITrigger> Exit;

        private Property<T, TOwner> _Property;
        private T _Value;

        public PropertyTrigger(Property<T, TOwner> property, T value)
        {
            this._Value = value;
            this._Property = property;

            EnterActions = new List<ITriggerAction>();
            ExitActions = new List<ITriggerAction>();
        }

        private void PropertyChanged(PropertyChangedArgs<T, TOwner> args)
        {
            if (EqualityComparer<T>.Default.Equals(args.OldValue, _Value) && 
                !EqualityComparer<T>.Default.Equals(args.NewValue, _Value))
            {
                Exit(this);


            }
            else if (!EqualityComparer<T>.Default.Equals(args.OldValue, _Value) && 
                     EqualityComparer<T>.Default.Equals(args.NewValue, _Value))
            {
                Enter(this);

            }
        }

        public void OnAttach(AdvancedElement target)
        {
            _Property.AddChangedCallback(target as TOwner, PropertyChanged);
        }

        public void OnDetach(AdvancedElement target)
        {
            _Property.RemoveChangedCallback(target as TOwner, PropertyChanged);
        }
    }
}

