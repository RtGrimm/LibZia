using System;


namespace Zia.UI.Data
{

   public class PropertyBinding<T, TOwner> : ITwoWayBindable<T> where TOwner : PropertyBase
    {
        public event Action Changed = () => {};

        TOwner _Target;
        Property<T, TOwner> _Property;

        public PropertyBinding(TOwner target, Property<T, TOwner> property)
        {
            this._Property = property;
            this._Target = target;

            _Property.AddChangedCallback(_Target, (args) => Changed());
        }

        public void SetValue(T value)
        {
            _Property.SetValue(_Target, value);
        }

        public T GetValue()
        {
            return _Property.GetValue(_Target);
        }

        public void Clear()
        {

        }

        public void Set()
        {

        }
    }
    
}
