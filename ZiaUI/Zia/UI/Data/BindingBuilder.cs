using System;
using System.Linq.Expressions;

namespace Zia.UI.Data
{
    public class BindingBuilder
    {
        public static BindingBuilder<T> Bind<T, TOwner>(TOwner owner, Property<T, TOwner> property) where TOwner : PropertyBase
        {
            return new BindingBuilder<T>(new PropertyBinding<T, TOwner>(owner, property));
        }

        public static BindingBuilder<T> Bind<T>(Expression<Func<T>> ex) 
        {
            return Bind(new ExpressionBinding<T>(ex));
        }

        public static BindingBuilder<T> Bind<T>(IBindingSource<T> source) 
        {
            return new BindingBuilder<T>(source);
        }

        public static BindingBuilder<TBindingType> Bind<TBindingType, TDataContextValue, TOwner>(
            DataContext<TDataContextValue, TOwner> dataContext,
            TOwner owner, 
            Expression<Func<TDataContextValue, TBindingType>> contextExpression) 
            where TOwner : Element 
            where TDataContextValue : class
        {
            return new BindingBuilder<TBindingType>(new DataContextSource<
                TDataContextValue, TBindingType, TOwner>(owner, dataContext, contextExpression));
        }
    }

    public static class BindingBuilderImpl 
    {
        public static OneWayBinding<T, T> Set<T>(this BindingBuilder<T, T> target)
        {
            var binding = new OneWayBinding<T>(target._Source, target._Target);
            binding.Set();

            return binding;
        }

        public static OneWayBinding<T, U> Set<T, U>(this BindingBuilder<T, U> target)
        {
            if (target._Converter == null && typeof(T) != typeof(U))
                throw new InvalidOperationException(
                    "Unable to create binding between heterogeneous types in the absence of a converter.");

            var binding = new OneWayBinding<T, U>(target._Source, target._Target, target._Converter);
            binding.Set();

            return binding;
        }
    }

    public class BindingBuilder<T>
    {
        internal IBindingSource<T> _Source;
        internal IBindingTarget<T> _Target;

        public BindingBuilder(IBindingSource<T> source)
        {
            this._Source = source;
        }

        public OneWayBinding<T> Set()
        {
            var binding = new OneWayBinding<T>(_Source, _Target);
            binding.Set();

            return binding;
        }

        public BindingBuilder<T, U> To<U>(IBindingTarget<U> target) 
        {
            return new BindingBuilder<T, U>(_Source, target);
        }

        public BindingBuilder<T, U> To<U, TOwner>(TOwner owner, Property<U, TOwner> property) where TOwner : PropertyBase
        {
            return To(new PropertyBinding<U, TOwner>(owner, property));
        }
    }

    public class BindingBuilder<T, U>
    {
        internal IBindingSource<T> _Source;
        internal IBindingTarget<U> _Target;
        internal IBindingConverter<T, U> _Converter;

        public BindingBuilder(IBindingSource<T> source, IBindingTarget<U> target)
        {
            this._Source = source;
            this._Target = target;
        }

        public BindingBuilder<T, U> Converter(IBindingConverter<T, U> converter)
        {
            _Converter = converter;
            return this;
        }

        public BindingBuilder<T, U> Converter(Func<T, U> converter)
        {
            _Converter = new CallbackBindingConverter<T, U>(converter);
            return this;
        }

        public OneWayBinding<T, U> Set()
        {
            if (_Converter == null && typeof(T) != typeof(U))
                throw new InvalidOperationException(
                    "Unable to create binding between heterogeneous types in the absence of a converter.");

            var binding = new OneWayBinding<T, U>(_Source, _Target, _Converter);
            binding.Set();

            return binding;
        }
    }
}

