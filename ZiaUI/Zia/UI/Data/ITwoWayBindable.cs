using System;

namespace Zia.UI.Data
{

    public interface ITwoWayBindable<T> : IBindingSource<T>, IBindingTarget<T>
    {

    }
    
}
