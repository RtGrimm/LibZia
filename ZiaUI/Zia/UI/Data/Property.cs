using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.ObjectModel;

namespace Zia.UI.Data
{

    public class PropertyChangedArgs<T, TOwner> : EventArgs where TOwner : PropertyBase
    {
        public T OldValue
        {
            get;
            set;
        }

        public T NewValue
        {
            get;
            set;
        }

        public TOwner Owner
        {
            get;
            set;
        }

        public Property<T, TOwner> Property
        {
            get;
            set;
        }

        public PropertyChangedArgs(T oldValue, T newValue, Property<T, TOwner> property, TOwner owner)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
            this.Owner = owner;
            this.Property = property;
        }
    }

    public abstract class PropertyMetadata
    {
        public string Name 
        {
            get; 
            set; 
        }

        public IEnumerable<Tag> Tags
        {
            get;
            set;
        }

        public bool ChangedFromValue
        {
            get;
            set;
        }

        public bool ChangedFromCollection
        {
            get;
            set;
        }

        public bool ChangedFromCollectionItems
        {
            get;
            set;
        }
    }

    public class PropertyMetadata<T, TOwner> : PropertyMetadata
    {
        public T DefaultValue
        {
            get;
            set;
        }

        public Func<TOwner, T, T> CoerceValueCallback
        {
            get;
            set;
        }

        public PropertyMetadata(T value, string name) :
            this(value, name, new Tag[] {}, false, true, false, (owner, x) => x)
        {
        }

        public PropertyMetadata(T value, string name, IEnumerable<Tag> tags, 
                                bool changedFromValue, bool changedFromCollection, bool changedFromCollectionItems,
                                Func<TOwner, T, T> coerceValueCallback)
        {
            Name = name;
            DefaultValue = value;
            Tags = tags;

            ChangedFromValue = changedFromValue;
            ChangedFromCollection = changedFromCollection;
            ChangedFromCollectionItems = changedFromCollectionItems;

            CoerceValueCallback = coerceValueCallback;
        }

        public PropertyMetadata()  :
            this(default(T), "", new Tag[] {}, false, true, false, (owner, value) => value)
        {
        }
    }

    abstract class Option<T>
    {
        public class Some : Option<T>
        {
            public T Value
            {
                get;
                private set;
            }

            public Some(T value)
            {
                Value = value;
            }
        }

        public class None : Option<T>
        {

        }

        public static T GetValue(Option<T> option)
        {
            if (option is None)
                return default(T);
            else
                return (option as Some).Value;
        }
    }

    public class Tag
    {
        public string Name
        {
            get;
            private set;
        }

        public Tag(string name)
        {
            Name = name;
        }

        public Tag(Expression<Func<Tag>> name)
        {
            Name = (name.Body as MemberExpression)
                .Member.Name.Replace("Tag", "");
        }

        public override string ToString()
        {
            return string.Format("[Tag: Name={0}]", Name);
        }
    }

    public static class PropertyBuilderEx
    {
        public static PropertyBuilder<ICollection<TItem>, TOwner> CoerceToObservableCollection
            <TOwner, TItem>(this PropertyBuilder<ICollection<TItem>, TOwner> target) 
                where TOwner : PropertyBase
        {
            target.PropertyMetadata.CoerceValueCallback = (TOwner owner, ICollection<TItem> baseValue) => {
                if(baseValue is ObservableCollection<TItem>)
                return baseValue;

                if (baseValue == null)
                return new ObservableCollection<TItem> ();

                return new ObservableCollection<TItem>
                ((IEnumerable<TItem>)baseValue);
            };

            return target;
        }
    }

    public class PropertyBuilder<T, TOwner> where TOwner : PropertyBase
    {
        public PropertyMetadata<T, TOwner> PropertyMetadata
        {
            get;
            private set;
        }

        public PropertyBuilder()
        {
            PropertyMetadata = new PropertyMetadata<T, TOwner>();
        }

        public PropertyBuilder<T, TOwner> DefaultValue(T value)
        {
            PropertyMetadata.DefaultValue = value;
            return this;
        }

        public PropertyBuilder<T, TOwner> Name(Expression<Func<object>> ex)
        {
            PropertyMetadata.Name = (ex.Body as MemberExpression)
                .Member.Name.Replace("Property", "");

            return this;
        }




        public PropertyBuilder<T, TOwner> CoerceValue(Func<TOwner, T, T> callback)
        {
            PropertyMetadata.CoerceValueCallback = callback;
            return this;
        }

        public PropertyBuilder<T, TOwner> Name(string name)
        {
            PropertyMetadata.Name = name;
            return this;
        }

        public PropertyBuilder<T, TOwner> Tags(params Tag[] tags)
        {
            PropertyMetadata.Tags = tags;
            return this;
        }

        public PropertyBuilder<T, TOwner> ChangedFromCollectionItems(bool enable)
        {
            PropertyMetadata.ChangedFromCollectionItems = enable;
            return this;
        }

        public PropertyBuilder<T, TOwner> ChangedFromCollection(bool enable)
        {
            PropertyMetadata.ChangedFromCollection = enable;
            return this;
        }

        public PropertyBuilder<T, TOwner> ChangedFromValue(bool enable)
        {
            PropertyMetadata.ChangedFromValue = enable;
            return this;
        }

        public static implicit operator Property<T, TOwner>(PropertyBuilder<T, TOwner> builder)
        {
            return new Property<T, TOwner>(builder.PropertyMetadata);
        }
    }

    public class Property<T>
    {
    }


    public class Property<T, TOwner> : Property<T> where TOwner : PropertyBase
    {
        public PropertyMetadata<T, TOwner> Metadata
        {
            get;
            private set;
        }

        class ValueData
        {
            public event Action<PropertyChangedArgs<T, TOwner>> Changed = (args) => {};

            public Option<T> Value
            {
                get;
                private set;
            }

            private TOwner _Owner;
            private PropertyMetadata<T, TOwner> _Metadata;
            private Property<T, TOwner> _Property;

            public ValueData(TOwner owner, Property<T, TOwner> property, PropertyMetadata<T, TOwner> metadata)
            {
                _Property = property;
                _Owner = owner;
                _Metadata = metadata;

                Value = new Option<T>.None();
            }

            public void SetValue(T value)
            {
                value = _Metadata.CoerceValueCallback(_Owner, value);

                var oldValue = Option<T>.GetValue(Value);
                Value = new Option<T>.Some(value);

                if(_Metadata.ChangedFromCollection)
                    SubscribeCollection(oldValue, value);

                if (!EqualityComparer<T>.Default.Equals(oldValue, value))
                {
                    OnChanged(oldValue, value);
                }
            }

            private void OnChanged(T oldValue, T newValue)
            {
                var args = new PropertyChangedArgs<T, TOwner>(
                    oldValue, newValue, _Property, _Owner);

                Changed(args);
                _Owner.PropertyChanged(args);
            }

            private void SubscribeCollection(T oldValue, T newValue)
            {
                var oldCollection = oldValue as INotifyCollectionChanged;
                var newCollection = newValue as INotifyCollectionChanged;

                if(oldCollection != null)
                    oldCollection.CollectionChanged -= HandleCollectionChanged;

                if(newCollection != null)
                    newCollection.CollectionChanged += HandleCollectionChanged;
            }



            private void HandleCollectionChanged (object sender, NotifyCollectionChangedEventArgs e)
            {
                var currentValue = GetValue();
                OnChanged(currentValue, currentValue);
            }

            public T GetValue()
            {
                if (Value is Option<T>.None)
                    return _Metadata.DefaultValue;

                return Option<T>.GetValue(Value);
            }
        }

        private Dictionary<TOwner, ValueData> _PropertyValues = 
            new Dictionary<TOwner, ValueData>();

        public Property(PropertyMetadata<T, TOwner> metadata)
        {
            Metadata = metadata;
        }

        public static PropertyBuilder<T, TOwner> New()
        {
            return new PropertyBuilder<T, TOwner>();
        }

        public void AddChangedCallback(TOwner owner, Action<PropertyChangedArgs<T, TOwner>> callback)
        {
            GetValueData(owner).Changed += callback;
        }

        public void RemoveChangedCallback(TOwner owner, Action<PropertyChangedArgs<T, TOwner>> callback)
        {
            GetValueData(owner).Changed -= callback;
        }

        public T GetValue(TOwner owner)
        {
            return GetValueData(owner).GetValue();
        }

        public void SetValue(TOwner owner, T value)
        {
            GetValueData(owner).SetValue(value);
        }


        private ValueData GetValueData(TOwner owner)
        {
            if (!_PropertyValues.ContainsKey(owner))
            {
                _PropertyValues[owner] = new ValueData(owner, this, Metadata);
            }

            return _PropertyValues[owner];
        }
    }

    public class PropertyBase
    {
        public class Listener
        {
            public Func<PropertyMetadata, bool> Selector
            {
                get;
                private set;
            }

            public Action Callback
            {
                get;
                private set;
            }

            public Listener(Func<PropertyMetadata, bool> selector, Action callback)
            {
                Selector = selector;
                Callback = callback;
            }

            public Listener()
            {

            }

            public static Listener New()
            {
                return new Listener();
            }


            public Listener WithTags(params Tag[] tags)
            {
                Selector = metadata => tags
                    .Any(tag => metadata.Tags.Contains(tag));

                return this;
            }

            public Listener WithCallback(Action callback)
            {
                Callback = callback;
                return this;
            }
        }

        private List<Listener> _Listeners = new List<Listener>();
        internal List<PropertyBase> ParentListeners
        {
            get;
            set;
        }

        public PropertyBase()
        {
            ParentListeners = new List<PropertyBase>();
        }

        public void AddListener(Listener listener)
        {
            _Listeners.Add(listener);
        }

        public void RemoveListener(Listener listener)
        {
            _Listeners.Remove(listener);
        }

        private void CallListeners(PropertyMetadata metadata)
        {
            var listeners = _Listeners
                .Where(listener => listener.Selector(metadata));

            foreach (var listener in listeners)
            {
                listener.Callback();
            }

        }


        private void SubscribeToChildValue(PropertyBase oldValue, PropertyBase newValue)
        {
            if(oldValue != null)
                oldValue.ParentListeners.Remove(this);

            if(newValue != null)
                newValue.ParentListeners.Add(this);
        }

        private void SubscribeToCollection<T>(T oldValue, T newValue)
        {
            var oldCollection = ((oldValue) as IEnumerable);
            var newCollection = ((newValue) as IEnumerable);

            if (oldCollection != null)
                foreach (var item in oldCollection.OfType<PropertyBase>())
            {
                SubscribeToChildValue(item, null);
            }
            if (newCollection != null)
                foreach (var item in newCollection.OfType<PropertyBase>())
            {
                SubscribeToChildValue(null, item);
            }
        }

        internal void PropertyChanged<T, TOwner>(PropertyChangedArgs<T, TOwner> args) where TOwner : PropertyBase
        {
            if (args.Property.Metadata.ChangedFromValue && typeof(PropertyBase).IsAssignableFrom(typeof(T)))
            {
                SubscribeToChildValue(
                    args.OldValue as PropertyBase, 
                    args.NewValue as PropertyBase);
            }

            if(args.Property.Metadata.ChangedFromCollectionItems)
                SubscribeToCollection(args.NewValue, args.OldValue);

            CallListeners(args.Property.Metadata);

            foreach (var parentListener in ParentListeners)
            {
                parentListener.PropertyChanged(args);
            }
        }
    }

}

