using System;

namespace Zia.UI.Data
{
    public interface IBindingConverter<TFrom, TTo>
    {
        TTo Convert(TFrom value);
    }

    public abstract class BindingConverter<TFrom, TTo> : IBindingConverter<TFrom, TTo>
    {
        public abstract TTo Convert(TFrom value);
    }

    public class CallbackBindingConverter<TFrom, TTo> : BindingConverter<TFrom, TTo>
    {
        Func<TFrom, TTo> _Callback;

        public CallbackBindingConverter(Func<TFrom, TTo> callback)
        {
         
            this._Callback = callback;   
        }

        public override TTo Convert(TFrom value)
        {
            return _Callback(value);
        }
    }
}

