﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zia.UI
{
    public static class TreeHelper
    {
        public static IEnumerable<Element> Ancestors(this Element target)
        {
            if (target == null)
                return new Element[]{ };

            return new[] { target }.Concat(target.Parent.Ancestors());
        }

        public static IEnumerable<Element> Descendants(this Element target) 
        {
            return new[] {target}.Concat((target.GetChildren())
                .Select(child => child.Descendants())
                .SelectMany(list => list));
        }
    }
}

