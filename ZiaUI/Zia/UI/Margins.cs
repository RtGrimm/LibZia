using System;

namespace Zia.UI
{
    public struct Margins
    {
        public float Left;
        public float Right;
        public float Top;
        public float Bottom;

        public Margins(float left, float right, float top, float bottom)
        {
            Left = left;
            Right = right;
            Top = top;
            Bottom = bottom;
        }

        public Margins(float uniform)
        {
            Left = uniform;
            Right = uniform;
            Top = uniform;
            Bottom = uniform;
        }

        public static Margins operator*(Margins left, float right) 
        {
            return new Margins(left.Left * right, 
                               left.Right * right,
                               left.Top * right,
                               left.Bottom * right);
        }

        public static Margins operator+(Margins left, Margins right) 
        {
            return new Margins(left.Left + right.Left, 
                               left.Right + right.Right,
                               left.Top + right.Top,
                               left.Bottom + right.Bottom);
        }

        public static Margins operator-(Margins left, Margins right) 
        {
            return new Margins(left.Left - right.Left, 
                               left.Right - right.Right, 
                               left.Top - right.Top, 
                               left.Bottom - right.Bottom);
        }

        public override string ToString()
        {
            return string.Format("[Margins: Left={0}, Right={1}, Top={2}, Bottom={3}]", Left, Right, Top, Bottom);
        }
        
    }
}

