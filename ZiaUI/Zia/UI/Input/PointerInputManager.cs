using System;
using System.Collections.Generic;
using System.Linq;
using Zia.Common;
using Zia.Gfx;

namespace Zia.UI.Input
{
    class HitTestResult
    {
        public Element HitElement
        {
            get;
            set;
        }

        public Vec2 LocalPoint
        {
            get;
            set;
        }
    }

    public class PointerInputManager
    {
        private IPointerInputSource _Source;

        private Element _Root;

        private ICollection<Element> _PointerEnclosingElements;

        public PointerInputManager()
        {
            _PointerEnclosingElements = new List<Element>();
        }

        public void SetRoot(Element root)
        {
            _Root = root;
        }

        public void SetSource(IPointerInputSource source)
        {
            if (_Source != null)
            {
                _Source.PointerDown -= PointerDown;
                _Source.PointerUp -= PointerUp;
                _Source.PointerMove -= PointerMove;
            }

            _Source = source;

            _Source.PointerDown += PointerDown;
            _Source.PointerUp += PointerUp;
            _Source.PointerMove += PointerMove;
        }

        ICollection<HitTestResult> HitTest(Vec2 point)
        {
            return HitTest(_Root, point);
        }

        void GetTransforms(Element target, Dictionary<Element, Transform> dict)
        {
            dict[target] = target.LastRenderTransform;
            target.GetChildren().Each(child => GetTransforms(child, dict));
        }

        ICollection<HitTestResult> HitTest(Element target, Vec2 point)
        {
            if (target == null)
                return new List<HitTestResult>();

            var transformMap = new Dictionary<Element, Transform>();
            var hitElements = new List<HitTestResult>();

            GetTransforms(target, transformMap);
            HitTest(target, point, transformMap, null, hitElements);

            return hitElements;
        }

        void HitTest(Element target, Vec2 point, Dictionary<Element, Transform> transformMap, Transform transform, List<HitTestResult> hitElements)
        {
            if (!target.HasRendered)
                return;

            transform = transformMap[target];

            var localPoint = transform.Invert() * point;

            var targetHit = target.HitTest(point, localPoint);

            if (targetHit)
            {
                var result = new HitTestResult {
                    HitElement = target,
                    LocalPoint = localPoint
                };

                hitElements.Add(result);
            }

            foreach (var element in target.GetChildrenSortedByZOrder()) {
                HitTest(element, point, transformMap, transform, hitElements);
            }




        }

        void RaisePointerEvent<RoutedEvent>(PointerEventArgs args, RoutedEvent routedEvent, HitTestResult result) 
            where RoutedEvent : RoutedEvent<PointerEventArgs, Element>
        {
            if (result != null)
            {
                var element = result.HitElement;
                routedEvent.Raise(element, args);
            }
        }

        void RaisePointerEvent<RoutedEvent>(PointerEventArgs args, RoutedEvent routedEvent) 
            where RoutedEvent : RoutedEvent<PointerEventArgs, Element>
        {
            var elements = HitTest(args.Point);
            var result = elements.LastOrDefault();

            RaisePointerEvent(args, routedEvent, result);
        }

        void PointerMove(PointerEventArgs args)
        {
            ICollection<HitTestResult> results = HitTest(args.Point);



            RaisePointerEvent(args, AdvancedElement.PointerMoveEvent, results.LastOrDefault());

            var newEnclosingElements = results.Select(result => result.HitElement).ToList();
            var oldEnclosingElements = _PointerEnclosingElements;

            var exitElements = oldEnclosingElements
                .Where(element => !newEnclosingElements.Contains(element));

            var enterElements = newEnclosingElements
                .Where(element => !oldEnclosingElements.Contains(element));



            exitElements.Select(element => new HitTestResult {
                HitElement = element
            })
                .Each(result => RaisePointerEvent(args, AdvancedElement.PointerLeaveEvent, result));

            enterElements.Select(element => results.Single(res => res.HitElement == element))
                .Each(result => RaisePointerEvent(args, AdvancedElement.PointerEnterEvent, result));

            _PointerEnclosingElements = newEnclosingElements;
        }

        void PointerDown(PointerEventArgs args)
        {
            RaisePointerEvent(args, AdvancedElement.PointerDownEvent);
        }

        void PointerUp(PointerEventArgs args)
        {
            RaisePointerEvent(args, AdvancedElement.PointerUpEvent);
        }
    }
}

