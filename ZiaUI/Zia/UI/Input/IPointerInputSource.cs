using System;
using Zia.Common;

namespace Zia.UI.Input
{
    public class PointerEventArgs : RoutedEventArgs<Element>
    {
        public Vec2 Point
        {
            get;
            set;
        }

        public PointerEventArgs(Vec2 point)
        {
            Point = point;
        }

        public PointerEventArgs()
        {
            
        }

        public Vec2 GetPosition(Element element)
        {
            return element.LastRenderTransform.Invert() * Point;
        }
    }

    public interface IPointerInputSource
    {
        event Action<PointerEventArgs> PointerUp;
        event Action<PointerEventArgs> PointerDown;
        event Action<PointerEventArgs> PointerMove;
    }
}

