using System;

namespace Zia.UI.Input
{
    class KeyInputManager
    {
        private Element _Root;
        private IKeyInputSource _Source;

        private AdvancedElement _FocusedElement;

        public KeyInputManager()
        {
        }

        public void SetRoot(Element root)
        {
            if(_Root != null)
                AdvancedElement.RequestFocusEvent.RemoveHandler(_Root, RequestFocus);
                

            _Root = root;
            AdvancedElement.RequestFocusEvent.AddHandler(_Root, RequestFocus);
        }

        public void SetSource(IKeyInputSource source)
        {
            if (_Source != null)
            {
                _Source.KeyDown -= KeyDown;
                _Source.KeyUp -= KeyUp;
                _Source.KeyPress -= KeyPress;
            }

            _Source = source;

            _Source.KeyDown += KeyDown;
            _Source.KeyUp += KeyUp;
            _Source.KeyPress += KeyPress;
        }

        void RequestFocus(RoutedEventArgs<Element> args)
        {
            if (_FocusedElement == args.Source)
                return;

            if (_FocusedElement != null)
            {
                AdvancedElement.LostFocusEvent.Raise(_FocusedElement);
            }

            var newFocusElement = args.Source as AdvancedElement;

            if (newFocusElement != null)
            {
                AdvancedElement.ReceivedFocusEvent.Raise(newFocusElement);
                newFocusElement.HasFocus = true;
                _FocusedElement = newFocusElement;
            }
        }

        void KeyPress (KeyPressEventArgs args)
        {
            if(_FocusedElement != null)
                AdvancedElement.KeyPressEvent.Raise(_FocusedElement, args);
        }

        void KeyUp (KeyEventArgs args)
        {
            if(_FocusedElement != null)
                AdvancedElement.KeyUpEvent.Raise(_FocusedElement, args);
        }

        void KeyDown (KeyEventArgs args)
        {
            if(_FocusedElement != null)
                AdvancedElement.KeyDownEvent.Raise(_FocusedElement, args);
        }


    }
}

