using System;
using Zia.Gfx;
using Zia.Common;
using System.Linq;

namespace Zia.UI
{
    class RenderManager
    {
        private Element Root(Element target)
        {
            return target.Parent == null ? target : Root(target.Parent);
        }


        private bool BoundsTestElement(Element target, Transform transform)
        {
            if (target.Parent == null)
                return true;

            var rect = new Rect(transform * new Vec2(), target.RenderSize);

            var root = Root(target);
            var rootRect = new Rect(root.LastRenderTransform * new Vec2(), root.RenderSize);

            return rootRect.Intersects(rect);
        }

        public void Render (Element target, Context context, Transform projection, Transform transform, float opacity)
        {
            opacity *= target.Opacity;

            context.EnableClip();
            UpdateTransform (target, context);

            var localTransform = target.Transform * context.CreateTransform()
                .Translate(new Vec3(target.LayoutRounding ? (int)target.RenderLocation.X : target.RenderLocation.X, 
                                    target.LayoutRounding ? (int)target.RenderLocation.Y : target.RenderLocation.Y));



            var newTransform = transform * localTransform;

            target.LastRenderTransform = newTransform;

            var isVisable = BoundsTestElement(target, newTransform);

            if (target.RenderSize.X < 1 || target.RenderSize.Y < 1 || !isVisable) {
                if (!target.CacheChildren && !target.Clip)
                {
                    target.GetChildrenSortedByZOrder()
                        .Each(child => Render(child, context, projection, newTransform, opacity));
                }

                return;
            }

            target.HasRendered = true;

            if (target.CacheEnabled) {
                RenderCached(target, context, projection, newTransform, opacity); 
            } else {
                RenderUncached(target, context, projection, newTransform, opacity);
            }
        }

        void UpdateTransform (Element target, Context context)
        {
            if (target.TransformInvalid) {
                target.Transform = target.Transforms.Aggregate (
                    context.CreateTransform (), 
                    (accTransform, transform) => 
                    accTransform * transform.GetTransform (context));

                target.TransformInvalid = false;
            }
        }

        private void RenderEffect(Element target, Context context)
        {
            if (target.CacheEnabled && (target.EffectInvalid))
            {
                target.Effect.SetInput(target.RenderCache);

                if (target.EffectCache != null)
                {
                    target.EffectCache.Dispose();
                }

                target.Effect.ShaderEffect.Render();
                target.EffectInvalid = false;
            }
        }

        void EnsureCache(Element target, Context context)
        {
            if (target.RenderCacheSize.Item1 != (int)target.RenderSize.X || 
                target.RenderCacheSize.Item2 != (int)target.RenderSize.Y)
            {
                target.RenderCacheSize = Tuple.Create((int)target.RenderSize.X, (int)target.RenderSize.Y);

                if (target.RenderCache != null)
                    target.RenderCache.Dispose();

                target.RenderCache = context.CreateRenderableTarget(
                    target.RenderCacheSize.Item1, 
                    target.RenderCacheSize.Item2,
                    ColorFormat.RGBA);
            }
        }

        void UpdateCache(Element target, Context context, float opacity)
        {
            if (target.RenderInvalid)
            {
                context.DisableClip();
                EnsureCache(target, context);

                var localProjection = context.CreateTransform().Ortho(
                    0, target.RenderSize.X, target.RenderSize.Y, 0, -1, 1);

                context.PushTarget(target.RenderCache);
                context.Clear(new Color(0, 0, 0, 0));

                target.RenderCore(context, localProjection, context.CreateTransform(), opacity);

                if (target.CacheChildren)
                {
                    target.GetChildrenSortedByZOrder().Each(child => Render(
                        child, context, localProjection, context.CreateTransform(), opacity));
                }

                context.PopTarget();

                target.RenderInvalid = false;
                context.EnableClip();
            }
        }

        void RenderUncached(Element target, Context context, Transform projection, Transform transfrom, float opacity)
        {
            if (target.Clip)
            {
                //context.EnableClip();
                var clipLocation = transfrom * new Vec2();
                //context.PushClip(new Rect(clipLocation, target.RenderSize));
            }

            target.RenderCore(context, projection, transfrom, opacity);

            target.GetChildrenSortedByZOrder().Each(
                child => Render(child, context, projection, transfrom, opacity));



            if (target.Clip)
            {
                //context.PopClip();
                //context.DisableClip();
            }
        }

        void RenderCached(Element target, Context context, Transform projection, Transform transform, float opacity)
        {
            UpdateCache(target, context, opacity);
            if (target.Effect != null)
            {
                var effect = target.Effect;
                RenderEffect(target, context);
                context.DrawEffect(effect.ShaderEffect, transform * projection);
            }
            else
            {
                context.DrawSurface(target.RenderCache, transform * projection);
            }

            if(!target.CacheChildren)
                target.GetChildrenSortedByZOrder().Each(
                    child => Render(child, context, projection, transform, opacity));
        }


    }
}

