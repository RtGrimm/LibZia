using System;
using Zia.Common;
using Zia.UI.Data;


namespace Zia.UI.Controls
{
    public enum StackPanelOrientation
    {
        Vertical,
        Horizontal
    }

    public  class StackPanel : AdvancedElement
    {
        public static readonly Property<StackPanelOrientation, StackPanel> OrientationProperty = 
                    Property<StackPanelOrientation, StackPanel>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .Name(() => OrientationProperty);

        public StackPanelOrientation Orientation
        {
            get
            {
                return OrientationProperty.GetValue(this);
            }
            set
            {
                OrientationProperty.SetValue(this, value);
            }
        }

        protected override Vec2 ArrangeOverride(Vec2 finalSize, Vec2 finalLocation)
        {
            float offset = 0;

            GetChildren().Each(child => {
                var location = Orientation == StackPanelOrientation.Horizontal ? 
                    new Vec2(offset, 0) : new Vec2(0, offset);

                var maxSize = Orientation == StackPanelOrientation.Horizontal ? 
                    new Vec2(MathUtil.Min(child.DesiredSize.X, finalSize.X), finalSize.Y) : 
                        new Vec2(finalSize.X, MathUtil.Min(child.DesiredSize.Y, finalSize.Y));


                child.Arrange(new Rect(finalLocation + location, maxSize));

                offset += Orientation == StackPanelOrientation.Horizontal ? 
                    maxSize.X : maxSize.Y;
            });

            return finalSize;
        }

        protected override Vec2 MeasureOverride(Vec2 availableSize)
        {
            float offset = 0;
            float max = 0;

            GetChildren().Each(child => {
                var childAvailableSize = Orientation == StackPanelOrientation.Horizontal ? 
                    new Vec2(MathUtil.Max(availableSize.X - offset, 0), availableSize.Y) : 
                        new Vec2(availableSize.X, MathUtil.Max(availableSize.Y - offset, 0)); 

                child.Measure(childAvailableSize);

                max = Orientation == StackPanelOrientation.Horizontal ? 
                    MathUtil.Max(max, child.DesiredSize.Y) : 
                        MathUtil.Max(max, child.DesiredSize.X);

                offset += Orientation == StackPanelOrientation.Horizontal ? 
                    child.DesiredSize.X : child.DesiredSize.Y;
            });

            return Orientation == StackPanelOrientation.Horizontal ? 
                new Vec2(offset, availableSize.Y) : new Vec2(availableSize.X, offset);
        }
    }
}

