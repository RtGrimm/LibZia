using System;
using Zia.Common;
using System.Linq;
using Zia.UI.Data;

namespace Zia.UI.Controls
{

    public class VelocityScrollViewer : ScrollViewer
    {
        
        public static readonly Property<float, VelocityScrollViewer> FrictionProperty = 
                    Property<float, VelocityScrollViewer>
                        .New()
            .DefaultValue(1.1f)
                        .Name(() => FrictionProperty);

        public float Friction
        {
            get
            {
                return FrictionProperty.GetValue(this);
            }
            set
            {
                FrictionProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, ScrollViewer> VelocityProperty = 
                    Property<Vec2, ScrollViewer>
                        .New()
                        .Name(() => VelocityProperty);

        public Vec2 Velocity
        {
            get
            {
                return VelocityProperty.GetValue(this);
            }
            set
            {
                VelocityProperty.SetValue(this, value);
            }
        }

        public VelocityScrollViewer()
        {
            
        }

        public override void UpdateOverride()
        {
            var velocity = Velocity;

            if (Math.Abs(Velocity.X) > 0.1 || Math.Abs(Velocity.Y) > 0.1)
            {
                velocity /= Friction;

                Offset += new Vec2(velocity.X, velocity.Y);
                Velocity = velocity;
            }
        }
    }

    public class ScrollViewer : AdvancedElement
    {

        public static readonly Property<bool, ScrollViewer> EnableVerticalScrollProperty = 
                    Property<bool, ScrollViewer>
                        .New()
                        .DefaultValue(true)
                        .Tags(LayoutInvalidationTag)
                        .Name(() => EnableVerticalScrollProperty);

        public bool EnableVerticalScroll
        {
            get
            {
                return EnableVerticalScrollProperty.GetValue(this);
            }
            set
            {
                EnableVerticalScrollProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, ScrollViewer> EnableHorizontalScrollProperty = 
                    Property<bool, ScrollViewer>
                        .New()

                        .Tags(LayoutInvalidationTag)
                        .Name(() => EnableHorizontalScrollProperty);

        public bool EnableHorizontalScroll
        {
            get
            {
                return EnableHorizontalScrollProperty.GetValue(this);
            }
            set
            {
                EnableHorizontalScrollProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, ScrollViewer> OffsetProperty = 
                    Property<Vec2, ScrollViewer>
                        .New()
                        .CoerceValue(CoerceOffset)
                        .Name(() => OffsetProperty);

        public Vec2 Offset
        {
            get
            {
                return OffsetProperty.GetValue(this);
            }
            set
            {
                OffsetProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, ScrollViewer> RelOffsetProperty = 
                    Property<Vec2, ScrollViewer>
                        .New()
                        .Name(() => RelOffsetProperty);

        public Vec2 RelOffset
        {
            get
            {
                return RelOffsetProperty.GetValue(this);
            }
            set
            {
                RelOffsetProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Vec2, ScrollViewer> ViewRatioProperty = 
                    Property<Vec2, ScrollViewer>
                        .New()
                        .Name(() => ViewRatioProperty);

        public Vec2 ViewRatio
        {
            get
            {
                return ViewRatioProperty.GetValue(this);
            }
            set
            {
                ViewRatioProperty.SetValue(this, value);
            }
        }

        public ScrollViewer()
        {
            OffsetProperty.AddChangedCallback(this, args => UpdateOffset());
            RelOffsetProperty.AddChangedCallback(this, args => Offset = ContentMaxSize(this) * RelOffset);
            RenderSizeProperty.AddChangedCallback(this, args => ViewRatio = RenderSize / ContentMaxSize(this));
        }

        static Vec2 ContentMaxSize(ScrollViewer scrollViewer)
        {
            var sizes = scrollViewer.GetChildren().Select(child => child.DesiredSize).Concat(new[] {
                new Vec2(0, 0)
            }).ToList();
            var maxSize = new Vec2(sizes.Max(size => size.X), sizes.Max(size => size.Y));
            return maxSize;
        }

        public static Vec2 CoerceOffset(ScrollViewer scrollViewer, Vec2 value)
        {
            var maxSize = ContentMaxSize(scrollViewer);


            maxSize.X = MathUtil.Clamp(maxSize.X - scrollViewer.RenderSize.X, 0, float.PositiveInfinity);
            maxSize.Y = MathUtil.Clamp(maxSize.Y - scrollViewer.RenderSize.Y, 0, float.PositiveInfinity);

            return new Vec2(MathUtil.Clamp(value.X, 0, scrollViewer.EnableHorizontalScroll ? maxSize.X : 0), 
                            MathUtil.Clamp(value.Y, 0, scrollViewer.EnableVerticalScroll ? maxSize.Y : 0));
        }

        void UpdateOffset()
        {
            GetChildren().Each(child => {
                child.Transforms.Clear();
                child.Transforms.Add(new TranslateTransform {
                    Offset = new Vec3(new Vec2(-Offset.X, -Offset.Y))
                });
            });
        }


        protected override Zia.Common.Vec2 ArrangeOverride(Zia.Common.Vec2 finalSize, Vec2 finalLocation)
        {
            GetChildren().Each(child => {
                child.Arrange(new Rect(finalLocation, 
                                       new Vec2(
                    EnableHorizontalScroll ? child.DesiredSize.X : finalSize.X, 
                         EnableVerticalScroll ? child.DesiredSize.Y : finalSize.Y)));
            });

            UpdateOffset();

            return finalSize;
        }

        protected override Zia.Common.Vec2 MeasureOverride(Zia.Common.Vec2 availableSize)
        {
            if (EnableHorizontalScroll)
                availableSize.X = float.PositiveInfinity;

            if (EnableVerticalScroll)
                availableSize.Y = float.PositiveInfinity;

            return base.MeasureOverride(availableSize);
        }
    }
}

