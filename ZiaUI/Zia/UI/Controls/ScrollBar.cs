using System;
using Zia.UI.Data;
using Zia.Common;
using Zia.UI.Shapes;
using System.Collections.Generic;
using System.Linq;
using Zia.UI.Brushes;
using Zia.UI.Input;

namespace Zia.UI.Controls
{

    public class ScrollBar : ProxyRoot<ScrollBar>
    {
        private ProxyElement<ScrollBar> _Thumb;

        public static readonly Property<bool, ScrollBar> IsVerticalProperty =
                    Property<bool, ScrollBar>
                        .New()
                        .DefaultValue(true)
                        .Name(() => IsVerticalProperty);

        public bool IsVertical
        {
            get
            {
                return IsVerticalProperty.GetValue(this);
            }
            set
            {
                IsVerticalProperty.SetValue(this, value);
            }
        }

        public static readonly Property<float, ScrollBar> LengthProperty =
                    Property<float, ScrollBar>
                        .New()
                        .CoerceValue((owner, value) => MathUtil.Clamp(value, 0, 1))
                        .Name(() => LengthProperty);

        public float Length
        {
            get
            {
                return LengthProperty.GetValue(this);
            }
            set
            {
                LengthProperty.SetValue(this, value);
            }
        }

        public static readonly Property<float, ScrollBar> OffsetProperty =
                    Property<float, ScrollBar>
                        .New()
                        .CoerceValue((owner, value) => MathUtil.Clamp(value, 0, 1 - owner.Length))
                        .Name(() => OffsetProperty);

        public float Offset
        {
            get
            {
                return OffsetProperty.GetValue(this);
            }
            set
            {
                OffsetProperty.SetValue(this, value);
            }
        }

        private Vec2 _MouseOffset;
        private bool _IsDragging;

        class DefaultTemplate : ITemplate
        {
            public AdvancedElement CreateTree()
            {
                return new Rectangle
                {
                    Children = new List<Element> {
                        new ProxyElement<ScrollBar>("Track") {
                            Children = new List<Element> {
                                new ProxyElement<ScrollBar>("Thumb") {
                                    Children = new List<Element> {
                                        new Rectangle {
                                            Fill = new SolidBrush(Color.PeterRiver),
                                            Width = Zia.UI.Length.MatchSlot,
                                            Height = Zia.UI.Length.MatchSlot,
                                            CornerRadius = new CornerRadius(20)
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
            }
        }

        public ScrollBar()
        {
            OffsetProperty.AddChangedCallback(this, args => UpdateThumb());
            LengthProperty.AddChangedCallback(this, args => UpdateThumb());
            RenderSizeProperty.AddChangedCallback(this, args => UpdateThumb());



            TreeChanged += args => {
                if (_Thumb != null)
                {
                    AdvancedElement.PointerMoveEvent.RemoveHandler(_Thumb.Parent, ThumbPointerMove);
                  AdvancedElement.PointerDownEvent.RemoveHandler(_Thumb, ThumbPointerDown);
                  AdvancedElement.PointerUpEvent.RemoveHandler(_Thumb, ThumbPointerUp);
                }

                _Thumb = FindProxies("Thumb").SingleOrDefault();

                if (_Thumb != null)
                {
                  AdvancedElement.PointerUpEvent.AddHandler(_Thumb, ThumbPointerUp);
                    AdvancedElement.PointerMoveEvent.AddHandler(_Thumb.Parent, ThumbPointerMove);
                  AdvancedElement.PointerDownEvent.AddHandler(_Thumb, ThumbPointerDown);
                }

                UpdateThumb();
            };

            ProxyOverrides.Add(new ProxyOverride {
                Name = "Root",
                Template = new DefaultTemplate()
            });
        }

        void ThumbPointerUp(PointerEventArgs obj)
        {
            _IsDragging = false;
        }

        void ThumbPointerDown(PointerEventArgs args)
        {
            var mouseArgs = args;
            _MouseOffset = mouseArgs.GetPosition(_Thumb.Parent) - _Thumb.RenderLocation;

            Console.WriteLine("Offset : {0}", _MouseOffset);

            _IsDragging = true;
        }

        void ThumbPointerMove(PointerEventArgs args)
        {
            if (!_IsDragging)
                return;

            var mouseArgs = args;

            var fullLength = IsVertical ?
                _Thumb.Parent.RenderSize.Y :
                    _Thumb.Parent.RenderSize.X;

            Console.WriteLine(fullLength);


            var offset = (mouseArgs.GetPosition(_Thumb.Parent) - _MouseOffset) / fullLength;
            Offset = IsVertical ? offset.Y : offset.X;

            Console.WriteLine(Offset);
            Console.WriteLine(mouseArgs.GetPosition(_Thumb.Parent));
        }

        void UpdateLength(float fullLength)
        {
            var length = fullLength * Length;

            if (IsVertical)
                _Thumb.Height = new Exact(length);
            else
                _Thumb.Width = new Exact(length);
        }

        void UpdateOffset(float fullLength)
        {
            var offset = fullLength * Offset;
            var margins = _Thumb.Margins;

            if (IsVertical)
                margins.Top = offset;
            else
                margins.Left = offset;

            _Thumb.Margins = margins;
        }

        private void UpdateThumb()
        {
            if (_Thumb == null || _Thumb.Parent == null)
                return;

            var fullLength = IsVertical ? _Thumb.Parent.RenderSize.Y : _Thumb.Parent.RenderSize.X;

            UpdateLength(fullLength);
            UpdateOffset(fullLength);
        }
    }
}
