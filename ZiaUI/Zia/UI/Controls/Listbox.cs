﻿using System;
using System.Collections.Generic;
using Zia.UI.Data;
using System.Linq;
using Zia.Common;
using Zia.UI.Shapes;
using Zia.UI.Brushes;

namespace Zia.UI.Controls
{
    public class Listbox<T> : ProxyRoot<Listbox<T>> where T : class
    {
        public static readonly Property<ICollection<T>, Listbox<T>> ItemsProperty =
            Property<ICollection<T>, Listbox<T>>
                        .New()
                        .ChangedFromCollection(true)
                        .CoerceToObservableCollection()
                        .Name(() => ItemsProperty);

        public ICollection<T> Items
        {
            get
            {
                return ItemsProperty.GetValue(this);
            }
            set
            {
                ItemsProperty.SetValue(this, value);
            }
        }

        public static readonly DataContext<T, Element> ItemDataContext = new DataContext<T, Element>();
        private ICollection<ProxyElement<Listbox<T>>> _ListElements = 
            new List<ProxyElement<Listbox<T>>>();

        public Listbox()
        {
            Items = new List<T>();
            ItemsProperty.AddChangedCallback(this, ItemsChanged);

            UpdateProxyOverride += args => UpdateContainer();

            OverrideProxy(new ProxyOverride {
                Name = "Root",
                Template = new CallbackTemplate(() => new ProxyElement<Listbox<T>>("Container") {
                    SizeToContent = true,
                    Width = Length.Auto,
                    Height = Length.Auto
                })
            });

            OverrideProxy(new ProxyOverride {
                Name = "Container",
                Template = new CallbackTemplate(() => new StackPanel {
                    SizeToContent = true,
                    Width = Length.Auto,
                    Height = Length.Auto,
                    Orientation = StackPanelOrientation.Vertical
                })
            });

            OverrideProxy(new ProxyOverride {
                Name = "Item",
                Template = new CallbackTemplate(() => new AdvancedElement())
            });


        }

        void ItemsChanged(PropertyChangedArgs<ICollection<T>, Listbox<T>> args)
        {
            UpdateItems();
        }

        void UpdateContainer()
        {
            var proxies = FindProxies("Container");
            var proxyElement = proxies.SingleOrDefault();
            var container = proxyElement.GetChildren().SingleOrDefault();
            container?.Apply(_ => {
                container.Children = _ListElements.OfType<Element>().ToList();
            });
        }

        void UpdateItems()
        {
            _ListElements = Items.Select(item => 
            {
                var oldElement = _ListElements.SingleOrDefault
                    (element => ItemDataContext.Get(element) == item);

                if(oldElement != null)
                    return oldElement;

                var newElement = new ProxyElement<Listbox<T>>("Item") {
                    SizeToContent = true,

                    Height = Width = Length.Auto
                };
                ItemDataContext.Set(newElement, item);
                return newElement;
            }).ToList();

            UpdateContainer();
        }
    }
}

