using System;
using Zia.Common;
using System.Collections.Generic;
using System.Linq;
using Zia.UI.Data;

namespace Zia.UI.Controls
{
    public class Grid : AdvancedElement
    {


        public static readonly Property<int, Element> GridColumnProperty = 
            Property<int, Element>
                .New()
                .Name(() => GridColumnProperty);

        public static readonly Property<int, Element> GridRowProperty = 
            Property<int, Element>
                        .New()
                        .Name(() => GridRowProperty);

        public static readonly Property<ICollection<Row>, Grid> RowsProperty = 
            Property<ICollection<Row>, Grid>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .ChangedFromCollectionItems(true)
                        .ChangedFromCollection(true)
                        .Name(() => RowsProperty);

        public ICollection<Row> Rows
        {
            get
            {
                return RowsProperty.GetValue(this);
            }
            set
            {
                RowsProperty.SetValue(this, value);
            }
        }

        public static readonly Property<ICollection<Column>, Grid> ColumnsProperty = 
            Property<ICollection<Column>, Grid>
                        .New()
                        .Tags(LayoutInvalidationTag)
                        .ChangedFromCollectionItems(true)
                        .ChangedFromCollection(true)
                        .Name(() => ColumnsProperty);

        public ICollection<Column> Columns
        {
            get
            {
                return ColumnsProperty.GetValue(this);
            }
            set
            {
                ColumnsProperty.SetValue(this, value);
            }
        }

        public class Row : PropertyBase
        {
            public static readonly Property<float, Row> HeightProperty = 
                Property<float, Row>
                               .New()
                               .Tags(LayoutInvalidationTag)
                               .Name(() => HeightProperty);

            public float Height
            {
                get
                {
                    return HeightProperty.GetValue(this);
                }
                set
                {
                    HeightProperty.SetValue(this, value);
                }
            }

            public Row(float height)
            {
                this.Height = height;
            }
        }

        public class Column : PropertyBase
        {
            public static readonly Property<float, Column> WidthProperty = 
                Property<float, Column>
                    .New()
                    .Tags(LayoutInvalidationTag)
                    .Name(() => WidthProperty);

            public float Width
            {
                get
                {
                    return WidthProperty.GetValue(this);
                }
                set
                {
                    WidthProperty.SetValue(this, value);
                }
            }

            public Column(float width)
            {
                this.Width = width;
            }
        }

        class GridSlot
        {
            public int X
            {
                get;
                set;
            }

            public int Y
            {
                get;
                set;
            }

            public Column Column
            {
                get;
                set;
            }

            public Row Row
            {
                get;
                set;
            }

            public Vec2 Size
            {
                get;
                set;
            }

            public GridSlot(int x, int y, Zia.UI.Controls.Grid.Column column, Zia.UI.Controls.Grid.Row row)
            {
                this.X = x;
                this.Y = y;
                this.Column = column;
                this.Row = row;
            }
        }

        public Grid()
        {
            Columns = new List<Column>();
            Rows = new List<Row>();
        }

        private ICollection<Element> ElementsAtCoord(int x, int y)
        {
            return GetChildren().Where(element =>
                                       GridRowProperty.GetValue(element) == y &&
            GridColumnProperty.GetValue(element) == x).ToList();
        }

        private ICollection<GridSlot> GetSlots(Vec2 size)
        {
            var slots = Columns
                .Select((column, x) => Rows.Select((row, y) => new GridSlot(x, y, column, row)))
                .SelectMany(list => list)
                .ToList();

         

            float remainingWidth = size.X;
            slots.GroupBy(slot => slot.Column)
                .OrderByDescending(slotList => slotList.Key.Width > 1)
                .Each(slotList =>
            {
                float remainingHeight = size.Y;

                slotList.OrderByDescending(slot => slot.Row.Height > 1).Each(slot =>
                {
                    var width = slot.Column.Width;
                    var height = slot.Row.Height;

                    slot.Size = new Vec2(
                        width <= 1.0f ? size.X * width : width, 
                        height <= 1.0f ? size.Y * height : height)
                        .Clamp(new Vec2(), new Vec2(remainingWidth, remainingHeight));

                    remainingHeight -= slot.Row.Height > 1 ? slot.Size.Y : 0;
                });

                remainingWidth -= slotList.Key.Width <= 1.0f ? 
                remainingWidth * slotList.Key.Width : 0;
            });

            return slots;
        }

        private Vec2 OffsetAt(int x, int y, Vec2 scale, ICollection<GridSlot> slots)
        {
            var offset = new Vec2(
                slots.Where(slot => slot.X < x).Sum(slot => slot.Size.X),
                slots.Where(slot => slot.Y < y).Sum(slot => slot.Size.Y));
            return offset;
        }

        protected override Vec2 ArrangeOverride(Vec2 finalSize, Vec2 finalLocation)
        {
            var slots = GetSlots(finalSize);

            slots.Each(slot =>
            {
                var size = slot.Size;

                var elements = ElementsAtCoord(slot.X, slot.Y);
                var offset = OffsetAt(slot.X, slot.Y, finalSize, slots);

                elements.Each(element => element.Arrange(new Rect(offset + finalLocation, size)));
            });


            return finalSize;
        }

        protected override Vec2 MeasureOverride(Vec2 availableSize)
        {
            var slots = GetSlots(availableSize);

            slots.Each(slot =>
            {
                var size = slot.Size;
                var elements = ElementsAtCoord(slot.X, slot.Y);

                elements.Each(element => element.Measure(size));
            });

            var desiredSize = new Vec2(
                                  slots.Where(slot => slot.X <= Columns.Count && slot.Y == 0).Sum(slot => slot.Size.X),
                                  slots.Where(slot => slot.Y <= Rows.Count && slot.X == 0).Sum(slot => slot.Size.Y));

            return desiredSize;
        }
    }
}

