using System;
using Zia.Gfx;
using Zia.Gfx.Text;
using Zia.Common;
using System.Linq;
using Zia.UI.Input;
using Zia.Gfx.Geometry;
using Zia.UI.Data;

namespace Zia.UI.Controls
{
    public class EditText : AdvancedElement
    {
        internal static Tag TextLayoutInvalidationTag = new Tag("TextLayoutInvalidation");

        public static readonly Property<string, EditText> TextProperty =
            Property<string, EditText>
                .New()
                .Tags(RenderInvalidationTag, TextLayoutInvalidationTag)
                .Name(() => TextProperty);

        public string Text
        {
            get
            {
                return TextProperty.GetValue(this);
            }
            set
            {
                TextProperty.SetValue(this, value);
            }
        }

        public static readonly Property<bool, EditText> WrapProperty =
            Property<bool, EditText>
                .New()
                .Tags(RenderInvalidationTag, TextLayoutInvalidationTag)
                .Name(() => WrapProperty);

        public bool Wrap
        {
            get
            {
                return WrapProperty.GetValue(this);
            }
            set
            {
                WrapProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Font, EditText> FontProperty =
            Property<Font, EditText>
                .New()
                .Tags(RenderInvalidationTag, TextLayoutInvalidationTag)
                .Name(() => FontProperty);

        public Font Font
        {
            get
            {
                return FontProperty.GetValue(this);
            }
            set
            {
                FontProperty.SetValue(this, value);
            }
        }


        public static readonly Property<float, EditText> FontSizeProperty =
            Property<float, EditText>
                .New()
                .DefaultValue(20)
                .Name(() => FontSizeProperty);

        public float FontSize
        {
            get
            {
                return FontSizeProperty.GetValue(this);
            }
            set
            {
                FontSizeProperty.SetValue(this, value);
            }
        }

        public static readonly Property<Brushes.Brush, EditText> BrushProperty =
            Property<Brushes.Brush, EditText>
                .New()
                .Name(() => BrushProperty);

        public Brushes.Brush Brush
        {
            get
            {
                return BrushProperty.GetValue(this);
            }
            set
            {
                BrushProperty.SetValue(this, value);
            }
        }


        private TextLayout _TextLayout = new TextLayout();

        private int _StartIndex = 0;

        private int _EndIndex = 0;

        private Vec2 _CursorPosStart;

        private Vec2 _CursorPosEnd;

        private float _CursorHeight;

        private bool _IsDragging;

        private bool _CursorVisable;

        private bool _TextInvalid;

        public EditText()
        {
            _TextLayout.Text = "";

            PointerDown += args => {
                RequestFocusEvent.Raise(this);
                OnPointerDown((PointerEventArgs)args);
            };

            LostFocus += args => 
                _CursorVisable = false;
            ReceivedFocus += args => 
                _CursorVisable = true;

            PointerMove += args => 
                OnPointerMove((PointerEventArgs)args);
            PointerUp += args => 
                _IsDragging = false;
            PointerLeave += args => 
                _IsDragging = false;

            KeyPress += OnKeyPress;
            KeyDown += OnKeyDown;
            SubscribeTextEvents();

            /*
            FillWidthProperty.OverrideMetadata(typeof(EditText), new PropertyMetadata {
                DefaultValue = false
            });

            FillHeightProperty.OverrideMetadata(typeof(EditText), new PropertyMetadata {
                DefaultValue = false
            });
             */
        }

        int PointToIndex(Vec2 localPoint)
        {
            var hitTestMetrics = _TextLayout.HitTest(localPoint);

            if (!hitTestMetrics.Hit)
                return -1;

            var index = _TextLayout.CharsList.ToList(
            ).IndexOf(hitTestMetrics.CharData) + (hitTestMetrics.IsLeft ? 0 : 1);

            return index;
        }

        void OnPointerMove(PointerEventArgs args)
        {
            if (_IsDragging)
            {
                var index = PointToIndex(args.GetPosition(this));

                UpdateIndex(_StartIndex, index);
            }
        }

        void OnPointerDown(PointerEventArgs args)
        {
            var pos = args.GetPosition(this);
            var index = PointToIndex(pos);
            Console.WriteLine("Index:" + index);

            if (index == -1)
            {
                UpdateIndex(_EndIndex, _EndIndex);
                return;
            }

            UpdateIndex(index, index);
            _IsDragging = true;
        }

        void SubscribeTextEvents()
        {
            AddListener(Listener.New()
                        .WithTags(TextLayoutInvalidationTag)
                        .WithCallback(() => _TextInvalid = true));

            TextProperty.AddChangedCallback(this, args => _TextLayout.Text = Text);

            FontSizeProperty.AddChangedCallback(this, args =>
            {
                if (Font != null)
                    _TextLayout.Size = FontSize;
            });

            FontProperty.AddChangedCallback(this, args =>
            {
                _TextLayout.Font = Font;
                _TextLayout.Size = FontSize;
            });
        }

        void OnKeyDown(RoutedEventArgs<Element> args)
        {
            var keyArgs = (KeyEventArgs)args;

            if (keyArgs.KeyCode == Key.Left)
                MoveLeft(keyArgs.Shift);

            if (keyArgs.KeyCode == Key.Right)
                MoveRight(keyArgs.Shift);

            if (keyArgs.KeyCode == Key.BackSpace)
                DeleteChar(true);

            if (keyArgs.KeyCode == Key.Delete)
                DeleteChar(false);

            if (keyArgs.KeyCode == Key.Home)
            {
                if (keyArgs.Shift)
                    UpdateIndex(Math.Max(_StartIndex, _EndIndex), 0);
                else
                    UpdateIndex(0, 0);
            }

            if (keyArgs.KeyCode == Key.End)
            {
                if (keyArgs.Shift)
                    UpdateIndex(Math.Min(_StartIndex, _EndIndex), _TextLayout.CharsList.Count);
                else
                    UpdateIndex(_TextLayout.CharsList.Count, _TextLayout.CharsList.Count);
            }

            if (keyArgs.Control && keyArgs.KeyCode == Key.A)
            {
                UpdateIndex(0, _TextLayout.CharsList.Count);
            }


        }

        void MoveLeft(bool moveEnd)
        {
            if (moveEnd)
                UpdateIndex(_StartIndex, _EndIndex - 1);
            else
            {
                if (_EndIndex != _StartIndex)
                    UpdateIndex(_EndIndex, _EndIndex);
                else
                    UpdateIndex(_StartIndex - 1, _StartIndex - 1);
            }
        }

        void MoveRight(bool moveEnd)
        {
            if (moveEnd)
                UpdateIndex(_StartIndex, _EndIndex + 1);
            else
            {
                if (_EndIndex != _StartIndex)
                    UpdateIndex(_EndIndex, _EndIndex);
                else
                    UpdateIndex(_StartIndex + 1, _StartIndex + 1);
            }
        }

        void OnKeyPress(KeyPressEventArgs args)
        {
            var keyArgs = args;
            InsertChar(keyArgs.KeyChar);
        }

        private Vec2 CalcCursorPos(int index)
        {
            if (_TextLayout.CharsList.Count == 0)
            {
                _CursorHeight = _TextLayout.LinesList.Single().BoundingBox.Size.Y;
                return new Vec2(0, 0);
            }

            var clampIndex = MathUtil.Clamp(index - 1, 0, _TextLayout.CharsList.Count);

            var charData = _TextLayout.CharsList.ElementAt(clampIndex);

            var location = charData.Location;

            if (index != 0)
                location += new Vec2(charData.Size.X, 0);

            if (index == 0)
                location -= new Vec2(5, 0);
            else
                location -= new Vec2(2, 0);

            _CursorHeight = charData.Size.Y;

            return location;
        }

        void UpdateText()
        {
            if (_TextInvalid)
            {
                _TextLayout.UpdateLayout();
                _TextInvalid = false;      

                LayoutInvalidateEvent.Raise(this);
            }
        }

        private void UpdateIndex(int start, int end)
        {
            UpdateText();

            _StartIndex = MathUtil.Clamp(start, 0, _TextLayout.CharsList.Count);
            _EndIndex = MathUtil.Clamp(end, 0, _TextLayout.CharsList.Count);

            _CursorPosStart = _StartIndex < _EndIndex ? CalcCursorPos(_StartIndex) : CalcCursorPos(_EndIndex);
            _CursorPosEnd = _EndIndex > _StartIndex ? CalcCursorPos(_EndIndex) : CalcCursorPos(_StartIndex);

            RenderInvalidateEvent.Raise(this);
        }

        private void DeleteChar(bool backspace)
        {
            if (_TextLayout.CharsList.Count == 0)
                return;

            if (_StartIndex == _EndIndex)
            {
                if ((_StartIndex == 0 && backspace) || (_EndIndex == _TextLayout.CharsList.Count && !backspace))
                    return;

                if (backspace)
                {
                    Text = Text.Remove(_StartIndex - 1, 1);
                    UpdateIndex(_StartIndex - 1, _StartIndex - 1);
                }
                else
                {
                    Text = Text.Remove(_StartIndex, 1);

                }
            }
            else
            {
                Text = Text.Remove(Math.Min(_StartIndex, _EndIndex), Math.Abs(_EndIndex - _StartIndex  ));
                UpdateIndex(_EndIndex, _EndIndex);
            }
        }

        private void InsertChar(char c)
        {
            if (_StartIndex != _EndIndex)
            {
                DeleteChar(false);
            }

            Text = Text.Insert(MathUtil.Clamp(Math.Min(_StartIndex, _EndIndex), 0, int.MaxValue), c.ToString());
            UpdateIndex(_StartIndex + 1, _StartIndex + 1);

        }

        protected override void RenderOverride(Zia.Gfx.Context context, Zia.Gfx.Transform projection, Zia.Gfx.Transform transform, float opacity)
        {
            var brush = Brush;

            if (brush != null && Font != null)
            {
                context.DrawTextLayout(_TextLayout, brush.GetBrush(), transform * projection, opacity);

                var cursorPath = new PathGeometry();

                cursorPath.StrokeStyle = new StrokeStyle(JoinType.Miter, EndType.OpenButt, 1);

                if(_CursorVisable)
                if (_CursorPosStart == _CursorPosEnd)
                {

                    cursorPath.Rectangle((_CursorPosStart - new Vec2(0, 5)).Round(), new Vec2(1f, _CursorHeight + 7).Round());
                    context.FillGeometry(cursorPath, brush.GetBrush(), transform * projection, opacity);
                }
                else
                {
                    cursorPath.Rectangle(_CursorPosStart - new Vec2(0, 5), new Vec2((_CursorPosEnd - _CursorPosStart).X, _CursorHeight + 7));
                    context.FillGeometry(cursorPath, brush.GetBrush(), transform * projection, 0.5f * opacity);
                }


            }
        }

        protected override Vec2 MeasureOverride(Vec2 availableSize)
        {
            _TextLayout.MaxSize = availableSize;
            _TextLayout.UpdateLayout();

            return _TextLayout.TextSize;
        }

        protected override Vec2 ArrangeOverride(Vec2 finalSize, Vec2 finalLocation)
        {
            _TextLayout.MaxSize = finalSize;
            _TextLayout.UpdateLayout();
            UpdateIndex(_StartIndex, _EndIndex);

            return finalSize;
        }
    }
}
