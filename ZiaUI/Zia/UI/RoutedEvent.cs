using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using Zia.Common;

namespace Zia.UI
{
	public enum RoutingStrategy
	{
		Direct,
		Bubble,
		Tunnel
	}

	public class RoutedEventArgs<TOwner> : EventArgs where TOwner : Element
	{
		public bool Handled {
			get;
			set;
		}

        public TOwner Source {
			get;
			internal set;
		}
	}

	public class RoutedEvent<TArgs, TOwner> where TOwner : Element where TArgs : RoutedEventArgs<TOwner>, new()
	{
		public RoutingStrategy RoutingStrategy {
			get;
			private set;
		}

		public string Name {
			get;
			private set;
		}

        class EventData
        {
            private RoutedEvent<TArgs, TOwner> _Parent;
            private TOwner _Owner;
            private List<Action<TArgs>> _Handlers = new List<Action<TArgs>>();

            public EventData(TOwner owner, RoutedEvent<TArgs, TOwner> parent)
            {
                this._Owner = owner;
                this._Parent = parent;
            }

            public void Raise(TArgs args)
            {
                args.Source = _Owner;
                Raise(args, _Parent.RoutingStrategy);
            }

            public void AddHandler(Action<TArgs> handler)
            {
                _Handlers.Add(handler);
            }

            public void RemoveHandler(Action<TArgs> handler)
            {
                _Handlers.Remove(handler);
            }

            private void Raise(TArgs args, RoutingStrategy routingStratagy)
            {
                if (args.Handled)
                    return;

                if (routingStratagy == RoutingStrategy.Direct) {
                    _Handlers.Each (callback => callback (args));
                } else if (routingStratagy == RoutingStrategy.Bubble) {
                    Raise (args, RoutingStrategy.Direct);

                    if (_Owner.Parent != null) {
                        BubbleEvent(_Owner.Parent, args);
                    }
                }
                else if(routingStratagy == RoutingStrategy.Tunnel)
                {
                    Raise (args, RoutingStrategy.Direct);
                    _Owner.GetChildren().Each(child => TunnelEvent(child, args));
                }
            }

            private void TunnelEvent(Element target, TArgs args)
            {
                if (target == null)
                    return;

                var owner = target as TOwner;

                if (owner == null)
                {
                    target.GetChildren().Each(child => TunnelEvent(child, args));
                    return;
                }

                _Parent.GetData(owner).Raise(args, RoutingStrategy.Tunnel);
            }

            private void BubbleEvent(Element target, TArgs args)
            {
                if (target == null)
                    return;

                var owner = target as TOwner;

                if (owner == null)
                {
                    BubbleEvent(target.Parent, args);
                    return;
                }

                _Parent.GetData(owner).Raise(args, RoutingStrategy.Bubble);
            }
           
        }

        private Dictionary<TOwner, EventData> _EventDataList = 
            new Dictionary<TOwner, EventData>();


        public RoutedEvent (string name, RoutingStrategy routingStrategy)
		{
			RoutingStrategy = routingStrategy;
			Name = name;
		}

        public void Raise(TOwner owner)
        {
            GetData(owner).Raise(new TArgs());
        }

        public void Raise(TOwner owner, TArgs args)
        {
            GetData(owner).Raise(args);
        }

        public void AddHandler(TOwner owner, Action<TArgs> handler) {
            GetData(owner).AddHandler(handler);
        }

        public void RemoveHandler(TOwner owner, Action<TArgs> handler) {
            GetData(owner).RemoveHandler(handler);
        }


        private EventData GetData(TOwner owner)
        {
            if (owner == null)
                return null;

            if (!_EventDataList.ContainsKey(owner))
            {
                _EventDataList[owner] = new EventData(owner, this);
            }

            return _EventDataList[owner];
        }
		
        public RoutedEvent (Expression<Func<object>> name, RoutingStrategy routingStrategy)
        {
            RoutingStrategy = routingStrategy;
            Name = (name.Body as MemberExpression)
                .Member.Name.Replace("Event", "");
        }
	}
}

